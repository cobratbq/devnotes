# Genetic algorithms

## Notes

- Encodings: binary, vector, list
- Cross-overs: 
- Mutations: point mutation, swap
- Soft constraints: take into account in the fitness function.
- Hard constraints: remove from / take into account in encoding to ensure that we cannot generate solutions that violate hard constraints.
- Reasons for using GA:
  - there may not be an exact solution
  - search space is large
  - fitness landscape is complex
  - best solutions lie on Pareto front
- GA vs. Simulated Annealing:
  - GA: finds better solutions,
  - GA: global search,
  - GA: easily parallelizable
  - GA: broader applications,
  - SA: finds solutions faster,
  - SA: local search
- GA vs. Branch &amp; Bound:
  - GA: approximate solutions,
  - GA: better able to cope with large, complex problems,
  - BB: best solution, guaranteed,
  - BB: limited to small problems,
- GA vs. Gradient Descent:
  - GA: discrete optimization,
  - GA: wins at complex fitness landscapes,
  - GA: no calculus required,
  - GD: continuous optimization,
  - GD: wins at speed,
- GA vs. Neural Networks:
  - GA: optimization,
  - GA: classification (GBML)
  - GA: human comparable design,
  - NN: classification,
  - NN: pattern recognition,
- GA is good at finding the globally-best neighborhood, but less good at finding the perfect solution there. So sometimes use alternative method, e.g. hill-climbing, to find the actual solution once the globally-best neighborhood is established.
- Design choices:
  1. Encoding --> bad encoding, needs to change lot of code, start from scratch.
  1. Fitness function --> determines performance, quality of solutions.
- References:
  - [YouTube: Genetic Algorithms - Jeremy Fisher](https://www.youtube.com/watch?v=7J-DfS52bnI)
  - David E. Goldberg: Genetic Algorithms, in search of optimization and machine learning
