# Security considerations

## Mail clients

1. Know where drafts/queued-for-sending messages are stored.
    - prevent confidential messages to be stored on server (IMAP) in plain text, e.g. _Drafts_ and _Queue_ folders.
    - ensure that subject is either non-descriptive or encrypted. (GnuPG in-line and S/MIME cannot process subjects)
