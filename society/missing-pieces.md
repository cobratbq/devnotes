# Missing pieces in technology

- A simple, constant, reliable, available chat network that allows simple-but-reliable presence and contact.  
  Facebook, LinkedIn, WhatsApp, Telegram, etc. are available, but all offer a very elaborate set of features, but in a walled-off network. We need a boot-strapping mechanism on which elaborate networks can build.
- A childishly simple way of sharing/collaborating.  
  Many mechanisms, such as git and various git services, rely on centralized mechanism and always-online functionality in order to make it work. For developers to flourish we should be able to set up a collaboration with *no* thought, i.e. 1 or 2 commands and be reachable for others in your neighborhood. With minimal effort to everyone. (everyone requires some mechanism for presence, see above)
