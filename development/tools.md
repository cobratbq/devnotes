# Development

## VSCode /VSCodium / Codium

### Extensions

__Globally-enabled__:

- [Bookmarks](<alefragnani.bookmarks>)
- [Editorconfig](<editorconfig.editorconfig>)
- [Material Icon Theme](<pkief.material-icon-theme>)
- [TODO Highlight](<wayou.vscode-todo-highlight>)

__Workspace-enabled__:

- [Go](<golang.go>)
- [Debugger for Java](<vscjava.vscode-java-debug>)
- [Language support for Java](<redhat.java>)
- [Nix IDE](<jnoortheen.nix-ide>)
- [rust-analyzer](<matklad.rust-analyzer>)
- [Test-runner for Java](<vscjava.vscode-java-test>)
- [Verifpal](<symbolicsoft.verifpal>)

### Shortcuts

- `ctrl+p`: goto files, with `@` goto symbol, with `:` goto line.
- `ctrl+shift+o`: goto symbol.
- `ctrl+k z`: zen-mode (comfortable editor width for long-form writing)

## IntelliJ IDEA

- Create symlink `<intellij-base-dir>/idea.jdk` to point to custom JDK, overrides default JetBrains JDK.
- Set `-Dsun.java2d.uiScale.enabled=true` VM option to enable HiDPI mode through JRE  
  <https://intellij-support.jetbrains.com/hc/en-us/articles/360007994999-HiDPI-configuration>
