# Investigation

## Topics

### Allocation guard
Warning/failing guard to detect heap allocations / escapes to heap for functions annotated with directive `//go:noalloc`.

The idea is, Go by default offers memory management and provides the user with flexibility and simplicity for their implementation. In special cases, e.g. particular sensitive areas of an application or library, you might want to closely guard memory use. So if you are assuming that a part of a library or a particular library is allocation-less, then you can now guarantee this by having the compilation fail if an allocation is detected.

- [Dave Cheney: Go hidden pragmas](https://dave.cheney.net/2018/01/08/gos-hidden-pragmas)
- [runtime/HACKING.md](https://github.com/golang/go/blob/d029058b5912312963225c40ae4bf44e3cb4be76/src/runtime/HACKING.md)

