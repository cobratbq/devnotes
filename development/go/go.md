# Go

## Module system (go.mod, go.sum)

- Parts:
  - `go.mod` the list of dependency modules in use by a project as well as some project metadata.
  - `go.sum` the checksum of each of the modules such that we can confirm that the same source code is used every time, even if the source code was an arbitrary commit.
- Getting started:
  - `go mod init github.com/cobratbq/helloworld`
  - Next, when running `go get ...` to get a dependency, it will automatically be included in the module system.
    - `go get github.com/go-chi/chi@v4.0.1` for git tags,
    - `go get github.com/go-chi/chi@master` for git branch,
    - `go get github.com/go-chi/chi@08c92af` for git commit hash.
  - Update existing dependencies using `go get -u <some-package-name>`
- Tips:
  - `go mod tidy` for cleaning up unused dependencies.
  - Substitute imported modules, e.g. during development:
    - `go mod edit -replace github.com/go-chi/chi=./packages/chi` (for replacement contained in local directory)
    - `go mod edit -replace github.com/go-chi/chi=github.com/cobratbq/chi@version` (for replacement in different repository - version required)
    - Remove manual replacement: `go mod edit -dropreplace github.com/go-chi/chi`

## Quirks

- interfaces are fat pointers. Therefore, you may have a non-nil interface, because a _typed nil_ value is inserted. That means, a `nil` test results in non-nil because a `nil`-value for a _known_ (non-nil) type is stored.
- `recover()` can mitigate a panic that is in progress. However, `panic(nil)` cannot be detected, because `recover() == nil` both when no panic is in progress and when `panic(..)` is called with `nil` value.
  - This can be detect in outer function, when a value is not updated in a later statement, meaning that function execution was not completed.

## References

- https://medium.com/@adiach3nko/package-management-with-go-modules-the-pragmatic-guide-c831b4eaaf31
