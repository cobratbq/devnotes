# [Editorconfig](<https://editorconfig.org/>)

## Samples

For a root-config, always include a line on the top: `root = true`

### Editorconfig

```editorconfig
[.editorconfig]
charset = utf-8
trim_trailing_whitespace = true
insert_final_newline = true
```

### Go

```editorconfig
[{*.go,go.mod}]
charset = utf-8
indent_style = tab
end_of_line = lf
trim_trailing_whitespace = false
insert_final_newline = true
```

### Makefile

```editorconfig
[Makefile]
charset = utf-8
end_of_line = lf
indent_style = tab
insert_final_newline = true
```

### Markdown

Use 1 indentation for (nested) unordered lists, and 2 indentations for (nested) ordered lists.

```editorconfig
[*.md]
indent_style = space
indent_size = 2
trim_trailing_whitespace = false
insert_final_newline = true
```

### Rust

```editorconfig
[*.rs]
charset = utf-8
indent_style = space
indent_size = 4
insert_final_newline = true
trim_trailing_whitespace = false
```

## References

- [Formal specification](<https://editorconfig-specification.readthedocs.io/>)
