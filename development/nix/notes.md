# Notes

Notes on the use of Nix (`nix-shell`, `nixpkgs`)

## nix-shell with pinned package index

`shell.nix`
```nix
{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.which
    pkgs.htop
    pkgs.zlib
  ];
}
```

## nix-shell with pinned package index

`shell.nix`
```nix
{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/ff691ed9ba21528c1b4e034f36a04027e4522c58.tar.gz";
    sha256 = "xxx";
}) {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.which
    pkgs.htop
    pkgs.zlib
  ];
}
```