# Web Assembly / WASM

Web Assembly is a minimal assembly-like instruction set for the web such that web applications can be built with "close-to-native" performance characteristics.

## History

Started out as experimental _asm.js_ module, which is a subset of javascript such that Javascript runtimes/JITs would be able to aggressively optimize, using notational syntax that is plain javascript but selected such that the run-times would be able to derive additional types, such as `integer` type, allowing further optimization by the run-time.

## References

- [YouTube: WASM, waPC, and waSCA with Kevin Hoffman](https://www.youtube.com/watch?v=wuCgMTX5fb4) talks about the possibilities with WASM, waPC (Web Assembly Procedure Call) and waSCA. Rust-based Web Assembly run-time, cross-language guest modules, etc.
