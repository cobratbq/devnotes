# Programming

Notes related to programming itself. Specifically the lower-level aspects of writing code, and writing it such that if functions correctly.

# IPC

## Managing API / versions

Wireguard uses a quite elegant mechanism for identifying a particular method of the API and corresponding scheme. The command that is issued has format `<name>=<version>` on a single line. The elegance of this is that it is so minimal that it does not need to change over versions/formats. Everything that follows is predefined for that particular `<name>=<version>` tuple. Through its minimalism, it makes the API usable in almost any case.

# References

- [_Awesome_ list of falsehoolds](https://github.com/kdeldycke/awesome-falsehood/blob/master/README.md)
