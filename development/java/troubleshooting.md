# Troubleshooting

Various notes for troubleshooting Java-related issues.

## Security

### "trustAnchors parameter must be non-empty"

This is caused by the trust-store not being available, _in some sense of the word_ (`java.security.InvalidAlgorithmParameterException: the trustAnchors parameter must be non-empty`):

1. cannot find keystore (jks/pkcs12): system-property `javax.net.ssl.trustStore=<path>`
1. cannot open keytore, e.g. due to type: system-property `javax.net.ssl.trustStoreType=pkcs12`
1. cannot decrypt keystore due to bad/missing password: system-property: `javax.net.ssl.trustStorePassword=<password>`

## Modules & class loading

### Class with cannot be found by FQDN even though it exists / adding compile-time/provided dependency causes ClassCastException with same FQDN / Reflection-based calls fail.

_Symptoms_:
- Class cannot be found by referencing FQDN, even though you might have an instance with that exact class name according to its own metadata (reflection)
- ClassCastException refers to same class in both cases, but still fails to cast. (Indicating class loading issue with multiple duplicates on the class path.)
- Reflection operations fail on instances acquired of the problematic type.

The impression that is given is that the dependency's classes are both present and absent.

_Cause / solution_:

In case the `modules` system is in use, either by Java 8 or through module-based loading in an application server such as JBoss AS 7, then access to unapproved modules is actively prevented.
The effect of this is that one may be able to acquire an instance of a type, however as soon as it is statically referenced, problems occur. This is due to the module not being added as a "_module dependency_".

- Check your JBoss installation for module names in case of system modules that need to be exposed to the deployable. Look for `module.xml` and check the module name which includes the libraries that are needed.
- In your deployable, add the `module dependency` to the manifest:  
```
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-ear-plugin</artifactId>
  <configuration>
    <archive>
      <manifestEntries>
        <Dependencies>org.apache.activemq.artemis</Dependencies>
      </manifestEntries>
    </archive>
    <modules>
      ...
    </modules>
  </configuration>
</plugin>
```
- In your using module, add the same dependency as a `provided` dependency:  
```
<dependency>
    <groupId>org.apache.activemq</groupId>
    <artifactId>artemis-jms-client</artifactId>
    <scope>provided</scope>
</dependency>
```

Alternatively, look into using the `jboss-deployment-structure.xml` document (in case of JBoss) to define module dependencies.
