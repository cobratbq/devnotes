# Maven

# Plug-ins

## Compiler warnings

- Show compiler warnings ([`<showWarnings>true</showWarnings>`](https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#showWarnings))  
_NOTE: some versions of maven-compiler-plugin will not fail on warnings if warnings are not also shown._
- Fail on compiler warnings ([`<failOnWarning>true</failOnWarning>`](https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#failOnWarning))
- Enable most compiler warnings: `-Xlint:all,-deprecation`
- Enable javadoc linter: `-Xdoclint:all/protected`.  
  Exclusions with `-Xdoclint:all/protected,-html/package`. (See [Tools Reference: javac](https://docs.oracle.com/javase/10/tools/javac.htm#JSWOR627).)

## Versions checking

- `versions:display-dependency-updates`, to display possible updates to dependencies.
- `versions:display-plugin-updates` to display updates for used plugins.

## Dependency analyzer

- `dependency:analyze`, to list dependency-related issues.
- Include by default in the build process and fail the build process upon dependency warnings:  
```   <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <version>3.1.1</version>
        <executions>
          <execution>
            <id>analyze</id>
            <goals>
              <goal>analyze-only</goal>
            </goals>
            <configuration>
              <failOnWarning>true</failOnWarning>
	      <ignoreNonCompile>true</ignoreNonCompile>
            </configuration>
          </execution>
        </executions>
    </plugin>
```
  - `ignoreNonCompile` to avoid tripping over runtime dependencies.

## Animal Sniffer (Public API signature verification)

- `animal-sniffer:build`, to create a signature of an API.
- `animal-sniffer:check`, to check current code-base against pre-recorded API signature file.

# Configuration

- For older versions of Maven (probably < 3.3), configure maven to use secure connections to _Maven Central_ repository:  
```
    <!-- force HTTPS access for downloading jars -->
    <repositories>
        <repository>
            <id>central</id>
            <url>https://repo1.maven.org/maven2</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>central</id>
            <url>https://repo1.maven.org/maven2</url>
            <releases>
                <enabled>true</enabled>
            </releases>
        </pluginRepository>
    </pluginRepositories>
```
- [Store passwords in (somewhat) encrypted format](https://maven.apache.org/guides/mini/guide-encryption.html)

# Tricks

- Rebuild project and all its dependencies, but only those modules that are dependencies.  
  `mvn clean install -pl <target-project-groupId>:<target-project-artifactId> -am -amd -DskipTests`

# Notes

- Investigate: Ability to verify that public API has not changed backwards-incompatibly.
