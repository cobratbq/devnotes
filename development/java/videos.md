# Videos

- [Douglas Hawkins - Understanding the tricks behind the JIT](https://www.youtube.com/watch?v=oH4_unx8eJQ) - performance, optimization, language constructs and syntax
- [Douglas Hawkins - A visual introduction to inner-workings of the JVM](https://www.youtube.com/watch?v=JLFjY6Ixct8)
- [Douglas Hawkins - JVM mechanics](https://www.youtube.com/watch?v=a-U0so9FfqQ)
- [Douglas Hawkins - Concurrency concepts in Java](https://www.youtube.com/watch?v=ADxUsCkWdbE)
- [Douglas Hawkins - Java performance puzzlers 2018](https://www.youtube.com/watch?v=zdz_pf22nOI)
- [Paul Turner - User-level threads ... with threads](https://www.youtube.com/watch?v=KXuZi9aeGTw)
