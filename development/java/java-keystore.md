# Java keystore

The Java keystore is a cryptographically secure keystore for storing keys that are used in Java applications.

# Notes

- PKCS12 (PFX) containers can be used as a keystore directly.

# Importing private keys

## Import PKCS12 / PFX private keys

`keytool -importkeystore -srckeystore <pfx-filename> -srcstoretype pkcs12 -destkeystore keystore.jks -deststoretype jks`

# Passwords

Both the keystore and individual keys can have passwords to protect them.

## Password-less keys

To "remove" a password from an individual key, you need to set it equal to the password of the keystore: `keytool -keypasswd -keystore <keystore.jks> -alias my_private_key`

# Tooling

- [Keystore Explorer](https://keystore-explorer.org/) - A graphical tool to inspect and modify java keystores.
