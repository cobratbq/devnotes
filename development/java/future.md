# Future developments

- Valhalla  
  data classes/value types
- Amber  
  syntax simplifications, such as switch statements
- Loom  
  green threads / user-level threads
- Metropolis  
  JVM implemented in Java
- Panama
  FFI improved over standard JNI

# References

- [Simon Ritter - JDK 9, 10, 11 and Beyond: Delivering New Features in the JDK](https://www.youtube.com/watch?v=mFyzyVnYcoY)
