# Security notes

The default password is `changeit`.

## Importing additional CA certificates

To import a new CA certificate into the CA keystore: `"$JAVA_HOME/bin/keytool" -keystore "$JAVA_HOME/jre/lib/security/cacerts" -importcert -alias my-ca-alias -file cacert.cer`

## Security

### Keystore

Relevant properties:

- `javax.net.ssl.keyStore`
- `javax.net.ssl.keyStorePassword`
- `javax.net.ssl.keyStoreType`
- `javax.net.ssl.trustStore`
- `javax.net.ssl.trustStorePassword`
- `javax.net.ssl.trustStoreType`
- `javax.net.debug`

## Troubleshooting

- To list all certificates: `"$JAVA_HOME/bin/keytool" -keystore "$JAVA_HOME/jre/lib/security/cacerts" -list`
- To display (additional) debug information regarding establishing secure connections: `-Djavax.net.debug=ssl`
- A secure connection is not validated: "unable to find valid certification path to requested target"
  - Ensure that some certificate in the chain is a trusted CA certificate in the `cacerts` keystore.
  - Ensure that the full certificate chain is sent. That is, if only a single certificate is sent, then _its_ __direct__ issuer needs to be known and trusted already.  
  _This can be verified by using `-Djavax.net.debug=ssl` and checking the contents of the certificate chain that is printed as debug information._
  - Alternatively, if certificates in the chain are missing (or even if only end cert is sent), import intermediate CA certificates such that (direct) issuer is known in CA certificate store.

## References

- [Secure Coding Guidelines for Java SE](https://www.oracle.com/technetwork/java/seccodeguide-139067.html)
