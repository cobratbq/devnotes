# Best practices

- `TODO: set a base-line for error handling. Describe how deviating from given practice will worsen the experience so has to be a trade-off for something sufficiently valuable.`

These are my Java best practices.

- Show all compiler warnings.
- Fail on all compiler warnings.  
  _For existing code bases, you may need to make an exception to make achieving this goal feasible._
- `serialVersionUID` defined explicitly.
- Use _checked exceptions_ for reasonable exceptional cases/control flow.
- Use _unchecked exceptions_ for unreasonable cases (bad usage, illegal state, things that one should not reasonably expect or should not happen at all).
- Define local variables, fields, parameters, methods, classes as `final` if possible.
- Keep variable scope most-restricted.
- Use enums for fixed number of cases/values/variants.

# Exceptions

- NullPointerException - data does not exist but is expected
- IllegalArgumentException - data provided on method call is bad
- IllegalStateException - internal state of the instance is bad
- NoSuchElementException - requested element is not found
- UnsupportedOperationException - operation is not available (under current circumstances)

# Naming

- `requireXXX` for argument requirements  
  Properties: throws `IllegalArgumentException`.
- `validateXXX` for checked exception  
  Properties: throws some _checked exception_ to emphasize a possible control path that needs to be handled.
- `expectXXX` confirm expectations that the internal state is as expected prior to it being used
  Properties: throws `IllegalStateException`.
- `fieldname()` and other fixed values with accessor without `get` prefix. These are constant values. Java applies same practices for records. (Project Valhalla)
  Properties: constant-time operation.
- `getFieldName()` for accessing variable fields for current value.  
  Properties: constant-time operation.
- `setFieldName()` for accessing variable fields for setting specified value.  
  Properties: constant-time operation.
