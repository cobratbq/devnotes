# IntelliJ IDEA

Editor - Code style - Java:

- _Wrapping and braces_
  - _Method declaration parameters_
    - Uncheck _align when multiline_

Customize keymaps:

- `CTRL+w` close tab
- `CTRL+y` redo

Build, execution, deployment:

- Compiler
  - Uncheck _Add runtime assertions for notnull-annotated methods and parameters_

## Plugins

- Statistics (reporting)  
  Go to `View -> Tool Windows -> Statistics` for statistics.
