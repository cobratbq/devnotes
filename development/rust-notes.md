# Rust notes

- constants (`const`) can be constructed at compile-time. If the function needed for construction is not suitable for const, there is a macro `lazy_init` that can be used as an alternative:
  1. `Cargo.toml`:  
  ```toml
lazy_static = "1"
  ```
  1. At desired code location:  
  ```rust
use lazy_static::lazy_static;

lazy_static! {
  static ref YENC_HEAD_PATTERN: Regex = Regex::new(r"=ybegin part=(\d+) total=(\d+) line=(\d+) size=(\d+) name=(.+)").unwrap();
}
  ```


- Constant values.
  - `const`/`static`
  - [`once_cell`](<https://docs.rs/once_cell/latest/once_cell/>)
    - `OnceCell<T>` for exactly-once initialization (at time of first use)
    - `Lazy<T>` for "lazy" initialization or in case const-initialization is not possible
  - `lazy_static!`: note that this makes a "fake" construction that acts like a reference to the type, so you might encounter typing issues in some edge cases. (prefer `OnceCell`/`Lazy`)

## Suggestions for improvement of warnings/messages

- ...

## Talks

- https://www.youtube.com/watch?v=IPmRDS0OSxM
