# NodeJS/NPM

## Installation

- Install/unpack into local user-writable directory, for up-to-date, mutable global namespace.
- `npm update -g` to update global packages.
- `npm install -g 'angular/cli'` for installation of angular in global.

## Use

- `npm install` to get initial batch of dependencies installed locally in the project.

