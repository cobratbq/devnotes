# Tools

## Primary tools

Commonly used tools.

- [git](https://git-scm.com)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
  - [Jetbrains Toolbox](https://www.jetbrains.com/toolbox/) - manage installations of multiple versions of their tools
  - plugin: CheckStyle-IDEA
- Visual Studio Code - Electron-based IDE
- Ghostwriter - fast, responsive, styled markdown editor
- [VisualVM](https://visualvm.github.io)
- [Atom](https://atom.io)
- [Freeplane](https://www.freeplane.org/) - Mind-mapping
- [7-Zip](https://www.7-zip.org/)
- [Firefox](https://www.mozilla.org/en-US/firefox/)
  - safe, javascript-based PDF viewer
- [Notepad++](https://notepad-plus-plus.org/)
- [Gimp](https://www.gimp.org/)
- [KDiff3](http://kdiff3.sourceforge.net/)
- [LibreOffice](https://www.libreoffice.org/)
- [Python](https://python.org)
- [vlc](https://www.videolan.org/) (a.k.a. VideoLAN)
- [Gitea](https://gitea.io/)
- [amp-what](http://www.amp-what.com) - unicode symbol lookup by description
- [Clementine](https://www.clementine-player.org/) - solid, reliable audio and podcast player

## Other tools / alternatives

Alternatives or rarely used tools.

- [Krita](https://krita.org/)
- [PlantUML](http://plantuml.com/) (IntelliJ IDEA plug-in)
- [YourKit Profiler](https://www.yourkit.com/)
- [SceneBuilder](https://gluonhq.com/products/scene-builder/) - Java GUI editor for JavaFX platform.
- [Proguard](https://www.guardsquare.com/en/products/proguard) ([maven plug-in](https://github.com/wvengen/proguard-maven-plugin)) - Obfuscation tool/maven plug-in for Java source code, both vanilla and Android.
- [Gogs](https://gogs.io/) (alternative to _Gitea_)
- [ConEmu](https://conemu.github.io/) - advanced console for Windows environments

## Android apps

- [Auditor][android-app-auditor] - Android app for performing attestation on an Android phone. The app can be used for both the audittee and the auditor. Attestation tests can be performed peer-2-peer using 2 android phones, and remotely using the attestation.app website.<sup>[[1]][website-attestation.app]</sup>


[android-app-auditor]: https://play.google.com/store/apps/details?id=app.attestation.auditor "Auditor - hardware-based intrusion detection"
[website-attestation.app]: https://attestation.app/ "Device integrity monitoring"
