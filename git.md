# git

## Way of working

- Merge branches as squashed commits. This gets rid of in-development history, modifications that are undone at later time, aborted approaches, bad commit messages, etc.

## Useful command-line arguments

- `-p`: immediately start per-chunk selection, available for `add`, `checkout`, `reset`, etc.

## Tricks

- Merge ignoring space-changes:  
  `git merge -srecursive -Xignore-space-change`
- Merge changes as a single squashed commit (squash merge):  
  `git merge --squash my-feature-branch`
- Find latest common commit between two branches, i.e. at what point did the two branches diverge:  
  `git merge-base origin/<branch1> origin/<branch2>`
- Find all branches that contain a certain commit:  
  `git branch -a --contains 9abde6e09859e7e5c94bf67c9218268bd6c9ce54`
- Find a certain file in history:  
  `git log --all -- "**/thefile.*"`
- Rebase a range of commits, that may include merge-commits, onto another base revision. (For example to skip some commits of a series.)  
  `git rebase -p --onto <commit>^ <commit>` (Example command skips exactly one commit.)
- Find ancestry-path leading to from one commit into another commit/branch.  
  `gitk e764144cb99482dddbac5d2199170bdf218a1e65..origin/master --ancestry-path`
- Visualize diff-tree for a branch, given a base commit.  
  `git diff-tree -r origin/master origin/my-feature-branch`
- Clone into bare git repository, then make bare git repo fetch all heads.
  - `git clone --mirror <repo>`,
  - then to make remote references separately available: `git config remote.origin.fetch 'refs/heads/*:refs/remotes/origin/*'` (this is mainly if the bare repository is used for multiple worktrees)
- Use git "_worktree_" to have several checkouts using the same backing git repository.  
  `git worktree add <path-for-new-checkout>` - add new checkout that uses the same backing repository  
  `git worktree list` - list all known checkouts to this backing repository  
  `git worktree prune` - clean up references to checkouts that have since disappeared
- Use `git config remote.origin.prune true` to configure automatic pruning on fetch for a specific remote, e.g. _origin_.
- Show diff without taking whitespaces into account.  
  _This is very useful in case of line-ending changes, restructuring of code-blocks causing indentation changes, etc._  
  `git diff -w`
- Set distinct URLs for fetching and pushing:  
  _By setting distinct URLs you can avoid doing excess authentication when the operation doesn't really require authenticated access._  
  `git remote set-url <remote> <url>` - sets both URLs.  
  `git remote set-url --push <remote> <url>` - sets URL specifically for push operations.
- Combining contents of two files while preserving history of both:
  1. Rename to same file, each in a separate branch.
  1. Merge all branches together, which will cause conflicts.
  1. Resolve conflicts and finish merge.
  1. Done. `git blame` will now list history of all branches, i.e. history from all original files.
- Squash all existing commits into single commit / start new commit-tree:  
  `git reset $(git commit-tree HEAD^{tree} -m "A new start")`  
  _Commit current HEAD to new commit-tree, then reset HEAD to new tree, hence only single commit containing current state._
- List files at given commit: `git ls-tree --full-tree -r --name-only HEAD`
- `git blame --ignore-rev` to indicate which revisions to ignore when executing `git blame`
- Shallow clones/checkouts: (save space)
  - `git clone --depth=1`: shallow repository clone with depth 1.
  - `git fetch --depth=1; git gc --prune=now`: make fetch (more) shallow, clean up old history.
  - `git fetch --deepen=3`: deepen history with 3.
  - `git fetch --unshallow`: make shallow checkout into checkout with full history.

## Configuration

- Ignoring revisions during `git blame`. (see also `--ignore-rev`) This is primarily useful for commits that contain mass-changes to whitespaces or formatting.
  - `--ignore-revs-file` for pointing to a list of revisions stored in a file, e.g. `.git-blame-ignore-revs`
  - `git config blame.ignoreRevsFile .git-blame-ignore-revs` to configure this for the repository.
- Configure signing key for forced specific subkey: `0xlongcode!` (with exclamation mark)

## Concepts

- `pull` = `fetch` followed by `merge`
- __Fast-forward__ = Merge that does not require joining two branches and therefore can simply just move the HEAD reference to the new ("merge"-) location in the commit graph. _Fast-forward merges_ produce a linear commit history.

## References

- Using a [`.mailmap` file](https://www.git-scm.com/docs/git-check-mailmap) to correlate authors that commit under different identities (either name, email address, or both).
- [Ignoring mass reformatting commits with git blame](https://akrabat.com/ignoring-revisions-with-git-blame/)
- [Git Happens (Jessica Kerr)](<https://www.youtube.com/watch?v=Dv8I_kfrFWw> "YouTube: Jessica Kerr 'Git Happens'") [[2]](<https://www.youtube.com/watch?v=SjlxILg69ek> "YouTube: Git Happens")

