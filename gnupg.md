# GnuPG notes

## Concepts

### PGP trust semantics

- __Marginal trust__ multiple marginal trust signatures (from keys that are fully trusted) are required to make a key fully trusted.
- __Full trust__ a single trust signature (from a key that is fully trusted) is required to make a key fully trusted.
- __Ultimate trust__ like fully trusted, but in addition functions as an introducer into the Web-of-Trust. Any key that is _ultimately trusted_ is immediately accepted/trusted. Hence it needs no signatures from other fully/ultimately trusted keys. Your own keys will be ultimately trusted, such that your own keys are the entry point to the web-of-trust. (Or taken from another perspective, without the notion of _ultimate trust_ you would not have an initial key that you would trust.)

## Tooling / configuration

### Enable gpg-agent on session start

The article assumes that a gpg-agent session is started at user log-in. If this is not the case, this issue needs to be tackled first.

### Configure smartcard as SSH identity

1. Start gpg-agent to service SSH agent requests:
  - _one-time use_:`gpg-agent --enable-ssh-support`
  - _on every start_, edit `~/.gnupg/gpg-agent.conf` and add line `enable-ssh-support`.
1. Redirect ssh to gpg-agent: `export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"`. This may be added as part of the console start-up script if needed.
1. List available ssh keys: `ssh-add -L` (This listing should include any smartcards that are connected to the system.)
  - Identities added with `ssh-add`, will be added in `~/.gnupg/sshcontrol`. The file contains the key, a TTL and, optionally, a confirmation flag.
1. Export card identity into identity file, such as `~/.ssh/id_smartcard.pub`: `gpg2 --export-ssh-key KEY-ID`
1. Use identity: `ssh -i ~/.ssh/id_smartcard.pub user@sshserver.local`

### Configure different identity for specific host

Create or update `.ssh/config`:
```
Host sshserver.local
    IdentitiesOnly yes
    IdentityFile ~/.ssh/id_smartcard.pub
```

## Tricks

- Verify signature without adding public key to keyring: `gpg --no-default-keyring --keyring ./claws-mail-public-key.gpg --verify claws-mail-3.17.5.tar.gz.asc`

## Notes

- Recommended keyserver: `keys.openpgp.org` (although requires verification so does not contain all keys), or `keyserver.ubuntu.com` (no identification requirement).
- Avoid keyserver: `sks-keyservers.net` (no longer maintained).

## References

- [drduh - YubiKey Guide](https://github.com/drduh/YubiKey-Guide)
- [Information Security: What is the difference between full and ultimate trust?](https://security.stackexchange.com/questions/69062/what-is-the-difference-between-full-and-ultimate-trust)
