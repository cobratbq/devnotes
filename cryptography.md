# Cryptography

## Primitives / constructs

- Ciphers
  - Initialization vector (IV)
- Nonce
- MAC
  - HMAC (HMAC-SHA-256)
  - CMAC (CMAC-AES)
- One-way functions
  - Hash-functions
  - Key Derivation Functions
  - Salt
- Signatures
- Zero-knowledge proofs
- AKE
  - DAKE
  - PAKE (SRP, OPAQUE, Dragonfly, CPAKE)

## Properties

- _Semantic security_: where only negligible information about the plaintext can be feasibly extracted from the ciphertext. Specifically, any probabilistic, polynomial-time algorithm (PPTA) that is given the ciphertext of a certain message m {\displaystyle m} m (taken from any distribution of messages), and the message's length, cannot determine any partial information on the message with probability non-negligibly higher than all other PPTA's that only have access to the message length (and not the ciphertext).

## References

- _[Explicit-Formulas Database](https://hyperelliptic.org/EFD/)_ Database of formules for various elliptical curves, with trade-offs for various ratios of addition vs multiplication clock cycle expensiveness.
- _[Big Integer Desgn](https://www.bearssl.org/bigint.html)_ The design of big integers used in [BearSSL] to support the large numbers needed for RSA and elliptic curve cryptography. The article describes multiple implementations with varying characteristics.
- _[Bulletproofs: Short Proofs for Confidential Transactions and More](https://crypto.stanford.edu/bulletproofs/)_
- [CSIDH]() - Post-quantum cryptographic algorithm with good properties but very little review for now.

[BearSSL]: https://bearssl.org/
