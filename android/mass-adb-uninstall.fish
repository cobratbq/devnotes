
function adb--mass-uninstall-admin
  echo 'Uninstalling packages for owner ...'
  for pkg in $argv
    echo -n "$pkg: "
    adb uninstall --user 0 "$pkg"
  end
end

function adb--mass-uninstall-work
  echo 'Uninstalling packages for work profile ...'
  for pkg in $argv
    echo -n "$pkg: "
    adb uninstall --user 10 "$pkg"
  end
end
