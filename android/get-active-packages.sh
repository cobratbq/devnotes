#!/bin/sh
# Get list of packages that are currently enabled for owner.
adb shell pm list packages --user 0 -e | cut -b9- | sort
