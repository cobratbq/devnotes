# Secure set-up of Android device

How to set up an Android device with additional security.

## Install auditing app for remote attestation

An auditing app can verify system integrity. In order to judge impartially whether or not the system is still in a correct state, an additional machine is necessary. This can be either a second (trusted) phone for local attestation, or the remote service offered at [attestation.app].

This mechanism relies on modern hardware that contains certain hardware security features as well as [TOFU] when installed. At a minimum, a [TEE][tee] is required, as mandated by Android 8. If available, an [HSM][hsm] is used - which may be available as of Android 9 devices - as this offers even better integrity guarantees.

__NOTE__ It is best to perform these steps before anything else. You do not need to create an Android account, configure WiFi or anything else. Installation of [Auditor][auditor-app] can be performed through USB cable, so internet access is not a requirement. Only once the software is installed do we briefly need internet in order to _enable remote attestation_. Similarly, the Android phone does not need to be fully up-to-date. If we do the update afterwards, we can use remote attestation to verify that the device's security remains intact.

To get started:

1. Create account at [attestation.app][attestation.app] and log in.
1. Install [Auditor][auditor-app], referenced at [attestation.app][attestation.app], ideally as the very first action.  
  _Auditor relies on [TOFU][tofu], so installing Auditor on a long-running system may mean that the system is already hacked, and you are thus verifying that an already hacked phone does not change._
  1. Download [Android SDK Platform Tools][android-platform-tools] and extract to separate directory.
  1. Enable developer options by tapping Android _build version_ repeatedly.
  1. Enable Android debugging via USB.
  1. Connect Android phone to PC.
  1. Check if Android phone is found: `./adb devices -l`.
  1. Install [Auditor][auditor-app]: `./adb install Auditor.apk`.
1. Start [Auditor][auditor-app] and enable (remote) attestation.
  1. Start [Auditor][auditor-app].
  1. Go to the menu and _Enable remote verification_.
  1. Follow the instructions, scan the QR-code on the [attestation.app] website when logged in to your own account.
1. Configure your preferences on the [attestation.app] website, such as to which email address alerts should be sent and which verification interval is desired.

__NOTE__ the verify first verification will submit data that will be trusted "blindly". Only changes that happen after the initial data submission can be verified.

##  Manage permissions

Go to _Settings_ - _Apps & notifications_, _Advanced_, _Special app access_:
- `Display over other apps` permissions.
- `Modify system settings` permissions.

## Developer options

- Simulate "corner cutout" such that one can identify when fake reboots are shown on the phone. (I.e. they should respect the cut-out configuration.)

## Curiosities in boot process

1. During _AndroidOne_ screen, at least 1 screen flash.
2. During _Nokia_ screen: instant transition for brighter blue to darker blue for Nokia logo characters.

# Notes

- https://github.com/mvt-project/mvt.git


[attestation.app]: https://attestation.app "Attestation.app - Device integrity monitoring"
[auditor-app]: https://github.com/GrapheneOS/Auditor/releases "Auditor app"
[android-platform-tools]: https://developer.android.com/studio/releases/platform-tools "Android SDK Platform Tools"
[tofu]: https://en.wikipedia.org/wiki/Trust_on_first_use "Trust on first use"
[tee]: https://en.wikipedia.org/wiki/Trusted_execution_environment "Trusted Execution Environment"
[hsm]: https://en.wikipedia.org/wiki/Hardware_security_module "Hardware Security Module"
