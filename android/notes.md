# Android

- [CA-certificates git-repository](<https://android.googlesource.com/platform/system/ca-certificates/> "Google Android CA-certificates source")

## Locally-hosted services

Using an (old) Android phone for locally-hosted services.

- __Syncthing__ local network-based synchronization service.
- __OTP__ TOTP/HOTP code generation.

Selective internet access:

- newsreaders (RSS)
- video/chromecast/miracast (YouTube, 

### Considerations

- Do not use accounts. Not needed, little benefit when avoiding internet altogether.
- Controlled/blocked internet access  
  _As the old phone is no longer supported, reduce risk by mostly/completely blocking access to the internet._
  - InviZible Pro - firewall/DNS, supports leading all traffic through custom VPN profile
  - RethinkDNS - firewall/DNS, supports leading all traffic through custom VPN profile

