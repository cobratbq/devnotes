# Publications

- [article "What we learned from Xiami cellphones being blocked in Cuba"](<https://havanatimes.org/features/what-we-learned-from-xiaomi-cellphones-being-blocked-in-cuba/> "Havana Times: What We Learned from Xiaomi Cellphones Being Blocked in Cuba")
- [article "Nokia phones lead the trust rankings](<https://www.counterpointresearch.com/nokia-phones-lead-trust-rankings/> "Counterpoint: Nokia Phones Lead the Trust Rankings")
- ...

