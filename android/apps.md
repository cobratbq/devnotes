# Android Apps

## Recommended

- App sandboxing: Shelter (alternatively: Island)
- Security: Sophos Intercept X, Auditor
- Ad/DNS/Host blocking: [personalDNSfilter](https://www.zenz-solutions.de/personaldnsfilter/) ([github](https://github.com/IngoZenz/personaldnsfilter)) (alternatively: [Blokada](https://blokada.org/) [DNS66](https://github.com/julian-klode/dns66))
- Web Browser: Firefox (alternatively: Fennec, Firefox Focus)
- Twitter: Talon (alternatively: Twitter)
- SMS/messaging: Signal
- RSS feed aggregator: Feeder
- Podcasting: AntennaPod
- Mastodon client: Tusky (alternatively: Fedilab)
- Reddit client: falling for reddit
- Audio/Video: VLC for Android
- Misc: Coffee (always-on display)

## Other

- Social networks: WhatsApp Messenger, WhatsApp Business, Facebook, Facebook Messenger, LinkedIn.

## Security-related changes

- Replace _Gboard_ with _Simple Keyboard_.
- Disable:
  - spell checking (personal/work),
  - auto-fill service (personal/work),
  - advanced location services
  - usage reporting and diagnostics
  - disable Google app
  - disable Private DNS
  - disable Network rating provider
  - disable Printing service
  - disable Chrome
  - disable location history, activity
- Enable _lockdown_ option in shutdown menu: Display - Lock screen - enable Lockdown option.
- Use _Sophos Intercept X_ to evaluate permissions for apps, restrict if needed.
- Use _Sophos Intercept X_ "_App protection_" for security-sensitive apps:
    - Auditor
    - Settings
    - Play store and other app stores
    - personalDNSfilter
    - banking
    - email clients
- Alternatively: use secure segmentation (e.g. Samsung _Secure Folder_) for isolating sensitive applications.
- Wi-Fi:
  - Disable network rating provider / network quality.
- Location:
  - Disable enhancing using bluetooth/wifi even if turned off.

### Shelter - customized work profile

Shelter manages a work profile for isolated app usage. The work-profile can now be used to install untrusted apps into, such that these apps cannot access data in the admin-profile. Through freezing/unfreezing we can run apps without all the other apps being present, hence mostly isolated from each-other as well.

NOTE: _Cloning apps installed in main profile, will not always work reliable. Best to install from app-store or APK._

- Set-up Play Store with Google-account.
- Configure the work-profile preferences:  
  _Go to (regular) settings and visit the various configuration categories to configure the work-profile. Sometimes it asks which profile beforehand, sometimes the list is extended with a section for the work profile._
  - Keyboard, spell check, auto-fill service, etc.
  - Accounts  
    _Google-account in work-profile has limited options, e.g. no synching of calendar, mail, etc._
  - Lock screen
  - Security
  - Disable location services for work profile.
- Configure apps to auto-freeze when not in use, freeze on lock, create unfreeze+launch shortcuts for convenience.

Work-profile:

- Google Play Store + Google-account for app access
  - Simple Keyboard (no fancy features, no leakage if user input)
  - Firefox Focus (don't keep browsing history together with untrusted apps)
  - PDF Viewer (`org.grapheneos.pdfviewer`)
  - Social network apps
- F-Droid
  - etc.

## Permissions

Apps:

- WhatsApp: restrict as much as possible.
  - Advanced permissions: _Picture-in-picture_, 
  - Consider taking photos from gallery, instead of _Camera_ permission. 

Special app permissions:

- _Display over other apps_
- _Picture-in-picture_

## Disabled

- Google Chrome
- Messages
- Support
- Digital wellbeing

_To disable apps, make sure they are not a default app for any category._

## Misc

- Hosts blocklist:
  - [EnergizedProtection](https://github.com/EnergizedProtection/block) - _Energized blu_ is recommended by Blokada
  - [NoTracking](https://github.com/notracking/hosts-blocklists)
  - [StevenBlack](https://github.com/StevenBlack/hosts)

## Firefox

- Disable JIT:
  - `javascript.options.baselinejit` `false`
  - `javascript.options.ion` `false`
  - `javascript.options.asmjs` `false`
  - `javascript.options.wasm` `false`, OR accept wasm as there is significant consideration for security and disable parts:
    - `javascript.options.wasm_baselinejit` `false`
    - `javascript.options.wasm_optimizingjit` `false`

## Tips

- Enable developer mode: tap "build number" until something happens that enables developer mode.
- Enable USB debugging: go into "developer options". Turn on "USB Debugging".
- Uninstall app only for current user:
  - Use _App inspector_ to discover app identity. (or adb shell: `pm list packages`)
  - Start adb shell: `adb shell`
  - Execute: `pm uninstall -k --user 0 <app-id>`, where `<app-id>` is the application ID. (Reinstall with `pm install-existing <app-id>`)
  
## Samsung Galaxy A40

- Apps to be removed: (`adb uninstall -k --user 0 <app-id>`, user `150` for _Samsung Secure Store_.)
  - Samsung Calendar: `com.samsung.android.calendar`
  - Samsung Checkout: `com.sec.android.app.billing`
  - Samsung Contacts: `com.samsung.android.app.contacts`
  - Samsung Galaxy Essentials: `com.sec.android.widgetapp.samsungapps`
  - Samsung Games: `com.samsung.android.game.gametools`, `com.samsung.android.game.gamehome`, `com.samsung.android.game.gos`
  - Samsung Dual Messenger: `com.samsung.android.da.daagent`
  - Samsung Pass auto-fill: `com.samsung.android.samsungpassautofill`
  - Samsung Secure Wifi: `com.samsung.android.fast`
  - Samsung Android Car stub: `com.samsung.android.drivelink.stub`
  - Samsung Kids: `com.samsung.android.kidsinstaller`
  - Samsung Link Sharing: `com.samsung.android.app.simplesharing`
  - Bixby Home: `com.samsung.android.app.spage`, `com.samsung.android.bixby.service`, `com.samsung.android.app.settings.bixby`
  - Chrome customizations: `com.sec.android.app.chromecustomizations`
  - Facebook: `com.facebook.katana`, `com.facebook.system`, `com.facebook.appmanager`, `com.facebook.services`
  - Office: `com.microsoft.office.officehubrow`
  - OneDrive: `com.microsoft.skydrive`
  - Dictionary: `com.diotek.sec.lookup.dictionary`
  - Upday: `de.axelspringer.yana.zeropage`
  - Enhance Gameservice: `com.enhance.gameservice`
  - Automation Test: `com.sec.android.app.DataCreate`
  - Hiya caller ID: `com.hiya.star`
  - Emoji: `com.sec.android.mimage.avatarstickers`, `com.samsung.android.emojiupdater`
- Apps to remove from Secure Store only:
  - Photo 360 editor: `com.sec.android.mimage.gear360editor`
  - Google Maps: `com.google.android.apps.maps`
  - Google Play services for AR: `com.google.ar.core`
  - Google Device Health: `com.google.android.apps.turbo`
  - Facebook: `com.facebook.katana`, `com.facebook.system`, `com.facebook.appmanager`, `com.facebook.services`
  - Samsung Pass auto-fill: `com.samsung.android.samsungpassautofill`

See [third party clean up script for Galaxy A40](https://gist.github.com/joaofl/7671e7e96a760e9b368992d6694a33d9).
