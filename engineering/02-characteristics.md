# Characteristics

## Properties

- _Centralized_/_Decentralized_/_Distributed_:
  - _Centralized_: a single service point (location) serves *all* clients.
  - _Decentralized_: multiple service points (locations) serve any number of clients. Servers are interconnected, basically multiple centralized constructs with servers interconnected.
  - _Distributed_: each participant is both client and server, i.e. they are peers. Peers are connected artibrarily, i.e. connect to best convenience/connectivity with no other special meaning as every participant is equal.
- _Consistency_: `TODO: add decent description`
  - _Eventual consistency_: `TODO: add decent description`
- _Cryptographically secure_: provides guaranteed characteristics that satisfy cryptographic security.
- _Determinism_ ([deterministic system][wikipedia-deterministic-system]): a deterministic system is a system in which no randomness is involved in the development of future states of the system. A deterministic model will thus always produce the same output from a given starting condition or initial state.
- _Homomorphism_: `TODO`
- _Idempotency_: repeating an operation with same input multiple times will not change the outcome.
- _Monotonicity_ ([monotonic function][wikipedia-monotonic-function]): In case the subject, e.g. a _function_, is entirely _non-decreasing_ or entirely _non-increasing_.
- _Mutual (dis)trust_: multiple parties, parties do (not) require to trust each other.
- _Persistence_: storage characteristic for data that is not transient.
- _Transitivity_: `A -> B`, `B -> C` implies `A -> C` for some operation `->`.
- _Validation_: ability to validate, for example the work that was previously performed, the contribution of other parties, etc.
- _Probabilistic_: `TODO: information derived from original value. "Lossy" representation due to nature in statistics i.e. small chance of failure for efficiency.`
- _Informationless/Informationful_: `TODO? how to express that data stored is used for certain operations but is itself not the actual information it represents - not counting derivations such as presence, etc.`
- _(Non)Authoritative_: `TODO: whether or not something is an authority on certain knowledge. For example, cache is non-authoritative for the data it caches. The backing store, on the other hand, is authoritative.`
- _(Im)mutability_: `TODO: data cannot be changed ... create-only.`
- _Updateable_: `TODO: data can be changed after initial creation.` `TODO: is dual of Immutable?`
- _Stateful/Stateless_: `TODO: whether or not data is stored`
- _Confidentiality_: `TODO: ...`
- _Integrity_: `TODO: ...`
- _Authenticity_: `TODO: ...`
- _(Non-)Repudiation_: `TODO: ...`
- _Context-sensitive_: `TODO: ...`
- _(Non-)Malleable_: `TODO: ... (ability to make changes to the input that are reflected in the output, though changes may not always have predictable results).`
- _Incomprehensible_ (value): `TODO: not sure what the right words for this are, but consider homomorphic encryption and how we can do calculations without having access to the original value represented by the blob of data. The information did not disappear but is inaccessible. The value is not interpretable/comprehensible, but it can still be used.`
- _(Un)trusted_: `TODO: given some source of trust, data can be trusted. For example, due to being authenticated through a trusted source.`
- _Completeness_: We can prove everything that is true. That is, every property that we can state, we can also prove.
- _Soundness_: Everything we can prove is true. That is, all the properties that we know how to prove, we can also verify they hold.

Characteristic pairs:

- precision / recall
- verification / validation
- necessary / sufficient
- quality / quantity
- accuracy / precision
- completeness / soundness
- ...

Characteristic alternatives:

- validation vs reconstruction  
  If validation is complicated due to many rules, and the result is strictly defined, then it is often better to reconstruct the result and compare the expected and actual result for exact equality, instead of trying validate the received value based on a complex interplay of rules.

### Transitive nature of characteristics

In addition, there is the transitive nature of these properties given certain implementations. For example, for a data path containing a data cache and a backing store, it is critical that the data path itself is authoritative, meaning that even though the data cache is not authoritative, the backing store is. This reflects back on the data path as a whole. Consistency + reliability of data is determined by the presence of an authoritative store.

Cryptographic security is transitive to the extent that cryptographically secure input results in cryptographically secure output for security-preserving transformations.

## Pairing up characteristics to negate undesirable effects

- _Malleability + Integrity_  
  a malleable construction does not prevent manipulation. By adding the ability to check integrity, one can ensure that even though input is malleable, nothing has in fact been modified. Therefore, malleability is no longer a risk.
- _Integrity + Authenticity_  
  an integrity check means nothing if the integrity mechanism can be updated along with the source data. By adding an ability to check authenticity, one can ensure that the integrity mechanism is provided by a trustworthy source.
- _Confidentiality + Context-sensitivity_  
  Even though a confidential value cannot be produced by itself, it could still be taken out of its context, allowing its semantics to subtly change. By adding the property of context-sensitivity, one can protect against taking data out of its context.
- _Authenticity + Context-sensitivity_  
  Even though a value is authenticated and therefore not easily replaceable, the added property of context-sensitivity prevents it from being taken out of the intended context. Therefore, e.g. values cannot be reused in different sessions at different times, due to the context-sensitivity that is used to pin the value to its original context.
- _Malleability + Incomprehensibility_  
  The property of malleability allows changing inputs such that output correspondingly changes. Incomprehensibility suggests that one can do this without understanding the value. For example, bit-wise modify an encrypted payload such that when decrypted, the result also is changed. Even though the value is not immediately comprehensible, the malleability allows directedly influencing the output.
- ...

## Problem identification and dissection

- Identify required properties
- Identify properties in conflict
- Consider phasing (requirements that need not hold at the same time, i.e. significant at different moments in time)  
  temporal partitioning -> whether or not requirements are co-located in the same temporal partition
- ...

## Trade-offs

- _Perfection/Accuracy_, i.e. solutions that match perfectly for all cases / complete problem-space, may not always be beneficial for performance. That is, facilitating all use cases may require an unacceptably high level of complexity. Look for partial solutions that trade off accuracy against memory-efficiency/speed to (partially) satisfy requirements. (See purpose of _Bloom filters_/_Xor filters_.)
- _Computational efficiency/Storage_, i.e. solutions that are less computationally efficient to the benefit of requiring less storage capacity.

### Trilemmas

Triples of characteristics that cannot all hold simultaneously. For each triplet, choosing two characteristics to be optimal __forces__ the third characteristic to be suboptimal. No trilemma can have all three characteristics optimal at the same time. That is, at the same time. Phasing is sometimes used to work around this limitation.

- [Project management triangle][wikipedia-project-management-triangle]: Time, cost (quality), scope (functionality).
- [CAP theorem][wikipedia-cap-theorem]: consistency, availability, partition-tolerance.
- [Zooko's triangle][wikipedia-zookos-triangle]: human-meaningful, secure, decentralized.
- [Anonymity trilemma][paper-anonymity-triangle]: strong anonymity, low bandwidth overhead, low latency.
- [Blockchain trilemma][blog-cryptopedia-blockchain-trilemma][<sup>2</sup>][blog-modex-scalability-trilemma]: fast/decentralized, secure, scalable.
- Data storage (RAID technology): fast, cheap, good.
- Program safety: unsafe input, native code, unrestricted access.  
  _You lose significant amounts of control/predictability/safety when more than 2 are present._ (Source: WUFFS - wrangling untrusted file formats safely)
- _unknown_(`TODO: re-watch 36c3 video The safety, security and privacy`): safety validation/qualifications, sustained testing/verification, patching/"online-readiness"/online health
  - safety + sustainability: spend significant time for verification, validation, qualification. Cannot cope with quick changes required to adapt to on-line threats.
  - safety + patching: timeliness required for online-readines forces quick safety validation, hence there is no time to sustain an extensive testing/verification regime. (Or otherwise, very costly)
  - sustainability + patching: moving fast in fixing issues to ensure timeliness in on-line setting and applying extensive testing regime leaves no time for safety valdation/qualification.

<!--
- IACR-RealWorldCrypto-2022,session-5 Symmetric Cryptography: security, performance, cost (is this an established trilemma?)
-->

__Question/to-be-investigated: can any one of these trilemmas be mapped onto the triple control-predictability-safety?__ There does seem to be an indication to that, but not sure what the reasoning behind that would be, yet.

### Required characteristics triples

All three elements have significant value. They should be aligned, well-balanced.

- Magic conflict: technological viability (technical) + business case (functional) + creativity : all three concerns aligned. (Source: [YouTube: GDC - The Design of Subnautica](https://youtu.be/7R-x9NSBS2Y?t=2414).)
- [Information Security triad][wikipedia-information-security-triad]: confidentiality, integrity, availability

[wikipedia-deterministic-system]: <https://en.wikipedia.org/wiki/Deterministic_system> "Wikipedia: Deterministic system"
[wikipedia-monotonic-function]: <https://en.wikipedia.org/wiki/Monotonic_function> "Wikipedia: Monotonic function"
[wikipedia-cap-theorem]: <https://en.wikipedia.org/wiki/CAP_theorem> "Wikipedia: CAP theorem"
[wikipedia-zookos-triangle]: <https://en.wikipedia.org/wiki/Zooko%27s_triangle> "Wikipedia: Zooko's triangle"
[wikipedia-project-management-triangle]: <https://en.wikipedia.org/wiki/Project_management_triangle#Project_management_triangle_topics> "Wikipedia: Project management triangle"
[wikipedia-information-security-triad]: <https://en.wikipedia.org/wiki/Information_security#Key_concepts> "Information Security: CIA triad"
[paper-anonymity-triangle]: <https://eprint.iacr.org/2017/954.pdf> "Research: Anonymity Trilemma: Strong Anonymity, Low Bandwidth Overhead, Low Latency — Choose Two"
[blog-modex-scalability-trilemma]: <https://modex.tech/a-brief-overview-of-the-scalability-trilemma/> "A brief overview of the Scalability Trilemma"
[blog-cryptopedia-blockchain-trilemma]: <https://www.gemini.com/cryptopedia/blockchain-trilemma-decentralization-scalability-definition> "Cryptopedia: The Blockchain Trilemma: Fast, Secure, and Scalable Networks"
