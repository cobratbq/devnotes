# Solution constructs

Constructions that solve complex problems.

## Measuring duration (elapsing time)

To measure elapsed time: calculate the difference between two moments in time.

To reliably measure elapsed time, it is critical that "the clock" does not jump back, as this would effectively result in "less time elapsed" after the distance between two timestamps is calculated.

Properties:

- _monotonicity_
- _transitivity_
- ... (should not skip forward either?)

Non-requirements:

- Individual snapshots to represent an actual date/time value for the given moment. (Hence it is also agnostic of timezones.) They may be arbitrary values, as long as the delta is able to describe a period (duration) of elapsed time in the desired unit.

## Distributed, persistent storage for mutually distrusting parties (a.k.a. Blockchain)

_Proof of Work_ or _Proof of Stake_ for selection of the user who may contribute a block to the blockchain.

- _Proof of Work_: a computational "puzzle" must be solved, typically with random success characteristics and a certain difficulty level. The participant who solves the problem, is selected to compute. The random characteristics are there to ensure equal opportunity. The difficulty level ensures that throwing more/better hardware at the problem will not trivialize this _"guard to contribution"_. Given that the computational problem is cryptographically hard to ensure fairness for all and sufficient difficulty is required, solving this puzzle requires a significant amount of energy.
- _Proof of Stake_: "Validators" put in stake to participate in the selection process. If a validator is selected, he may validate the next block. If the validation is fair, his stake will be returned after the job is finished. If the validator misbehaves (i.e. validation is bad, validator is corrupt) he will lose his stake.

Properties:

- _Persistence_: data (very small part for each record) needs to be stored for extended amount of time.
- _Distributed_: stored at multiple locations, no need to trust a single storage location. Locations are eventually consistent.
- _Eventual consistency_: all distributions of the blockchain should eventually be consistent, i.e. in sync.
- _Mutual distrust_: participating parties do not require to trust each-other.
- _Validation_: the blockchain must be validatable such that each party can convince themselves that everything is (still) in order.
- _Cryptographically secured data structure_: the data structure, in which critical data is stored, relies on cryptographic guarantees. (Hence creates trust.)

Non-requirements:

- Large-scale storage, the blockchain need to support only a small amount of data. The data can reference a large blob stored on a specialized storage location.

## Approximate sets (membership)

Imperfect representation of data in a probabilistic (e.g. hashed) format. The representation allows for testing if an element is in the data set. A `false` answer is perfect, i.e. absence is certain. A `true` answer is approximate, i.e. there is a possibility for _false positive_ due to the nature of the data structure.

- _Probabilistic_: `TODO`
- _Deterministic_: `TODO`
- _Informationless_: `TODO`
- _Performance_ (storage)

Trade-offs:

- Performance for informationless

Implementations:

- Bloom-filter
- Cuckoo-filter
- Xor-filters

## Stacked probabilistic complete representation of huge data sets

Use of cascading probabilistic (lossy) data structures as complete representation of huge datasets. Each subsequent data structure compensates for the imperfect nature of the previous data structure.

Properties:

- _Persistence_
- _Deterministic_
- _Completeness_: Given a fixed space of inputs, the stacked data structure can provide complete coverage of values both contained and not contained in the dataset.
- _Informationless_: Given the probabilistic nature, the data structure can only answer queries of presence, not of actual information content.
- Performance (storage) `TODO: ...`

Trade-offs:

- Performance for informationless

### Implementation: CRLite

Taking example of CRLite: data structure for revoked certificates:

- Bloom-filter 1: represents possibly revoked certificates.
- Bloom-filter 2: represents false positive for Bloom-filter 1 (i.e. certificates marked as revoked)
- Bloom-filter 3: represents false positives for Bloom-filter 2 (i.e. false false positives for Bloom-filter 1, a.k.a. certificates that _are_ actually revoked but are falsely registered as false positive).
- Etc.

Due to the statistical nature, we save significantly on space. We cascade the data structure to compensate for statistical imperfections.

Ref: [Introducing CRLite: All of the Web PKI’s revocations, compressed][article-mozilla-crlite], [CRLite at Real World Crypto 2020][youtube-realworldcrypto-crlite]

## Cache

Incomplete container for storing most recently used data entries.

Properties:

- Performance
- Non-authoritative
- Incomplete
- Informationful

Trade-offs:

- Performance for incompleteness

Implementations:

- LRU (least recently used): cache that stores frequently used data in order of recent access. Least recently used entries drop out of the cache.
- Most frequently used `TODO:...`
- "cacheables", e.g. immutable values.

## Cookies

A _cookie_ is a delegated storage solution for keeping server's data about the client on/at the client. The reason is that we either have to store data for each served client on the server - requiring significant amounts of storage space - or we store each bit of information on the corresponding client.

Delegate storage of server's information about the client (session), to each corresponding client.

An interesting side-effect is that the data "disappears" with the corresponding client. It is unpredictable whether or not a client may return. In this form of storage, automatically disappears as the corresponding client disappears.

Properties:

- Centralized (communication, but not storage)
- Stateless
- Persistence
- Informationful
- (Non-)Authoritative
- `TODO:???` clients may disappear, so only client knows until what point the data remains relevant

Trade-offs:

- Statelessness for communication overhead

Implementations:

- [HTTP "cookies"][rfc-2109] ([revised RFC][rfc-2965]): storing data relevant for the server in a client's cookie such that it is served to the server with each new request.<sup>[1][article-cookie-specification-gdpr-compliant]</sup>
- cryptographically secure (session) cookies: store server's data on an established session _securely_ (e.g. confidentially and/or authentically) in a cookie that is passed on to the client for "safe keeping". The client cannot read/modify the cookie. The only thing the client can do is send it back to the server such that the previously established session can be continued. (Note that this is _not_ about session resumption.)

## Cipher

Properties:

- +Confidentiality
- Informationful
- Cryptographically secure
- Deterministic
- Pseudorandom
- Malleable

## Hash

Properties:

- +Integrity
- Informationless
- Probabilistic
- Cryptographically secure
- Deterministic
- Pseudorandom

Trade-offs:

- (Speed?)

## MAC

Properties:

- +Integrity
- Informationless
- Probabilistic
- Cryptographically secure
- Deterministic
- Pseudorandom

## Authenticated Encryption (AE)

Properties:

- +Confidentiality
- +Integrity
- Informationful
- Cryptographically secure
- Deterministic
- Pseudorandom
- Non-Malleable

### ... with Associated Data (AEAD)

Additional properties:

- +Context-sensitive

### ... with key commentment

Properties underlying AEAD.

Additional properties:

- Prevent attacker from finding a second key, i.e. not the original key, that passes authentication. (https://eprint.iacr.org/2020/1153.pdf)

## Session ID

Properties:

- `TODO: ...`

## Self-authenticating address

An address which itself can be used to authenticate its backing server.

Properties:

- +Authenticity
- Informationful
- Cryptographically secure

Implementations:

- [tor '.onion' addresses](https://support.torproject.org/glossary/self-authenticating-address/)  
   The address represents a public key. This public key is used by the server to authenticate itself during communication. Although we might not know who owns the server, we do know that the server controls the private key that matches with the public key in the .onion address.

## Notes

- in-line code either in loop or just expanded/laid out in linear form
- in-line code as substitute for function call to ensure: 1. no overhead, 2. ability to co-optimize caller and function (e.g. drop safety guards to function input, output)
- tail call optimized function-call / for-loop / function call / in-lined code through unrolling loop/in-lining function
  - TCO is an example of how certain code structure characteristics can enable further optimization by the compiler, without having to write low-level code by yourself.
- Data-oriented layout is an optimization over Object-oriented layout. That is, we choose to separate each of the fields into its own array, such that data access is more performant, for which we assume certain ways of how the code works. This could also be done by the compiler - optionally if annotated as such - such that we achieve performance without losing simplicity (i.e. readability, straight-forward representation).


[rfc-2109]: https://tools.ietf.org/html/rfc2109 "RFC-2109: HTTP State Management Mechanism (February 1997)"
[rfc-2965]: https://tools.ietf.org/html/rfc2965 "RFC-2965: HTTP State Management Mechanism (October 2000)"
[article-mozilla-crlite]: https://blog.mozilla.org/security/2020/01/09/crlite-part-1-all-web-pki-revocations-compressed/ "Article: Introducing CRLite: All of the Web PKI’s revocations, compressed"
[youtube-realworldcrypto-crlite]: https://www.youtube.com/watch?v=kED3K57EV6w "YouTube: Real World Crypto conference 2020: session 10"
[article-cookie-specification-gdpr-compliant]: https://baekdal.com/thoughts/the-original-cookie-specification-from-1997-was-gdpr-compliant/ "Article: The Original Cookie specification from 1997 was GDPR compliant"
