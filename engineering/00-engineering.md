<head>
    <style type="text/css">
    body {
      background-color: #131313
    }
    </style>
</head>

# Engineering

A listing of engineering problems and the expected (_conceptual_) properties of a good solution, as well as possible notes the what properties are really needed. This takes a most-restrictive approach.
