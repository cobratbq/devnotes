# Engineering constructs

Engineering constructs that are composable such that the composition provides a fully-functional system. The implementation of such a system may contain many cpus, systems, networks, even geographically distribution.

Constructs:

- _Data access (paths)_, e.g. `TODO: --> presence check --> data in cache --> data in (persistent) store`
  - variations/types of paths:
    - actual values
    - derivations of values: presence of value, aggregates, statistics, metadata, ...
  - types of sources:
    - generator
    - persistent store
    - user input
    - network communication
    - ...
  - __base__: for reliability + consistency, data path must terminate with authoritative store.
  - __step__: for performance, insert non-authoritative stores in between that have chosen performance benefits.
- _Data value transformation_, e.g. encrypt/decrypt, hashing, decision making, neural network training, etc.
  - reverseable? / lossy transformation? (`TODO; define lossy, sorting loses the original chaotic order but none of the actual data - maybe should be determined by representation, i.e. data structure containing the data.`)
  - efficiency? (Big-O classification, computation)
  - ...
- _Data representation transformations_, e.g. in-memory binary to binary encoding/text-based encoding/endianness and vice-versa.
  - reverseable? / lossy transformation? (`TODO: from list to set is lossy, i.e. losing order`)
  - efficiency? (Big-O classification, representation)
- _Infrastructure transitions_, e.g. cache, in-memory, IPC, inter-system communication, network-communication, persistent storage (harddisk, message queue, etc.).

