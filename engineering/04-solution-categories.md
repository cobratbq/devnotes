# Solution categories

Typical software engineering solutions consist of a precise, careful selection of chained/composed solutions such that a total construct solves a particular engineering problem.

The challenge in software engineering isn't in coming up with solutions. It is in being able to select those solutions such that one is able to compose a fully consistent construct and minimizes the number of excess, possibly undesirable, properties. The solution should be as simple as possible, while fulfilling all necessary requirements, i.e. having _all_ desirable properties, while leaving out _all other properties_.

This chapter lists all kinds of categories of solutions known in computer science / software engineering.

_Disclaimer: this document is to index and categorize concepts and lessons learned in projects. Undoubtedly, there is room for improvement and descriptions are not perfectly accurate._

_Attribution: this document uses quotes from their respective linked sources in the descriptions._

## Ciphers

A [cipher][wikipedia-cipher] (or cypher) is an algorithm for performing encryption or decryption—a series of well-defined steps that can be followed as a procedure.

- Block ciphers
  - [AES][wikipedia-AES]
- Stream ciphers
  - [Salsa20][wikipedia-salsa20]
  - [XSalsa20][wikipedia-xsalsa20] with 192-bit nonce.
  - [ChaCha20][wikipedia-chacha20]

## Cryptography

- [Ciphers](#ciphers)
- CSPRNG (Cryptographically secure pseudo-random number generator) in [Randomness](#randomness)
- [Encryption](#encryption)
- Key Agreement
  - [Key Exchange](#key-exchange)
  - Key Retrieval
  - Continuous Group Key Agreement (CGKA)
- [Key Derivation Function](#key-derivation-function)
  - Key derivation
  - Password hashing
- [Message authentication codes](#message-authentication-codes) (MAC)
- [Oblivious transfer][wikipedia-oblivious-transfer] (OT) protocol is a type of protocol in which a sender transfers one of potentially many pieces of information to a receiver, but remains oblivious as to what piece (if any) has been transferred.
- [Oblivious RAM](#oblivious-ram) (ORAM)
- [Oblivious Data Structure](#oblivious-data-structure) (ODS)
- [Private Information Retrieval][wikipedia-private-information-retrieval] (PIR) protocol is a protocol that allows a user to retrieve an item from a server in possession of a database without revealing which item is retrieved.
  - __Examples__: retrieval of _contact lists_ as not to disclose your social network, or retrieval of individual _contact_ as to hide which exact contact your are interested in, or _presence_ such that the server cannot tell which person you would want to contact.  
  _Trivial solution_: download everything, query locally. (Does not scale.)
- Properties (mostly for protocols):
  - [Forward secrecy][wikipedia-forward-secrecy]
  - [Deniability][wikipedia-deniability]

## Data structures

A [data structure][wikipedia-data-structure] is a data organization, management, and storage format that enables efficient access and modification.

- _Specialization_:
  - [Oblivious Data Structure](#oblivious-data-structure)
- Space-efficient probabilistic data structure that is used to test whether an element is a member of a set. False positives: possible, false negatives: impossible.
  - [Bloom-filter][wikipedia-bloomfilter]
  - [Cuckoo-filter][wikipedia-cuckoo-filter]
  - __Xor-filter__ <sup>[1](https://lemire.me/blog/2019/12/19/xor-filters-faster-and-smaller-than-bloom-filters/)</sup>
  - [Conflict-Free Replicated Data Types][wikipedia-CRDT]
- [LSM tree][wikipedia-lsm-tree] is a data structure with performance characteristics that make it attractive for providing indexed access to files with high insert volume, such as transactional log data.
- [Merkle tree][wikipedia-merkle-tree] is a tree in which every leaf node is labelled with the hash of a data block, and every non-leaf node is labelled with the cryptographic hash of the labels of its child nodes. Hash trees allow efficient and secure verification of the contents of large data structures.
- [Cuckoo++ hash table][research-cuckoo++-filter]

## Encoding

Encodings are (alternative) representations of information. The meaning of the information is the same, but the way it looks/is stored, e.g. in memory or on paper, is different.

- _Specialized_:
  - [encryption](#encryption): encryption is the process of encoding a message or information in such a way that only authorized parties can access it and those who are not authorized cannot.
- _Text encodings for binary content_:
  - [Base32][johndcook-base32base64]
  - [Base64][johndcook-base32base64]
  - [Base58][johndcook-base58]
  - [Base85][johndcook-base85]
- _String encodings_:
  - [ASCII][wikipedia-ascii] is a 7-bit character encoding standard for electronic communication.
  - Unicode <sup>[1][youtube-cppcon2019-unicode-rabbit-hole],[2][youtube-blackhatUSA2019-hostsplit]</sup>
    - [Punycode][wikipedia-punycode] is a representation of Unicode with the limited ASCII character subset used for Internet host names.
    - [UTF-8][wikipedia-utf8] is a variable width character encoding capable of encoding all 1,112,064 valid code points in Unicode using one to four 8-bit bytes.
    - [UTF-16][wikipedia-utf16]
- __Floating-point encodings_:
  - [Floating point][wikipedia-floating-point] [IEEE-754][wikipedia-IEEE-754] floating point arithmetic (FP) is arithmetic using formulaic representation of real numbers as an approximation so as to support a trade-off between range and precision.
  - [BFLOAT16][wikipedia-bfloat16] The BF16 format is sort of a cross between FP16 and FP32, the 16- and 32-bit formats defined in the IEEE 754-2008 standard, also known as half precision and single precision.<sup>[1][johndcook-bfloat16]</sup>
  - [Type III Unum: Posit][wikipedia-posit] is a hardware-friendly version of unum where difficulties faced in the original type I unum due to its variable size are resolved. Similar size posits when compared to floats offer a bigger dynamic range and more fraction bits for accuracy.<sup>[1][youtube-posit-next-gen-arithmetic]</sup>
- _Others_:
  - [Endianness][wikipedia-endianness]: depending on the hardware (architecture), data in transit (e.g. inside a computer, on the [network][wikipedia-endianness-networking], ...) may be stored with the least significant bit first, last or otherwise.
  - [Two's complement][wikipedia-twos-complement] representation of integers (in memory).

## Encryption

[Encryption][wikipedia-encryption] is the process of encoding a message or information in such a way that only authorized parties can access it and those who are not authorized cannot.

- [Authenticated encryption (with associated data)][wikipedia-aead] (AE, AEAD): are forms of encryption which simultaneously assure the confidentiality and authenticity of data.
- [Symmetric key encryption][wikipedia-symmetric-encryption]: schemes in which the encryption and decryption keys are the same.
- [Public key encryption][wikipedia-public-key-encryption]: schemes in which the encryption key is published for anyone to use and encrypt messages. However, only the receiving party has access to the decryption key that enables messages to be read.
- [Homomorphic encryption][wikipedia-homomorphic-encryption]: a form of encryption that allows computation on ciphertexts, generating an encrypted result which, when decrypted, matches the result of the operations as if they had been performed on the plaintext.<sup>[1][johndcook-homomorphic-encryption]</sup>
  - [Fully Homomorphic Encryption][wikipedia-fully-homomorphic-encryption] (FHE) is _Homomorphic Encryption_ that supports _arbitrary computation_ on ciphertexts.

## Identifiers

- UUID
  - Version 1 - date-time and MAC-address
  - Version 2 - date-time and MAC address, DCE security version
  - Version 3 - namespace name-based (MD5)
  - Version 4 - random
  - [Version 5][rfc-4122] - namespace name-based (truncated SHA-1 with salt)
  - [Version 6][homepage-uuidv6] - a version 1 UUID with bytes reordered to make it naturally ordered, sortable.

## Key Derivation Function

A key derivation function (KDF) is a cryptographic hash function that derives one or more secret keys from a secret value such as a main key, a password, or a passphrase using a pseudorandom function.

- Argon2
- scrypt
- bcrypt
- yescrypt

## Key Exchange

- Authenticated Key Exchange (AKE)
  - Deniable Authenticated Key Exchange (DAKE)
  - Password-based Authenticated Key Exchange (PAKE)
    - _Specialized_:
      - [Balanced PAKE][rfc-draft-pake-reqs] When both sides of a PAKE store the same representation of the password, the PAKE is said to be "balanced". In a balanced PAKE the password can be stored directly, in a salted state by hashing it with a random salt, or by representing the credential as an element in a finite field (by, for instance, multiplying a generator from a finite field the password represented as a number to produce a "password element").
    - SRP
    - OPAQUE
    - [Dragonfly][rfc-7664]
    - SPAKE2

## Message authentication codes

`TODO: is it really about authenticity? If you don't know the origin of the MAC, the MAC itself is only valuable as integrity check(?)`

[Message authenticaion codes][wikipedia-message-authentication-code] (MAC) is a short piece of information used to authenticate a message—in other words, to confirm that the message came from the stated sender (its authenticity) and has not been changed.

- [Poly1305][wikipedia-poly1305] is a cryptographically secure message authentication code (MAC) created by Daniel J. Bernstein. It can be used to verify the data integrity and the authenticity of a message. A variant of Bernstein's Poly1305 that does not require AES has been standardized by the Internet Engineering Task Force in [RFC 8439][rfc-8439].

## Neural Networks

A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes. Thus a neural network is either a biological neural network, made up of real biological neurons, or an artificial neural network, for solving artificial intelligence (AI) problems.

- [Spiking Neural Network][wikipedia-spiking-neural-network] are artificial neural network models that more closely mimic natural neural networks.<sup>[1][article-spiking-neural-networks-with-pytorch]</sup>
- [Sum-Product Networks][homepage-sum-product-network] neural networks based on addition and multiplication. Addition is used to combine different instantiations of the same property, while multiplication allows for combining (orthogonal) properties.

## Oblivious Data Structure

[Oblivious data structure][wikipedia-oblivious-data-structure] (ODS) is a data structure that gives no information about the sequence or pattern of the operations that have been applied except for the final result of the operations.

## Oblivious RAM

[Oblivious RAM][wikipedia-oblivious-ram] (ORAM) is about constructing mechanisms such that no (side-channel) information is leaked from RAM usage patterns (for sensitive data).

- Path ORAM
- Circuit ORAM

## Pseudorandom number generator

`TODO: also option of Noise functions, though they have slightly different characteristics`

[Pseudorandom number generator][wikipedia-pseudorandom-number-generator] is an algorithm for generating a sequence of numbers whose properties approximate the properties of sequences of random numbers.

- [Mersenne Twister][wikipedia-mersenne-twister]: The Mersenne Twister is a pseudorandom number generator (PRNG). It is by far the most widely used general-purpose PRNG. Its name derives from the fact that its period length is chosen to be a Mersenne prime.<sup>[1][homepage-mersenne-twister]</sup>
- [PCG random][wikipedia-pcg]: A permuted congruential generator (PCG) is a pseudorandom number generation algorithm developed in 2014 which applies an output permutation function to improve the statistical properties of a modulo-2n linear congruential generator. It achieves excellent statistical performance with small and fast code, and small state size.<sup>[1][homepage-pcg]</sup>
- [Xorshift][wikipedia-xorshift] are a class of pseudorandom number generators.

## Randomness

- _Specialized_:
  - [Pseudorandom number generator](#pseudorandom-number-generator)
- [CSPRNG][wikipedia-csprng] (cryptographically secure pseudo-random number generator): a pseudo-random number generator (PRNG) with properties that make it suitable for use in cryptography.
- DRBG (deterministic random bit generator): See [Pseudorandom number generator][wikipedia-pseudorandom-number-generator].

## Serialization

Serialization is the process of translating data structures or object state into a format that can be stored (for example, in a file or memory buffer) or transmitted (for example, across a network connection link) and reconstructed later (possibly in a different computer environment).

Serialization includes [encoding](#encoding)-concerns, but is broader as it is also concerned with possible metadata that needs to be stored with the encoded representation of the data.

- [Apache Avro][wikipedia-apache-avro]
- [Bencode][wikipedia-bencode] (BitTorrent serialization protocol)
- [BSON][wikipedia-bson]
- [Cap'n Proto][homepage-capnproto]
- [FlatBuffers][wikipedia-flatbuffers]
- [JSON][wikipedia-json]
- [MessagePack][wikipedia-messagepack]
- [NestedText][homepage-nestedtext]
- [Protocol Buffers][wikipedia-protocol-buffers] (protobuffers)
- [Thrift][wikipedia-thrift]
- [XML][wikipedia-xml]
- [YAML][homepage-yaml]


[johndcook-base32base64]: https://www.johndcook.com/blog/2018/12/28/base-32-and-base-64-encoding/ "John D. Cook: Base 32 and base 64 encoding"
[johndcook-base58]: https://www.johndcook.com/blog/2019/03/04/base-58-encoding-and-bitcoin-addresses/ "John D. Cook: Base 58 encoding and Bitcoin addresses"
[johndcook-base85]: https://www.johndcook.com/blog/2019/03/05/base85-encoding/ "John D. Cook: Base85 encoding"
[johndcook-homomorphic-encryption]: https://www.johndcook.com/blog/2019/07/04/homomorphic-encryption/ "John D. Cook: Homomorphic encryption"
[wikipedia-utf8]: https://en.wikipedia.org/wiki/UTF-8 "Wikipedia: UTF-8"
[wikipedia-twos-complement]: https://en.wikipedia.org/wiki/Two%27s_complement "Wikipedia: Two's complement"
[wikipedia-bloomfilter]: https://en.wikipedia.org/wiki/Bloom_filter "Wikipedia: Bloom Filter"
[wikipedia-lsm-tree]: https://en.wikipedia.org/wiki/Log-structured_merge-tree "Wikipedia: Log-structured merge-tree"
[wikipedia-endianness]: https://en.wikipedia.org/wiki/Endianness "Wikipedia: Endianness"
[wikipedia-endianness-networking]: https://en.wikipedia.org/wiki/Endianness#Networking "Wikipedia: Endianness: Networking"
[wikipedia-encryption]: https://en.wikipedia.org/wiki/Encryption "Wikipedia: Encryption"
[wikipedia-pseudorandom-number-generator]: https://en.wikipedia.org/wiki/Pseudorandom_number_generator "Wikipedia: Pseudo-random number generator"
[wikipedia-csprng]: https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator "Wikipedia: Cryptographically secure pseudo-random number generator"
[wikipedia-homomorphic-encryption]: https://en.wikipedia.org/wiki/Homomorphic_encryption "Wikipedia: Homomorphic encryption"
[wikipedia-symmetric-encryption]: https://en.wikipedia.org/wiki/Encryption#Symmetric_key "Wikipedia: Encryption: Symmetric key"
[wikipedia-public-key-encryption]: https://en.wikipedia.org/wiki/Encryption#Public_key "Wikipedia: Encryption: Public key"
[wikipedia-pcg]: https://en.wikipedia.org/wiki/Permuted_congruential_generator "Wikipedia: Permuted congruential generator"
[homepage-pcg]: http://www.pcg-random.org/ "Homepage: PCG, A Family of Better Random Number Generators"
[wikipedia-data-structure]: https://en.wikipedia.org/wiki/Data_structure "Wikipedia: Data structure"
[wikipedia-mersenne-twister]: https://en.wikipedia.org/wiki/Mersenne_Twister "Wikipedia: Mersenne Twister"
[homepage-mersenne-twister]: http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html "Homepage: Mersenne Twister Home Page"
[wikipedia-floating-point]: https://en.wikipedia.org/wiki/Floating_point "Wikipedia: Floating-point arithmetic"
[wikipedia-posit]: https://en.wikipedia.org/wiki/Unum_(number_format)#Type_III_Unum_-_Posit "Wikipedia: Unum (number format) - Type III Unum - Posit"
[wikipedia-spiking-neural-network]: https://en.wikipedia.org/wiki/Spiking_neural_network "Wikipedia: Spiking neural network"
[article-spiking-neural-networks-with-pytorch]: https://guillaume-chevalier.com/spiking-neural-network-snn-with-pytorch-where-backpropagation-engenders-stdp-hebbian-learning/ "Article: Spiking Neural Network (SNN) with PyTorch: towards bridging the gap between deep learning and the human brain"
[wikipedia-AES]: https://en.wikipedia.org/wiki/Advanced_Encryption_Standard "Wikipedia: Advanced Encryption Standard"
[wikipedia-salsa20]: https://en.wikipedia.org/wiki/Salsa20 "Wikipedia: Salsa20"
[wikipedia-chacha20]: https://en.wikipedia.org/wiki/Salsa20#ChaCha_variant "Wikipedia: Salsa20: ChaCha variant"
[wikipedia-cipher]: https://en.wikipedia.org/wiki/Cipher "Wikipedia: Cipher"
[wikipedia-xsalsa20]: https://en.wikipedia.org/wiki/Salsa20#XSalsa20_with_192-bit_nonce "Wikipedia: XSalsa20 with 192-bit nonce"
[wikipedia-private-information-retrieval]: https://en.wikipedia.org/wiki/Private_information_retrieval "Wikipedia: Private Information Retrieval"
[wikipedia-utf16]: https://en.wikipedia.org/wiki/UTF-16 "Wikipedia: UTF-16"
[wikipedia-ascii]: https://en.wikipedia.org/wiki/ASCII "Wikipedia: ASCII"
[wikipedia-oblivious-transfer]: https://en.wikipedia.org/wiki/Oblivious_transfer "Oblivious transfer"
[wikipedia-forward-secrecy]: https://en.wikipedia.org/wiki/Forward_secrecy "Wikipedia: Forward secrecy"
[wikipedia-fully-homomorphic-encryption]: https://en.wikipedia.org/wiki/Homomorphic_encryption#Fully_Homomorphic_Encryption "Wikipedia: Homomorphic Encryption: Fully Homomorphic Encryption"
[wikipedia-oblivious-ram]: https://en.wikipedia.org/wiki/Oblivious_RAM "Wikipedia: Oblivious RAM"
[wikipedia-oblivious-data-structure]: https://en.wikipedia.org/wiki/Oblivious_data_structure "Wikipedia: Oblivious data structure"
[wikipedia-poly1305]: https://en.wikipedia.org/wiki/Poly1305 "Wikipedia: Poly1305"
[rfc-8439]: https://tools.ietf.org/html/rfc8439 "RFC 8439: ChaCha20 and Poly1305 for IETF Protocols"
[wikipedia-message-authentication-code]: https://en.wikipedia.org/wiki/Message_authentication_code "Wikipedia: Message authentication code"
[wikipedia-deniability]: https://en.wikipedia.org/wiki/Plausible_deniability#Use_in_cryptography "Wikipedia: Plausible deniability: Use in cryptography"
[wikipedia-aead]: https://en.wikipedia.org/wiki/Authenticated_encryption#Authenticated_encryption_with_associated_data "Wikipedia: Authenticated encryption: Authenticated encrypion with associated data"
[wikipedia-apache-avro]: https://en.wikipedia.org/wiki/Apache_Avro "Wikipedia: Apache Avro"
[wikipedia-bencode]: https://en.wikipedia.org/wiki/Bencode "Wikipedia: Bencode"
[wikipedia-protocol-buffers]: https://en.wikipedia.org/wiki/Protocol_Buffers "Wikipedia: Protocol Buffers"
[wikipedia-flatbuffers]: https://en.wikipedia.org/wiki/FlatBuffers "Wikipedia: FlatBuffers"
[wikipedia-json]: https://en.wikipedia.org/wiki/JSON "Wikipedia: JSON"
[wikipedia-bson]: https://en.wikipedia.org/wiki/BSON "Wikipedia: BSON"
[wikipedia-messagepack]: https://en.wikipedia.org/wiki/MessagePack "Wikipedia: MessagePack"
[wikipedia-thrift]: https://en.wikipedia.org/wiki/Thrift_(protocol) "Wikipedia: Thrift"
[homepage-capnproto]: https://capnproto.org/capnp-tool.html "Homepage: Cap'n Proto"
[wikipedia-xml]: https://en.wikipedia.org/wiki/XML "Wikipedia: XML (Extensible Markup Language)"
[wikipedia-merkle-tree]: https://en.wikipedia.org/wiki/Merkle_tree "Wikipedia: Merkle tree"
[wikipedia-xorshift]: https://en.wikipedia.org/wiki/Xorshift "Wikipedia: Xorshift"
[wikipedia-bfloat16]: https://en.wikipedia.org/wiki/Bfloat16_floating-point_format "Wikipedia: blfloat16 floating-point format"
[johndcook-bfloat16]: https://www.johndcook.com/blog/2018/11/15/bfloat16/ "John D. Cook: Comparing bfloat16 range and precision to other 16-bit numbers"
[wikipedia-punycode]: https://en.wikipedia.org/wiki/Punycode "Wikipedia: Punycode"
[youtube-cppcon2019-unicode-rabbit-hole]: https://www.youtube.com/watch?v=SMSmKg1nApM "YouTube: CppCon 2019: Peter Bindels “Unicode: Going Down the Rabbit Hole"
[youtube-blackhatUSA2019-hostsplit]: https://www.youtube.com/watch?v=hyyVeKdpeUU&list=PLH15HpR5qRsWrfkjwFSI256x1u2Zy49VI&index=19&t=0s "YouTube: black hat USA 2019: HostSplit: Exploitable Antipatterns in Unicode Normalization"
[wikipedia-cuckoo-filter]: https://en.wikipedia.org/wiki/Cuckoo_filter "Wikipedia: Cuckoo filter"
[homepage-sum-product-network]: https://spn.cs.washington.edu/ "Homepage: Sum-Product Networks (University of Washington)"
[rfc-draft-pake-reqs]: https://tools.ietf.org/id/draft-irtf-cfrg-pake-reqs-02.html#rfc.section.3.1 "Requirements for PAKE schemes"
[rfc-7664]: https://tools.ietf.org/html/rfc7664 "RFC 7664: Dragonfly Key Exchange"
[research-cuckoo++-filter]: https://arxiv.org/abs/1712.09624 "Research: Cuckoo++ Hash Tables: High-Performance Hash Tables for Networking Applications"
[homepage-uuidv6]: http://gh.peabody.io/uuidv6/ "Homepage: UUID 'Version 6'"
[rfc-4122]: https://tools.ietf.org/html/rfc4122 "RFC 4122: A Universally Unique IDentifier (UUID) URN Namespace"
[wikipedia-IEEE-754]: https://en.wikipedia.org/wiki/IEEE_754 "Wikipedia: IEEE Standard for Floating-Point Arithmetic (IEEE-754)"
[youtube-posit-next-gen-arithmetic]: https://www.youtube.com/watch?v=JJgT-YphE1Y "YouTube: PLSE Seminar Series: John Gustafson, 'Next Generation Arithmetic for HPC and AI: An Update'"
[wikipedia-CRDT]: https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type "Wikipedia: Conflict-free Replicated Data Types"
[homepage-yaml]: https://yaml.org/ "Homepage: The Official YAML Web Site"
[homepage-nestedtext]: https://nestedtext.org/ "Homepage: NestedText: A Human Friendly Data Format"

