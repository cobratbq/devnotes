.PHONY: all clean

all: engineering.html

clean:
	rm -f engineering.html

engineering.html: engineering/*.md
	cmark -t html engineering/*.md > engineering.html

