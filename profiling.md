# Profiling

## References

["Performance Matters" by Emery Berger](https://www.youtube.com/watch?v=r-TLSBdHe1A) (Strangeloop 2019) - Presentation on the do's and don'ts of applying profiling tools to programs.
