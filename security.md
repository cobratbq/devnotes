# Security notes

Notes on various security concerns.

# Goals

Be and stay in control of the IT landscape.

- Ensure only intended actions are available.
- Ensure only intended data can be accessed.
- Minimizing unintended, unanticipated, unexpected consequences of lack of control.
- Be aware of the distinct concerns, trade-offs to be made
- ...

# Classification of security in IT

A classification of security in IT systems in three categories. These categories have fundamentally different characteristics. The threats themselves may be cross-cutting, hence multiple classes may need to be addressed to resolve certain security risks.

## Software-security

The "intra-software" security, i.e. the security within the software system. (Must be solved inside the software.)

Goals:
- Eradicate/reduce surface area that may be cause of vulnerabilities.

Concerns:
- Architecture/design
- Authn/authz
- Fuzzing
- Security-standards/best practices
- Cryptograhy-standards/best practices
- Side-channels
- Security-related support in frameworks

## Infrastructure-security

The "inter-software" security, i.e. the security between applications. This is broad in the sense that it can be concerned with security between different programs, but extends to software between systems, clusters, data centers, i.e. everything that is infrastructure to software. (Must be solved in/as part of the infrastructure.)

Concerns:
- Network security
- Cross-system authn/authz
  - SSO (single sign-on)

## System/desktop security

If you go for ultimate security, consider _at least_ the following, but there are likely plenty more missing steps necessary.

- __disable__ Intel Turbo Boost (or AMD equivalent) ([Hertzbleed](<https://www.hertzbleed.com>)
- __disable__ SMT (Intel Hyper Threading) (Spectre/Meltdown/etc.)
- __disable__ Intel SGX secure enclave (now considered deprecated technology)
- __enable__ Intel VT / virtualization / DirectIO, for controlled access through hardware
- __enable__ IO-MMU
- __enable__ SMM
- __enable__ W^X / NX-bit for enforced writeable (i.e. data) or executable (i.e. logic/control flow) bytes. This ensures no arbitrary data can be used to redirect program execution.

## Personal security

The security-concerns of the user. (Must be solved at the user, i.e. human actor.)

Goals:
- Prevent/minimize possibilities for security issues to occur.
- Convenience

Concerns:
- Password management
- Second-factor authentication

# Notes on software

## FIDO U2F/FIDO2 use cases

- _gocryptfs_ (dev-branch): FIDO2 token to protect master key of encrypted volumes.
- _OpenSSH 8.2+_: FIDO U2F for `ecdsa-sk` and `ed25519-sk`.
- ...

## Email clients

- Avoid using S/MIME for (openpgp) encryption. It is, as of yet, inherently unsafe. A new standard is being defined that fixes the mistakes currently present.

## PGP/GnuPG

- [GnuPG 2.3+ allows sealing keys with TPM 2.0](https://gnupg.org/blog/20210315-using-tpm-with-gnupg-2.3.html)
  - Sealed private/secret key would have limited algorithm support, i.e. defined by TPM capabilities.
  - Sealed private/secret key can not be reacquired from TPM: all cryptographic operations happen within TPM, so bound to hardware.
  - Would need to manually ensure a second copy (backup copy) is present, e.g. on hardware security key.

# References

- [mozilla wiki: Security/Server Side TLS](https://wiki.mozilla.org/Security/Server_Side_TLS)
- [Mozilla SSL Configuration Generator](https://ssl-config.mozilla.org/)
- [HTTP Security Headers - A Complete Guide](https://nullsweep.com/http-security-headers-a-complete-guide/)
- [Using TPMs to Cryptographically Verify Devices at Scale - Matthew Garrett &amp; Tom D&quot;Netto](https://www.youtube.com/watch?v=EmEymlA5Q5Q)
