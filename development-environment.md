# Development environment

Generally useful/sane settings and short-hands to use in development environments, either IDEs or rich editors, to facilitate the development process.

## Tooling

- Apache Maven 3.5.4
- IntelliJ IDEA 2018.2
- git, gitk
- kdiff3

## Settings

- Use _package-private_ access modifier by default.  
  _There is an unfortunate habit among developers to make many methods and classes public. One of the primary benefits of Object Orientation, encapsulation, gets diluted because of this. By defaulting to package-private, you require yourself to think about every method that you need to make public. Any method that is made public becomes part of the public API, therefore there are consequences/obligations to backwards-compatibility._
- Automatically add `final` to generated code, such as local variable declarations.  
  _Some exceptions exist, however in general there is no need to keep reusing variables. Variables, and parameters, can be made final. Adding the `final` keyword explicitly helps the JVM to be able to perform more aggressive optimizations as it can assume that the "final" element will not change anymore._  
  _It also promotes the developer to think about the application structure more. A `final` variable can only be written to once, and therefore the control flow has to be structured such that only a single write occurs for any of the possible cases._
- Ensure that no code is automatically generated on annotations, such as @Nonnull. Some IDEs generate `IllegalArgumentException`, which is not necessary useful if you expect `NullPointerException` to be generated.  
  _IntelliJ allows choice to: 1. not do anything, 2. generate assertions, or 3. generate `IllegalArgumentException`._
- Set default style of line-ending. Avoid making it OS-dependent.

## Code snippets

- `needtest`: Marker for use during development to mark an implemented section as needing (unit) tests. This allows you to first finish work currently in focus, while not forgetting to write some tests, to consolidate the behavior, later. It can also be used to delay testing such that the code can soak while finishing up the other parts that required this code.  
```
// FIXME write unit tests
```
- `fixme`: Marker for use during development to mark a section as in-development. The work is not done yet. The mark allows focusing on other areas first, without losing track of this area.  
```
// FIXME implement <text>
throw new UnsupportedOperationException("To be implemented");
```
- `tuo`: Throw an _UnsupportedOperationException_ with user-specified message.  
```
throw new UnsupportedOperationException("<to be implemented>");
```
- `tis`: Throw an _IllegalStateException_ with user-specified message.  
```
throw new IllegalStateException("<>");
```
- `tia`: Throw an _IllegalArgumentException_ with user-specified message.  
```
throw new IllegalArgumentException("<>");
```

# References

- [IntelliJ IDEA](https://www.jetbrains.com/idea/)
- [Apache Netbeans](https://netbeans.apache.org/)
- [Eclipse](https://eclipse.org)

