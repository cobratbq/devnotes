# HTTP

## HTTP/2

### Benefits

- HTTP/1 suffers from HTTP-based head-of-line blocking.

## HTTP/3

### Characteristics

- HTTP over [QUIC](quic.md) (protocol on top of UDP packet layer)

### Benefits

- HTTP/2 suffers from TCP-based head-of-line blocking.

## Notes

- How to identify/transition from HTTP/1 and HTTP/2 which live on TCP, to HTTP/3:
  - `Alt-Svc` header indicates `host`, `protocol`, `port` for same _origin_.
- 2019-10-09: Challenges adopting HTTP/3:
  - 3%-7% of attempted QUIC connections fail. (Company policies, firewalls)
  - Fallback algorithm needed.
  - CPU intensive.
  - Unoptimized UDP stacks.
  - "Funny" TLS layer.
  - All QUIC stacks are user-land.
  - No standard QUIC API.
  - Lack of tooling. (Wireshark is up to speed with development)

## References

- [HTTP/3 - HTTP over QUIC is the next generation by Daniel Stenberg](https://www.youtube.com/watch?v=idViw4anA6E) by Daniel Stenberg (@badger) of curl
- [IETF QUIC HTTP/3 draft](https://datatracker.ietf.org/doc/draft-ietf-quic-http/)
