# KCP

Reliable, error-corrected, cryptographically secure transport protocol on top of UDP.

## Characteristics

- No head-of-line blocking due to nature of UDP (connectionless)
- Session identifier independent of network address
- Optional encryption of data
- Optional Foward Error Correction (FEC)
- Reliable (retransmission if not correctable)
- Low latency

## References

- https://github.com/skywind3000/kcp
- https://github.com/xtaci/kcp-go
