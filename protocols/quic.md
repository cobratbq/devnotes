# QUIC

Reliable network transport protocol built on top of UDP.

## Characteristics

- Multiple parallel streams (connections embedded in the QUIC session)
- Connection ID, making it independent from client's current `host`+`port` pair, i.e. roaming, switching networks is possible and QUIC will adapt.
- QUIC as independent transport protocol. (QUIC is not tied to HTTP/3, but HTTP/3 is first application protocol implementation on top of QUIC)
