# Misc

Miscellaneous notes that do not fit in their own category for now.

## Atom

- Modify settings of package `whitespace` to suit your needs.
- Disable module `spell-check` for responsiveness, speed benefits and get rid of the squigly lines.

## Audio

- Transcode m4a to mp3: `faad -w podcast.m4a | lame -h -b 192 - podcast.mp3`
- Extract audio from web: `ffmpeg -i input.webm -vn -acodec copy audio.opus`
- Transcode opus to mp3: `opusdec --force-wav audio.opus - | lame -V2 - audio.mp3`

## Other

- Verify image checksum after it is written to disc: `sudo dd if=/dev/sr0 bs=4M | sha256sum -b -`
- Verify image checksum after it is written to disc with specific size in bytes: `sudo dd if=/dev/sr0 iflag=count_bytes bs=4M count=<bytes> | sha256sum -b -`
- Replace text in file: `sed --in-place= 's/my-text/my-new-text/g' input.txt` (or use `#` as delimiter)

- https://crypto.stackexchange.com/questions/1795/how-can-i-convert-a-der-ecdsa-signature-to-asn-1#1797
