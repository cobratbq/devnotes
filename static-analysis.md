# Static analysis / Quality control

Tools and other kinds of facilities that help in the quest for quality control and improvements.

# Tools / plug-ins

- [SpotBugs](https://spotbugs.github.io/)
- [pmd](https://pmd.github.io/) ([PMD Applied](https://pmdapplied.com/))  
  _NOTE: pmd has strong support for custom XPath-based rules engine_
- [Checkstyle](https://checkstyle.org/)
  - coding style (formatting rules)
  - [ImportControl](https://checkstyle.org/config_imports.html#ImportControl) package import rules (whitelisting/blacklisting)
- [sonarqube](https://www.sonarqube.org/)
- [TICS](https://tiobe.com/tics/tics-framework/)
- [PVS Studio](https://www.viva64.com/en/pvs-studio/)
- [Animal Sniffer](https://www.mojohaus.org/animal-sniffer/animal-sniffer-maven-plugin/) - Analyzer for public API. (Guard against API breakage.)
- [Structure 101](https://structure101.com/)
- [Maven Dependency Analyzer](https://maven.apache.org/plugins/maven-dependency-plugin/) Analyze and enforce dependency rules.
- [ErrorProne][errorprone] Google's Error-Prone compiler plug-in.
  - Static analysis through compiler annotation processor.
  - Extend static analysis capabilities through errorprone-annotations.
  - [NullAway][nullaway] plug-in: Compile-time nullability analysis.

# Annotations enhancing static analysis

- [API Guardian](https://apiguardian-team.github.io/apiguardian) - An annotation that allows the developer to indicate the level of stability on public artifacts.
- [ErrorProne][errorprone] - Error-Prone offers annotations for assisted synchronization, checking or ignoring return values, disallowed method calling, etc.
- [SpotBugs annotations](https://spotbugs.readthedocs.io/en/latest/migration.html#com-google-code-findbugs-findbugs-annotations)
- [JSR-305](https://jcp.org/en/jsr/detail?id=305) - Dormant, JSR was never officially accepted. Supported in IntelliJ, FindBugs, Eclipse.  
  Supported in [NullAway][nullaway].  
  No longer supported in [SpotBugs](https://github.com/spotbugs/spotbugs/pull/180), in favor of _spotbugs-annotations_.
- [IntelliJ annotations](https://www.jetbrains.com/help/idea/annotating-source-code.html)
- [Checker Framework](https://checkerframework.org/) - Up and coming framework for checking source code, per individual aspects.  
  - _Used by Google in Guava._
- __ErrorProne annotations__ - Annotations to indicate various properties that are checked through the ErrorProne compiler annotation processor.

# Notes

- Investigate: capabilities of [SpotBugs annotations as of latest release](https://github.com/spotbugs/spotbugs/tree/3.1.6/spotbugs-annotations/src/main/java/edu/umd/cs/findbugs/annotations).

# History

- [FindBugs](https://github.com/findbugsproject/findbugs)  
  _FindBugs is not actively maintained and FindBugs refers to SpotBugs as "the spiritual successor"._
- [clirr](http://clirr.sourceforge.net/)  
  _clirr allows verification of the public API to ensure that only available public API is used._


[errorprone]: https://errorprone.info/ "Google's Error-Prone compiler plug-in for static analysis"
[nullaway]: https://github.com/uber/NullAway "NullAway: fast annotation-based null checking for Java"