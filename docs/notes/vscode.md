# Visual Studio Code

## Default key bindings

- `CTRL+P` (search files), `CTRL+G` (goto line), `CTRL+SHIFT+O` (outline/search symbol within file), `CTRL+SHIFT+P` (search action)
- `CTRL+,` (settings), also action `Open Settings (JSON)`
- `CTRL+SHIFT+I` (format document)
- ...

## Customized key bindings

- `CTRL+E` (open editors)

## Notes

- User settings: `~/.config/VSCodium/User/` (settings, keybindings, snippets)
