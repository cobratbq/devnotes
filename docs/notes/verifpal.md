# Verifpal Cheat Sheet

- _Attacker_: `attacker[passive]`, `attacker[active]`
- _Constant declarations_: `knows`, `generates`, `leaks`, _assignment_
- _Constants_ properties: _immutable_, _global_, _no referencing_
- _Guarded constant_: `[c]` with `c` a constant. Even if visible to active attacker, it is tamper-proof.
- _PrimitiveSpec_: `Decompose`, `Recompose`, `Rewrite`, `Rebuild`
- _Core primitives_: `Assert(a, b)`?, `Concat(...)`, `Split(x)`?
- _Hashing primitives_: `HASH(...)`, `MAC(k, m)`, `HKDF`, `PW_HASH`
- _Encryption primitives_: `ENC`, `DEC`, `AEAD_ENC`, `AEAD_DEC`?, `PKE_ENC`, `PKE_DEC`
- _Signature primitives_: `SIGN`, `SIGNVERIF`?, `RINGSIGN`, `RINGSIGNVERIF`?, `BLIND`, `UNBLIND`
- _Secret Sharing primitives_: `SHAMIR_SPLIT`, `SHAMIR_JOIN`
- Define a new phase: `phase[1]` (used for modeling active attacker)
- _Queries_: `confidentiality? c`, `authentication? Alice→Carol: c`, `freshness? c`, `unlinkability? c`, `equivalence? a, b`
- ```verifpal
  Bob→ Alice: e[
    precondition[Alice→ Carol: m2]
  ]
  ```
- Queries: ```verifpal
  queries[
    confidentiality? e1
    confidentiality? m1
    authentication? Bob→ Alice: e1
    equivalence? ss_a, ss_b
  ]
  ```
- ```verifpal
  principal Alice [
    knows public c0, c1
    knows private m1
    generates a
  }
  principal Bob[
    knows public c0, c1
    knows private m2
    generates b
    gb = G^b
  ]
  ```
