# Conferences

# General

- [ACCU](https://www.youtube.com/channel/UCJhay24LTpO1s4bIZxuIqKw)
- [Deconstruct](https://deconstructconf.com)
- [DevConf.us](https://devconf.info/us/) - [YouTube](https://www.youtube.com/channel/UCmYAQDZIQGm_kPvemBc_qwg)
- [FOSDEM](https://fosdem.org) - [YouTube](https://www.youtube.com/user/fosdemtalks), [video-directory](https://video.fosdem.org/)
- [Libre Application Summit](https://las.gnome.org/conferences/LAS)
- [LibrePlanet](https://libreplanet.org/2019/)
- [Open Source Firmware Conference (OSFC)](https://osfc.io/)
- PolyConf
- StrangeLoop - [YouTube](https://www.youtube.com/channel/UC_QIfHvN9auy2CoOdSfMWDw)
- [WeAreDevelopers](https://www.wearedevelopers.com/) - [YouTube](https://www.youtube.com/channel/UCSD0dLRGQk_T-D3RvpM5aFQ)
- [X.org Developer's Conference](https://xdc2018.x.org/)

# Games

- [GDC](http://www.gdconf.com/) - [YouTube](https://www.youtube.com/channel/UC0JB7TSe49lg56u6qH8y_MQ)

# Go

- GopherCon - [YouTube](https://www.youtube.com/channel/UCx9QVEApa5BKLw9r8cnOFEA)
- GopherCon SG - [YouTube](https://www.youtube.com/channel/UCazkIMpjghmT8fugD1WF_DQ)
- Golang UK - [YouTube](https://www.youtube.com/channel/UC9ZNrGdT2aAdrNbX78lbNlQ)
- GothamGo - [YouTube](https://www.youtube.com/channel/UCgRpkkcigKZk52JyAOYNs6w)
- [Go Northwest](https://gonorthwest.io/)

# Java

- Devoxx
- [goto Amsterdam](https://gotoams.nl/)
- [JAX London](https://jaxlondon.com/)
- Oracle Code - [YouTube](https://www.youtube.com/channel/UCdDhYMT2USoLdh4SZIsu_1g) (previously _Java One_)

# C++

- [CppCon](https://cppcon.org/) - [YouTube](https://www.youtube.com/user/CppCon) [2019](https://www.youtube.com/watch?v=u_ij0YNkFUs&list=PLHTh1InhhwT6KhvViwRiTR7I5s09dLCSw)
- [LLVMConf](https://llvm.org/devmtg/) - [2018](https://www.youtube.com/watch?v=zfiHaPaoQPc&list=PL_R5A0lGi1AARZysSx4VzpaLAzny4es2e)

# Erlang / Elixer

- [Code BEAM SF 2019](https://codesync.global/conferences/code-beam-sf-2019/)

# Hacking / Security

- CCC
- DEFCON - [2018](https://www.youtube.com/playlist?list=PL9fPq3eQfaaD0cf5c7wkzMoj2kifzGO4U)
- Linux Security Summit
  - [2018](https://events.linuxfoundation.org/events/linux-security-summit-europe-2018/) - [YouTube](https://www.youtube.com/playlist?list=PLbzoR-pLrL6oa4x78bHssxmGAw_ns1Tm2)
- NorthSec (www.nsec.io, https://www.youtube.com/channel/UCqVhMzTdQK5VAosAGkMtpJw)
- [OffensiveCon](https://www.offensivecon.org/) - [YouTube](https://www.youtube.com/c/offensivecon)
- Derbycon (www.irongeek.com)
- Security B-Sides London

# Physics and Mathematics

- [Copernicus Center for Interdisciplinary Studies](https://www.copernicuscenter.edu.pl/en/) - [YouTube](https://www.youtube.com/user/CopernicusCenter)
- [FQXi](www.fqxi.org) - [YouTube](https://www.youtube.com/user/FQXi)
- [The Institute of Art and Ideas](https://iai.tv) - [YouTube](https://www.youtube.com/user/IAITV)
- [The Royal Institution](https://www.youtube.com/user/TheRoyalInstitution)

# Web

- [Angular Connect](https://angularconnect.com/) - [2017 YouTube](https://www.youtube.com/watch?v=9mVfjLhUQl0&list=PLAw7NFdKKYpGUpg7JJ8-PJNMdlrOnmZtN)

# Misc

- [@Scale](https://atscaleconference.com/)
- [All Systems Go!](https://all-systems-go.io/)
  - [ASG 2018 videos](https://media.ccc.de/c/asg2018)
- [Compose::Conference](https://www.composeconference.org) - [YouTube](https://www.youtube.com/channel/UC0pEknZxL7Q1j0Ok8qImWdQ)
- [DLD Conferences](https://www.dld-conference.com/) - [YouTube](https://www.youtube.com/user/DLDconference) conferences on Digital-Life-Design.
- FOSS4G - [2019](https://2019.foss4g.org/) [Videos](https://media.ccc.de/c/foss4g2019)
- FrOSCon 2018
- GNU Tools Cauldron
  - [GNU Tools Cauldron 2018](https://gcc.gnu.org/wiki/cauldron2018) - [YouTube](https://www.youtube.com/watch?v=rHHN3bbNWnY&list=PLOGTP9W1DX5VSSCvk_OApXX_Zil1F87s-)
- Hydra Conferences (@HydraConferences)
- [Linux.conf.au](https://linux.conf.au/) - [2019](https://www.youtube.com/channel/UCciKHCG06rnq31toLTfAiyw)
- [Linux Plumbers Conference](https://www.linuxplumbersconf.org/) - [2018](https://www.youtube.com/channel/UCIxsmRWj3-795FMlrsikd3A)
- PLISS (Programming Language Implementation Summer School) - [YouTube](https://www.youtube.com/channel/UCofC5zis7rPvXxWQRDnrTqA)
  - [PLISS 2019](https://pliss2019.github.io/)
  - [PLISS 2017](https://pliss2017.github.io/)
- [RustFest](https://rustfest.eu) - [Rome 2018](https://media.ccc.de/c/rustfest18rome), [Paris 2018](https://media.ccc.de/c/rustfest18)
- [State of the Map](https://stateofthemap.org/) - [2019](https://2019.stateofthemap.org) [Videos](https://media.ccc.de/c/sotm2019)
- [Usenix LISA18](https://www.usenix.org/conference/lisa18) - [YouTube](https://www.youtube.com/watch?v=WYrZyciNCoY&list=PLbRoZ5Rrl5lduvzqW_-No_Xi5vQzZiu52)
- [Usenix Security 18](https://www.usenix.org/conference/usenixsecurity18) - [YouTube](https://www.youtube.com/watch?v=ajGX7odA87k&list=PLbRoZ5Rrl5lew16BZO_C6wihpBkAYYaxi)
- [Velocity](https://conferences.oreilly.com/velocity/vl-eu) - [YouTube](https://www.youtube.com/watch?v=p3dV93jWWhc&list=PL055Epbe6d5bl0ouX9CtjO1BCJrKbDDvG)
- [VLC Video Dev Days]()
  - [Video Dev Days 2018](https://www.videolan.org/videolan/events/vdd18/) - [YouTube](https://www.youtube.com/watch?v=GqFU3eqsCJI&list=PLQLpBN3oI7E4W5VKzNC7xb59gWQw8ghxA)
- [WineConf 2018](https://wiki.winehq.org/WineConf2018) - [PeerTube](https://peertube.mastodon.host/accounts/wineconf2018/videos)
- XDC - X.org Developers Conference
  - [XDC 2018](https://xdc2018.x.org/#program)
- YOW! Conference - [2018](https://www.youtube.com/channel/UCAvGvvEemkeX8hurdPXr7hA)

# TODO

- LibrePlanet - [LibrePlanet 2019](https://media.libreplanet.org/u/libreplanet/tag/libreplanet-2019-video/)
- Re:Publica
- Logan CIJ
- Linux.conf.au
- DebConf
- Gnome GUADEC
- Hope
