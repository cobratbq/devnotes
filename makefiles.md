# Makefiles

Notes on how makefiles work.

- _make_ looks for: `GNUmakefile`, `makefile`, `Makefile` in order. Otherwise specify a makefile with `-f <makefile>`.
- _make_ determines which goals need to run based on timestamps. Newer prerequisites indicate that later results are out-of-date.
- _Default target_ is the first target encountered in the makefile.
- `make` assume that each target maps to a file on the file system, whether or not it exists at that particular moment.
- `make` determines what to do based on existence of specified targets. `make` may determine that a target does not need executing because file system content is up to date.
- `.PHONY:` special section for listing targets that should not match to files. Typical phony targets are `all`, `clean`, `distclean`, etc. (Alternatively, you can use the old method of setting a dependency `FORCE` to force a target to run.)
  Reasons for not matching:
  - targets that do not exist on the file system,
  - targets that should not be looked up because they _do_ exist but `make` should not match to that file.
- `.SILENT:` special section to silence implicit rules, in case they conflict with rules in the makefile itself.
- Put `-` in front of command to ignore status code. (e.g. `-rm test.txt`)
- Put `@` in front of command to silence command output. (e.g. `@rm test.txt`)
- Use `%` for suffix rules, e.g. `%.c`.
- For Makefile sections, use format:  
  `portfolio.pdf: portfolio.md`, where `portfolio.pdf` is the target "end goal" and `portfolio.md` is the required source (dependency). Use `make portfolio.pdf` to build the specified goal (target), and _make_ will know to check if `portfolio.md` has changed to determine whether work is needed.
- Call a makefile in another directory: `$(MAKE) -C <directory>`, optionally with `-f <makefile>`. `$(MAKE)` is predefined.  
  `-C` ensures that we enter the respective directory first. Then if `-f` is specified, look up this particular makefile, otherwise go with default `Makefile`.
- Disable built-in rules:  
  ```
  MAKEFLAGS += --no-builtin-rules
  .SUFFIXES:
  ```

## Tips

- Use `touch` to update directory's last modified timestamp if it isn't updated automatically.
- `@which afl` to test whether a command, i.e. `afl`, is available for use.
- `$(wildcard *.java)` for simple wildcard expansion in variables, where wildcards do not get expanded automatically. (As opposed to directly in prerequisites where wildcard expansion does happen.)
- `TEST_FILES := $(shell find src/main/test -type f -name '*.java')`, then in prerequisites refer to `$(TEST_FILES)`. This allows recursively searching for changes files.
- Consider not using `tee` in Makefile commands. If the command piped into `tee` fails, scripts will continue due to the status code of the piped `tee`. Instead the user should pipe the output of the full `make` execution to a destination of his choosing.

## References

- [An Introduction to Makefiles](https://www.gnu.org/software/make/manual/html_node/Introduction.html)
- [Using .PHONY for recursive invocations of `make` command](https://riptutorial.com/makefile/example/20703/using--phony-for-recursive-invocations-of--make--command)
