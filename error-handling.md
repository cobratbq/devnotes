# Error and exception handling

Error handling is a common topic in software engineering. It is an even more common topic of discussion in groups of developers, either in companies or open source projects. A common understanding of error handling should be present, such that error are handled in a single consistent way and users of your library or application know what to expect.

# Types of error mechanisms

- different languages: C, Java, rust, go, Haskell
- sentinel values, additional returns, composite type, exception, ...
- efficiency improvements:
  - short-hand-macros-to-return-error

# Kinds of errors

There are multiple kinds of errors. Some errors should never happen and therefore you do not want to be bothered with this on a daily basis. Other errors are expected to happen and you would like to handle them appropriately as handling them is required to produce better software. Different kinds of errors are handled differently.

1. __out-of-scope / out-of-reach (unaddressable) errors__  
  Errors that are either impossible to handle, or are put out-of-scope due to the complexity of handling them consistently / appropriately in all cases, i.e. overreaching.
1. __programming mistakes__  
  Programming errors, either one of two categories:
  * Errors in the use of a method / object / library. There are restrictions to its use, e.g. to its input parameters. If these restrictions are not respected, an error occurs to prevent executing into an impossible state or to a known-bad result.
  * Errors in the implementation of a method / object / library. A mistake was made during the implementation. The error serves as a signal that it needs to be corrected.
1. __exceptional (use) cases, i.e. non-typical-path__  
  Then finally there's the kinds of interventions that can be handled, but cannot (or at least not in all cases) be detected a-priori. This allows the method to be designed around the most common path (typically the "happy-path") and unexpected premature endings are facilitated through "exceptional paths".  
  Reasons for choosing this path (non-exhaustive):
  * Optimizations (detecting a-priori is very expensive)
  * Race conditions (different systems/processes interacting, hence detecting a-priori cannot guarantee correctness)
  * Limited options of the API in use
  * Unpredictable circumstances that cannot be prevented or are expected to be handled gracefully. (e.g. network disconnecting)

Of what _kind_ an error is, is either dictated by the chosen technology or by the architecture decisions.  To handle any error appropriately, one first needs to know what kind of error it is considered. Determining the kind is a necessary step. Failing to do so is an indicating that the right architectural choices have not been made and risks handling errors inconsistently and consequently bad decisions may be made by individual developers.

_Note_: this is not a choice made on a technology. It is based on functional grounds, i.e. with the end-user goal/functionality in mind.

# Variations of error handling

## Primary handling

- NOT handling
  - possibility of occurrence is not clear
  - forgetting to handle
- Handling errors
  - Registration (e.g. logging)
  - Act/Solve (e.g. connection is not available, so re-establish connection)
  - Mitigate (e.g. resort to default value in order to continue)

Handling errors only once. (With exception of DEBUG or TRACE level logging, as this is intended as *secondary* information, instead of primary method of handling.)

## Secondary handling

Apart from handling the error in the appropriate way, you might also want to keep a record/trace of the event. The secondary action should not impact the core intention of the application, but may be used to - for example - keep a debug/trace log for later evaluation.

# Error logging

## Logging levels

The logging level semantics are not strictly defined. Different projects adopt different semantics for each level. The following definitions seem to be quite generally understood.

* __Severe / Error__ A serious error that cannot be overcome and consequently stops progress completely. One can only try to find a way around it, but ignoring the error to continue execution is not an option.
* __Warning__ An issue that was discovered that is quite serious but not serious enough to block all progress. A mitigation is possible such that the function is able to finish. It is, however, important enough to make note of what happened. For example, a calculation that was able to finish but with loss of precision due to less-than-ideal circumstances regarding input data.
* __Info__ FYI. An informational message to explain a certain event that happened in the software. (Mostly to give you a heads-up on the current activity.)
* __Debug / Fine / Finer__ Fine-granular details on the execution. This information is logged as secondary, detailed information. Another, the primary way of handling the error, is present next to the fine-granular logging.
* __Trace / Finest__ Informational "breadcrumbs" that are frequent enough that a full trace of the program execution can be reconstructed. This information is typically only logged on request for troubleshooting purposes.

# What types of errors to handle?

## Considerations in error handling

1. Determine and define the classification into kinds.

- Keep the "footprint" of error handling in mind. Biggest issue in Java: many checked exceptions in method signature.
- For logical boundary, such as library API: a single checked exception type which captures all possible variations.
- Use as-specific-as-possible types, such that you do not unnecessarily have to handle errors.
  - This avoids having to handle possible exception cases for overly generified situations
- Is your error/exception part of your API or your implementation? (Not all errors are part of the API. Some are. Define them as part of the API to avoid "leaky abstractions". Only makes sense, if the interface defines that a certain operation may fail.)

# Defining error handling in a project

1. Define the extent to which errors are handled and what is out-of-scope (requirements):
  - Defines guarantees for the user of the project.
  - Prevents excessive error handling.
  - Defines a common understanding of what should and shouldn't be handled.
  - Ensures consistency throughout the project.
1. Define how various errors are to be handled (implementation):
  - What is your primary way of handling
  - Are there additional concerns, such that they require a secondary way of handling? How? For what reasons?
  - Define error types that are part of the API as opposed to implementation details.

# Error handling scenarios

Examples to illustrate trade-offs to make while doing error handling.

`TODO`

# Error handling in Java

## Best practices

- There is no reason why you should ever catch `Exception`  
  Any type of `Exception` is declared, so can be caught specifically. To catch all possible exceptions, declared and otherwise: `catch (IOException | ...Exception | RuntimeException)`.
  - This ensures that when the method signature changes, you are aware of newly declared exceptional control flows.
- Never throw `Exception` or `RuntimeException` types themselves. Instead always throw subtypes. (Also, __never__ throw `Error` or `Throwable` type.)
- On API boundary, such as library interface types, throw only a single checked exception type.
  - If detailed handling is not needed, then we can handle any exception case with a single catch-block. If exceptions are needed, subtypes of the declared exception can be used.

## Common exception types

- Derivatives of `Error` _system errors such as hardware errors and resource limit errors_
- Derivatives of `Exception` (except for `RuntimeException`) _indicates exception control flows that should be handled/managed_
  - `IOException` _any kind of issue related to input/output, where we assume that this is the kind of input/output that may reasonably go wrong_
    - `ProtocolException` _a subset of input/output where the data is successfully acquired, but it does not follow the prescribed/expected format_
- Derivatives of `RuntimeException` _internal programming error / external use error_
  - `NullPointerException` _an instance is missing but was expected, so a control flow failure that allowed to flow null into an unwanted location_
  - `IllegalArgumentException` _bad/illegal input is provided to the method, so a control flow failure, therefore the method cannot execute successfully_
    - `NumberFormatException`
  - `IllegalStateException` _bad/illegal internal state is encountered, so bad control flow allowed the internal state to get corrupted, therefore the method cannot execute successfully_
  - `UnsupportedOperationException` _an operation is called at a point when this is not allowed/supported_
  - `IndexOutOfBoundsException`
    - `ArrayIndexOutOfBoundsException`

# Exception handling in Go

- `panic` to "panic" for uncorrectable situations  
  This signals a programming mistake and must be corrected. Although a program can recover from a panic, this is typically not recommended.
- returning an additional value of a type `error`
