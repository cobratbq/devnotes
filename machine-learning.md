# Machine Learning

## Kinds

- Neural Networks
- Tsetlin Machine
- Sum-Product Networks (SPN)

## Tsetlin Machine

Pros:
- Discrete nature

Cons:
- Relies on state machine to represent possible transitions for each weight.
- Based on single-bit inputs, so needs conversion of input before can be used. (I.e. compared to neural networks, relevant when benchmarking)

## Sum-Product networks

Consists of _sums_ and _products_. _Products_ are used to combine different features. (Presumably due to orthogonality of multiplication.) _Sums_ are used to combine results from different clusters of same feature. _Inference_ is tractable, i.e. order `P`, polynomial, meaning that it can be used to compute probabilities of specific events happening. (This is significant for explainability.)

_Product_: to express orthogonality, independent features. (Think _bitwise OR_.)  
_Sum_: to express correlation, dependent clusters of single feature. (Think _bitwise AND_.)  
_Max_: ??? not sure where this fits in yet.

_Clusters_: combinations of instantiations of a feature. For example, say you're trying to recognize body parts in a neural network. You might train a subnet to recognize hands. The hand that is the target for this subnet, might be opened, closed and at face view or (slightly) turned. The _sum_ operation, which forms clusters, is used to identify the hand irrespective of in which instance it is present.
_Features_: separate, independent characteristics that a particular subnet is learned on. For example, one feature might recongize hands. Another feature might recognize legs, and a third might recognize heads.

### Discriminative Weight Learning

- Classification

### Generative Weight Learning

- Prediction / generation

### SPN-based optimization

...

### Back-propagation

- Soft inference: marginal back-propagation (error gets extremely diffused)
- Hard inference: MAP States

### Properties

- _Decomposable_: an SPN is decomposable when each product node has children with disjoint scopes.
- _Complete_: an SPN is complete when each sum node has children with identical scopes.
- _Valid_: (__sufficient condition__) an SPN is valid, meaning it encodes a distribution - values add up to one, when the SPN is _decomposable_ and _complete_.
- Any SPN can be converted into a bipartite Bayesian network (Zhao, Melibari, Poupart; ICML 2015)

### Learning

Parameter estimation: estimating weights on edges leading into sum-nodes. (There are no weights on edges leading into product-nodes.)

## References

- [Sum-Product Networks: The Next Generation of Deep Models](https://www.youtube.com/watch?v=QegQUBz99o4)
- [Lec 4,1: Deep Learning, Sum-Product Networks](https://www.youtube.com/watch?v=eF0APeEIJNw)
- [Pascal Poupart, Sum Product Networks, Oct 17 2017](https://www.youtube.com/watch?v=Nm0jNqOnQ2o)
- [Sum-Product Networks: Powerful Models with Tractable Inference](https://www.youtube.com/watch?v=eqclcdmkEQo)

