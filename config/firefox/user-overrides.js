//
// Overrides for Arkenfox hardening (<https://github.com/arkenfox/user.js>)
//
/*
user_pref("pref.name.string", "i am a string"); // strings require quote marks
user_pref("pref.name.boolean", true);
user_pref("pref.name.integer", 0);

user_pref("pref.name", "value") // this will NOT be applied, it is missing a closing ;
user_pref("pref.name.integer", "0"); // this will be applied but NOT do anything (type mismatch)

user_pref("browser.startup.page", 0); // this will be applied and actually work
user_pref("browser.Startup.page", 0); // this will be applied but NOT do anything (incorrect case)
*/
// Personal overrides:
user_pref("mousewheel.default.delta_multiplier_y", 80);

/* 5003: disable saving passwords
 * [NOTE] This does not clear any passwords already saved
 * [SETTING] Privacy & Security>Logins and Passwords>Ask to save logins and passwords for websites ***/
user_pref("signon.rememberSignons", false);

/* 0710: disable DNS-over-HTTPS (DoH) rollout [FF60+]
 * 0=off by default, 2=TRR (Trusted Recursive Resolver) first, 3=TRR only, 5=explicitly off
 * see "doh-rollout.home-region": USA 2019, Canada 2021, Russia/Ukraine 2022 [3]
 * [1] https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/
 * [2] https://wiki.mozilla.org/Security/DOH-resolver-policy
 * [3] https://support.mozilla.org/en-US/kb/firefox-dns-over-https
 * [4] https://www.eff.org/deeplinks/2020/12/dns-doh-and-odoh-oh-my-year-review-2020 ***/
user_pref("network.trr.mode", 5);

/* 0801: disable location bar using search
 * Don't leak URL typos to a search engine, give an error message instead
 * Examples: "secretplace,com", "secretplace/com", "secretplace com", "secret place.com"
 * [NOTE] This does not affect explicit user action such as using search buttons in the
 * dropdown, or using keyword search shortcuts you configure in options (e.g. "d" for DuckDuckGo)
 * [SETUP-CHROME] Override this if you trust and use a privacy respecting search engine ***/
user_pref("keyword.enabled", true);

/* 2030: disable autoplay of HTML5 media [FF63+]
 * 0=Allow all, 1=Block non-muted media (default), 5=Block all
 * [NOTE] You can set exceptions under site permissions
 * [SETTING] Privacy & Security>Permissions>Autoplay>Settings>Default for all websites ***/
user_pref("media.autoplay.default", 5);

// 2810, 2811: (default so commented out, but here for documentation purposes)
// Clear everything but cookies, then sanitize on shutdown.
//user_pref("privacy.sanitize.sanitizeOnShutdown", true);
//user_pref("privacy.clearOnShutdown.cache", true);     // [DEFAULT: true]
//user_pref("privacy.clearOnShutdown.downloads", true); // [DEFAULT: true]
//user_pref("privacy.clearOnShutdown.formdata", true);  // [DEFAULT: true]
//user_pref("privacy.clearOnShutdown.history", true);   // [DEFAULT: true]
//user_pref("privacy.clearOnShutdown.sessions", true);  // [DEFAULT: true]
//user_pref("privacy.clearOnShutdown.offlineApps", false); // [DEFAULT: false]
//user_pref("privacy.clearOnShutdown.cookies", false);
//user_pref("privacy.clearOnShutdown.siteSettings", false);

/* 7002: set default permissions
 * Location, Camera, Microphone, Notifications [FF58+] Virtual Reality [FF73+]
 * 0=always ask (default), 1=allow, 2=block
 * [WHY] These are fingerprintable via Permissions API, except VR. Just add site
 * exceptions as allow/block for frequently visited/annoying sites: i.e. not global
 * [SETTING] to add site exceptions: Ctrl+I>Permissions>
 * [SETTING] to manage site exceptions: Options>Privacy & Security>Permissions>Settings ***/
user_pref("permissions.default.geo", 2);
user_pref("permissions.default.camera", 2);
user_pref("permissions.default.microphone", 2);
user_pref("permissions.default.desktop-notification", 0);
user_pref("permissions.default.xr", 2); // Virtual Reality

/*** [SECTION 9000]: PERSONAL
   Non-project related but useful. If any interest you, add them to your overrides
***/
user_pref("layout.spellcheckDefault", 0); // 0=none, 1-multi-line, 2=multi-line & single-line
user_pref("general.autoScroll", false); // middle-click enabling auto-scrolling [DEFAULT: false on Linux]
user_pref("extensions.pocket.enabled", false); // Pocket Account [FF46+]
