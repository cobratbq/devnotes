#function exists -d "Checks if command exists"
#    if test (count $argv) -lt 1
#        echo "Incorrect use of 'exists' function"
#        exit
#    end
#    type $argv[1] >/dev/null 2>&1
#    return $status
#end

# Consider using this conditional ...
#if status is-interactive
#    # Commands to run in interactive sessions can go here
#end

# Recommend sourcing from local config in `~/.config/fish/config.fish`
set -x LC_ALL 'en_US.UTF-8'
set -x EDITOR /usr/bin/nvim
set -x SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)

# Git (security mitigation: do not accept home dir as git-dir)
set -x GIT_CEILING_DIRECTORIES $HOME

# Java development, JDK and JAVA_HOME environment configuration.
set -x JAVA_HOME "$HOME/apps/jdk-17-HotSpot"
if [ -d "$HOME/apps/jdk-17-HotSpot" ]
    alias JAVA_HOME--jdk-17 'set -x JAVA_HOME "$HOME/apps/jdk-17-HotSpot"'
end
if [ -d "$HOME/apps/jdk-11-HotSpot" ]
    alias JAVA_HOME--jdk-11 'set -x JAVA_HOME "$HOME/apps/jdk-11-HotSpot"'
end
if [ -d "$HOME/apps/jdk-8-HotSpot" ]
    alias JAVA_HOME--jdk-11 'set -x JAVA_HOME "$HOME/apps/jdk-8-HotSpot"'
end
if [ -d "$HOME/apps/jdk-17-OpenJ9" ]
    alias JAVA_HOME--jdk-17 'set -x JAVA_HOME "$HOME/apps/jdk-17-OpenJ9"'
end
if [ -d "$HOME/apps/jdk-11-OpenJ9" ]
    alias JAVA_HOME--jdk-11 'set -x JAVA_HOME "$HOME/apps/jdk-11-OpenJ9"'
end
if [ -d "$HOME/apps/jdk-8-HotSpot" ]
    alias JAVA_HOME--jdk-11 'set -x JAVA_HOME "$HOME/apps/jdk-8-OpenJ9"'
end
# Maven configuration and convenience.
set -x MAVEN_HOME "$HOME/apps/apache-maven"
alias mvn--go-offline 'mvn dependency:sources dependency:go-offline'
alias mvn--check-dependencies 'mvn dependency:analyze'
alias mvn--check-pmd 'mvn pmd:check'
alias mvn--check-versions 'mvn versions:display-plugin-updates versions:display-dependency-updates'
alias mvn--dependency-list 'mvn dependency:list'
alias mvn--dependency-tree 'mvn dependency:tree'
alias mvn--resolve-artifacts 'mvn dependency:resolve dependency:resolve-plugins'
alias mvn--download-sources 'mvn dependency:sources'
alias mvn--download-javadoc 'mvn dependency:resolve -Dclassifier=javadoc'

# Go development.
set -x GOROOT "$HOME/apps/go"
set -x GOPATH "$HOME/.local/share/go"

# Flatpak requires XDG_DATA_DIRS to include the '--system' and '--user' data directories to be set
# for the system to fully incorporate flatpak application, such as the launch icons, paths, etc.
# Depending on how systems set this environment variable, shells might and might not pick it up.
# For example, setting the environment via systemd, logging in through SDDM and subsequently
# running the fish shell, will result in the environment variable not being available/updated in
# console windows. Therefore, we set the variable manually in case the shell fails to pick it up.
# Manually generate the expected value with `flatpak --print-updated-env`
if test -z "$XDG_DATA_DIRS"; and test -x /usr/bin/flatpak
    set -x XDG_DATA_DIRS '/home/danny/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share/:/usr/share/'
end


# Set up nix profile if exists. (source: .nix-profile/etc/profile.d/nix.sh)
if [ -L "$HOME/.nix-profile" ]
    # Set up the per-user profile.
    # This part should be kept in sync with nixpkgs:nixos/modules/programs/shell.nix
    set NIX_LINK "$HOME/.nix-profile"
    # Set up environment.
    # This part should be kept in sync with nixpkgs:nixos/modules/programs/environment.nix
    set NIX_PROFILES "/nix/var/nix/profiles/default $HOME/.nix-profile"
    # Set $NIX_SSL_CERT_FILE so that Nixpkgs applications like curl work.
    if [ -e "/etc/ssl/certs/ca-certificates.crt" ] # NixOS, Ubuntu, Debian, Gentoo, Arch
        set NIX_SSL_CERT_FILE "/etc/ssl/certs/ca-certificates.crt"
    else if [ -e "/etc/pki/tls/certs/ca-bundle.crt" ] # Fedora, CentOS
        set NIX_SSL_CERT_FILE "/etc/pki/tls/certs/ca-bundle.crt"
    else if [ -e "$NIX_LINK/etc/ssl/certs/ca-bundle.crt" ] # fall back to cacert in Nix profile
        echo "WARNING: falling back to ca-certificates bundle in Nix profile."
        set NIX_SSL_CERT_FILE "$NIX_LINK/etc/ssl/certs/ca-bundle.crt"
    else
        echo "ERROR: failed to set up environment variable to ca-certificates bundle."
    end
    set -x PATH "$NIX_LINK/bin:$PATH"
    set -e NIX_LINK
    # Updating for nix-env.
    alias nix--update 'nix-channel --update && nix-env -u && nix-collect-garbage'
end

# Administration.
alias l 'ls'
alias dpkg--list-files 'dpkg-query -L'
alias dpkg--list-packages-by-size 'dpkg-query -Wf "\${Installed-Size}\t\${Package}\n" | sort -n'
alias dpkg--owns 'dpkg-query -S'
alias flatpak--cleanup 'echo "Removing unused runtimes ..."; flatpak uninstall --unused; echo "Cleaning up data of removed packages ..."; flatpak uninstall --delete-data; echo "Repairing and pruning flatpak content ..."; flatpak repair --user'
alias sha256sum--truncate 'sha256sum -b | read -n'
function sha256sum--files-recurse-dirs
    command find $argv[1] -type f -print0 | sort -z | xargs -0 -- sha256sum -b
end
function shred--recurse
    set -l target_location (dirname "$argv[1]")
    if [ "/" = $target_location ]
        echo "Location $argv[1] reduces to '/'"
        return 1
    end
    if [ ! -d "$target_location" ]
        set -l current_command (status current-command)
        echo "$current_command <directory> <shred-arguments>"
        return 1
    end
    command find $target_location -type f -exec shred $argv[2..-1] {} \;
end
# Append server name/address to initiate connection. As no authentication method is preferred, depends on what server offers upon initiating connection.
alias ssh--test-authentication-methods 'ssh -o PreferredAuthentications=none -o NoHostAuthenticationForLocalhost=yes'
alias ssh--sign-file 'ssh-keygen -Y sign -f ~/.ssh/id_ed25519 -n file'
function ssh--verify-file
    command find ssh-keygen -Y verify -f ~/.ssh/allowed_signers -n file -I $argv[1] -s $argv[2]
end
alias sshfs--mount 'sshfs -o direct_io,ServerAliveInterval=10,reconnect'
alias udevadm--reload-rules 'sudo udevadm control --reload && sudo udevadm trigger'
alias restorecon--check-system 'sudo restorecon -Rvn /boot /etc /usr /var /opt /root /srv /mnt /media'
# apt aliases
alias apt--search 'apt-cache search'
alias apt--install 'sudo apt install --no-install-recommends'
alias apt--purge 'sudo apt purge --autoremove'
alias apt--update 'sudo apt update && sudo apt upgrade -y'
alias apt--check-backports 'sudo apt -t bullseye-backports upgrade --assume-no'
alias zypper--install 'sudo zypper install --no-recommends'
alias zypper--update 'sudo zypper dup -y --no-allow-vendor-change --no-recommends'
alias zypper--remove 'sudo zypper remove --clean-deps'
alias dnf--install 'sudo dnf install --setopt=install_weak_deps=False'
alias rpm--installed-packages 'rpm -qa --queryformat "%{NAME}\n"'
alias rpm--installed-packages-sizes 'rpm -qa --queryformat "%{SIZE}\t%{NAME}\n"'
# Check disk usage of core operating system files. (i.e. excluding home directories and ephemeral/temporary directories.)
alias du--operating-system 'echo "Checking operating system disk usage, excluding /home /opt and certain runtime directories."; sudo du -xhc --max-depth=1 --exclude=/home --exclude=/opt --exclude=/dev --exclude=/media --exclude=/mnt --exclude=/run --exclude=/var/run --exclude=/proc --exclude=/sys /'
alias oggenc--high-quality 'oggenc -q7'
alias oggenc--low-quality 'oggenc -q2'
# Transcode ogg file to lower quality (not ideal): `oggdec -o - $srcfile | oggenc -q7 -o $dstfile -`
# Strings of random data.
alias generate-random-string-symbols-n 'tr -dc "[:graph:]" < /dev/urandom | read -n'
alias generate-random-string-alphanum-n 'tr -dc "[:alnum:]" < /dev/urandom | read -n'
alias generate-random-string-alpha-n 'tr -dc "[:alpha:]" < /dev/urandom | read -n'
alias generate-random-string-dec-n 'tr -dc "[:digit:]" < /dev/urandom | read -n'
alias generate-random-string-hex-n 'tr -dc "[:xdigit:]" < /dev/urandom | read -n'
alias generate-random-bytes-n 'cat /dev/urandom | read -n'

# Find patterns
# Find various types of public and private keys.
function find--pattern-keys
    command find $argv[1] -iname '*.pub' -or -iname '*.crt' -or -iname '*.key' -or -iname '*_key' -or -iname '*_key.pub' -or -iname '*.pfx' -or -iname '*.pem' $argv[2..-1]
end
# Find broken symlinks
function find--broken-symlinks
    command find $argv[1] -xtype l $argv[2..-1]
end

# Alias 'vim' to 'nvim' if neovim is present.
set -l nvim_path '/usr/bin/nvim'
if test -f $nvim_path; and test -x $nvim_path
    alias vim $nvim_path
end

alias docker--cleanup 'docker image prune -f -a'

# Configure repository (typically bare) to not accept non-fast-forward pushes and refs deletes.
alias git--config-appendonly-bare 'git config --local receive.denyDeletes true; git config --local receive.denyNonFastForwards true'
# Prune git repository, in particular for shallow repositories to clean up old history.
alias git--prune-shallow 'git fsck --strict && git gc --aggressive --prune=now'

set -x PATH $PATH $HOME/.local/bin $HOME/.cargo/bin $GOROOT/bin $GOPATH/bin $MAVEN_HOME/bin
