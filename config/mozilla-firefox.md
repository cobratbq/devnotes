# Mozilla Firefox

## Addons

- uBlock origin
- uMatrix
- HTTPS Everywhere
- Privacy Badger
- Disconnect
- Livemarks
- OneTab

### Previously

- Firefox Multi-Account Containers

## Configuration

- about:config
  - `privacy.firstparty.isolate`: `true`
  - DNS-over-HTTPS (also in _General_ - _Network Settings_ - _Settings_):
    - `network.trr.uri`: `https://doh.libredns.gr/dns-query`
    - `network.trr.mode`: `2` for DoH preference, `3` for DoH exclusive.
    - `network.trr.bootstrapAddress`: `116.202.176.26` (also LibreDNS.gr) for initial access to nameserver.
