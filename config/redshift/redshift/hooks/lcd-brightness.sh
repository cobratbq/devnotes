#!/bin/sh
case $1 in
period-changed)
	case $3 in
	daytime) brightlight -w 1300;;
	transition) brightlight -w 1100;;
	night) brightlight -w 700;;
	esac
	;;
esac
