# Notes

## Troubleshooting

### WiFi

Possible issues with dropping WiFi connections:

1.  low quality due to large distance
1.  channel bandwidth: 20/40/80 MHz: wide is fast, but also more sensitive to interference
1.  enable WMM: allows significantly scaling up bandwidth
1.  drops due to pro-active disconnection based on signal-to-noise ratio (useful with multiple routers/repeaters, disable "Roaming assistant")
1.  deauth-packets spamming causing disconnect actions to be issued. (Use PMF 802.11 standard for protected management frames, or WPA3.)
1.  channel-range overlapping with military channels (RADAR a.k.a. DFS channels), which might require temporary silencing following protocol.

Remarks:
- ASUS: there seem to be issues (bugs) with station "leaving" similar to station-side deauth. Suggested to try disabling `Universal beamforming` (a.k.a. `implicit beamforming`).

