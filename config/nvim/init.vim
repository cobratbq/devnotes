" Install at ~/.config/nvim/init.vim

" Enable and initialize Plug plug-in management.
" Download Plug: https://github.com/junegunn/vim-plug
" Install plug-ins: `:PlugInstall`
call plug#begin()
"Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'editorconfig/editorconfig-vim'
call plug#end()

" Configure tabstop, shiftwidth and by default expand tabs to spaces.
"   Example vim mode line in comments: (see :help modeline)
"   # vim: ts=4:sw=4:expandtab
set ts=4
set sw=4
set expandtab
set nowrap
set mouse=nv
set number relativenumber
