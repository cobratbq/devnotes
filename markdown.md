# Markdown

Notes for convenience on using markdown for larger documents.

## General

- Use `-` for bulletpoint listings, and `_` for emphasis, such that unique symbols are used, i.e. as opposed to using `*` for both. Non-ambiguous, reading-convenience.
- Use `## <a id="MyHeading"></a>My Heading` to manually add an anchor for later reference using `[Back to My Heading](#MyHeading)`.

## HTML use

- Use `<details>` and `<summary>` tags for HTML-based hideable/showable content. Add `open`-attribute (e.g. `<details open>`) to show content by default.  
  ```html
  <details>
    <summary>Details</summary>
    Something small enough to escape casual notice.
  </details>
  ```

## References

- [MDN Web Docs: `<details>`: The Details disclosure element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/details)
