# Conversions between text formats

## Features

- Repeating same key several times is supported/illegal?
- Next-line continuation?
- allowed (special) characters in keys, values?
- key/value escape characters?
- integer / floating point formats, allowed representations?
- allowed literals for booleans?
- how are whitespaces of various kinds treated?
- multiple notations/structures for "nested" keys (dot-separation, nested tags, etc.)
- transformation syntax/rules/engine?
- order/position of various entries? (TOML recommends all related keys `[fruit.apple]` and `[fruit.pear]` to be in direct sequence.
- Mutations/manipulations based on trivial form.
- Streaming processing possible?

## Formats

- Key-value pairs
- Java properties
- XML
- JSON
- TOML
- YAML
- INI files
- bittorrent binary encoding
- GRPC?
- ...

## Notes

```text
Key-value pairs:
  - need to introduce appropriate nesting if not all on same level.

Java properties:
pom.build.plugins.1.artifactId = abc

xml:
pom.build.plugins.1.(plugin).artifactId = bla
  - need to convert index to label

json: (note: individual entries in list are ID-less)
pom.build.plugins.1.artifactId = bla

toml: (???)
[[pom.build.plugins]]
artifactId = abc

yaml?

Ini-files?

bittorrent binary encoding?

grpc?
```
