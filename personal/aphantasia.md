# Aphantasia

[Aphantasia][wikipedia-aphantasia] is the inability to voluntarily create mental images in one's mind.

## Characteristics

- On range from "_recall_" (calling on/re-experiencing previously stored visual memory) to "_reconstruction_" (rebuilding based on non-visual information) towards "_reconstruction_" extreme.
- Visual information is there mostly for immediate processing. (As opposed to storage for later recall.)
  - Limited capability of visual imagination.
  - Limited capability for visual memory.
- Learning disability: (potentially large amounts of) information for one-time use such as during tests are harder to process/work with.  
_Speaking from experience, but I did not know that at the time._
  - Data tables, models, diagrams, etc. In particular, unexpected twists in data and visualization that warrant close attention. Complicated, inaccurate, sloppy, convention-breaking, error-prone representations, because it would require remembering particularities of the representation that are a peculiar. Without the ability to visually recall from memory, one needs to physically go back and confirm/investigate the representation again.
  - _Note_: scenario descriptions such as for physics questions are less of a problem (or at least, for me) because these descriptions contain specific factual data which are the lead information to be used in the question, rather than the visual description.


## Consequences

- Visual memory contains a significant amount of past experiences (such as feelings):
  - "Living in the future" as opposed to "living in the past".
  - Reduced ability to control sudden, strong emotions. Past memories can help to mitigate/reduce in-the-moment emotions using memories from past events. Assuming the vast amount of past memories is positive, it would dampen the strength of emotions in-the-moment.
  - Reduced ["ability to hold grudges"][aphantasiacom-discussion-holding-grudges]. Having less (intense) hooks into past events, it is easier to _forget the grudge_. Note that this should not be confused with the forgetting/forgiving the injustice itself.
  - Reduced lasting impact of graphic/horrific events of the past. (Horror-movies, graphic scenes, etc.)
- Limited capability to imagine/imagination:
  - Reduced ability to enjoy books, specifically ones that rely on reader's imagination to fill in gaps.
- Trained to work with behavioral, auditory data to compensate for limited/lacking visual input.
  - Identifying and recognizing a person through behavioral and auditory cues:
    - tonal changes when speaking.
    - emotions when speaking, such as enthusiasm/reservedness.
    - care/time taken in choosing words.
    - the speed/readiness with which a person may start to ponder, laugh, show concern, etc.
  - Empathy based on person's expression rather than own past experiences.
  - `TODO: continue here ...`
  - cues toward intentions of the other person, such as urgency, honesty, etc. (`TODO: really relevant?`)
  - `TODO: continue here ...`
- Trained to work with information, information derivation, abstract thinking, information processing for purpose of "reconstruction" of memory:
  - `TODO: continue here ...`
  - trained at handling facts, due to need to reconstruct - at least visual info. (`TODO: ???`)
  - skill in abstract thinking (`TODO: ???`)


```
- emotion/empathy:
  - not averaged over time through other experiences (lack of visual memory significantly reduces influence/reduction through past experiences)
  - mirrors/reflects other parties (through audio/behavioral cues) instead of recall from visual memory
  - emotional response tied to party, i.e. not angry with everyone, so able - easier - to switch/turn-off/mitigate when talking to other people.

- false impressions/accusations:
  - "schizophrenic"/"psychopath", due to able to switch emotional intensity strongly based on other participants, i.e. moving away from people onto which emotions were aimed. (response is natural/associated, instead of deviously coordinated act of influence. Easy to let go of individual conflict due to lack of visual memory "fueling the grudge".)
  - 
```

-------------------------------------------------------


- creativity is very much original due to production based almost exclusively on construction from facts/rules (methodical application of abstract rules), rather than "copy-pasting" from recall (copying literal parts from readily-available memory).
  - also, hard to memorize certain types of "dry" information - loose facts not easily tied to your existing foundation of knowledge (studying for tests, etc.)
  - also, hard to work with models, images during tests. (Have to re-read/re-analyze models multiple times.)
  - (I wonder if) the extension of this ability to reconstruct from facts, is the ability to "simulate/extrapolate into the future" due to this taking only the current state and (repeatedly) applying known information with consideration of some predictions. I'm not saying others cannot do this, but rather that it's the go-to tool if you have very limited ability to recall(???) This might also explain my "ease in understanding and accepting" evolutionary theory, as it would be just applying the same trick to biological development.
- due to lack of visual support, depends very much on behavioral cues (for recognizing people). (I.e. will not (easily) identify/remember person by vision, but will by behavior.)
  - I find that I am able to remember the non-visual cues and reconstruct an image - with trial-and-error - that would produce the same non-visual cues. (Fixed-point)  
  E.g. I try to imagine someone's visual image of an emotional response and in my mind I'm slightly "fine-tuning"/tweaking the visual image during reconstruction.
- not distracted by massive amounts of visual information that does not add much? (I.e. additional value of visual information is limited, therefore less distracted by it?)
- thinking based on auditory processes (elapsing time, comparing options, etc.)

## Notes

- use of behavioral cues for identification, prediction.
- use of analyzed/derived information for reconstruction of visual imagery to minimal necessary useful level (or below :-P).
- repeated reads of images, such as complex pictures, formal models, etc. to actually (try to) absorb all the direct and indirect information visible from it.
- significantly less problems with (abstract) algebraic math compared to geometry (trigonometry?), which uses a visual component for comprehension.
- "scary stories/projects" are less scary due to  derived information provides less emotional payload?
- aphantasia (consequently in extreme of "reconstruction" on scale `recall <---> reconstruction`) seems to be similar to Feynman's observation on the way some people cannot maintain awareness of time while speaking (auditory?), while other people cannot maintain awareness of time while reading (visual?). (`TODO: Feynman remarks this in one of his interviews on YouTube, but I'm not sure which one.`)
  - I must be predominantly auditory, also due to good awareness of timing during talks.
- aphantasia -> minimal visualization -> ruins enjoyment of books (fiction)?
- aphantasia makes you "immune" to spoilers?
  - When I watch a movie/series it is very much for the visual expression. Knowing certain facts doesn't bother me, either because their presence is not very dominant - I would vaguely recollect and ignore again - or the single fact does not provide much "visual enjoyment", i.e. it doesn't spoil very much.
- I suck at adventure games X-D ...  
  I wonder if this is due to the fact that I cannot recall scenes I don't currently visit and therefore cannot use my visual memory to "investigate" the current puzzle/search for the missing piece.
- I have noticed that for a long time, whenever I look at a face - look a person in the eyes - I tend to wait for a change in expression, or rather a change therefore an expression. (Remember by emotion, rather than visual/picture-representation.)
- Benefit of being less distracted by the appearance of someone and instead more focused on the characteristics/attitude (initial and subsequent) of the person.
- Benefit of having memories based on who someone is (emotional, sometimes via auditory information), rather than whether they had a bad hairday.
- Open questions:
  - Lack of (strong) memory to explain more objective position, i.e. more based on facts than past experience(??? very circumstantial but would be interesting in questionair.)
  - How does the absence of (visual) memory influence the experience of illness? (E.g. once you have had a bad flu a few times, will your memory of past experiences automatically make your current flu feel worse? The intensity of visual memory with the symptoms of illnesses.)
- The term "aphantasia" confirms my earlier questioning, specifically a question I still wonder about: "Given that you look at a face and absorb the information you get from it: How much of what you see is visual information? How much is auditory/emotional/sensory information derived from it?" Like, what's the ratio of the visual component compared to the rest?
  - E.g. if someone looks angry, there is the visual component which is the way the face contorts, but there is also the emotional component of the anger, and the interaction - the disapproval - in response to what you were discussing with them.
- I've lived in this apartment for over 2 years. Not an hour ago, I last visited the restroom. There is a small sink available for washing your hands, by the brand "Sphinx". Even more than a year ago I decided that I liked the logo they printed on it, for its simplicity. A certain elegance in how a few lines are sufficient to illustrate the sphinx-logo. The logo contains a few lines that end just short of connecting to the other lines in the logo. I would not be able to tell you which lines those are, or to reproduce the logo exactly. I know the lines and curves are - for the most part - smooth, than there are probably two or three lines that end just short of reconnecting. I know it resembles a sphinx, obviously. The logo is maybe 2 cm x 2 cm in dimension. If I would need to reproduce it now, I would be guessing based on these facts. I hardly have an image available "in my mind" from which I can copy. (As I am writing this I am trying to visualize ... basically, this is it.)

`TODO: unrelated observation: in old interviews people talk about _science_-fiction (emphasis on "science"), while in modern society people talk about science-_fiction_ (emphasis on "fiction"): as if to say, previously it sparked peoples curiosity because the fiction was based on science, and now they're saying ... yeah whatever, it's only fiction anyways.`

## References

- [BBC: Interview Ed Catmull][bbc-ed-catmull-pixar]
- [YouTube: Ted-talk on aphantasia][youtube-ted-tamara-alireza-aphantasia]
- [YouTube: Aphantasia AMA][youtube-adam-zeman-ama]
- [YouTube: Ways of thinking - Richard Feynman][youtube-feynman-ways-of-thinking]
- [Wikipedia: Aphantasia][wikipedia-aphantasia]


[bbc-ed-catmull-pixar]: <https://www.bbc.com/news/health-47830256> "BBC: Aphantasia: Ex-Pixar chief Ed Catmull says 'my mind's eye is blind'"
[aphantasiacom-discussion-holding-grudges]: <https://aphantasia.com/discussion-question/emotional-memory-and-aphantasia/> "Aphantasia.com discussions: Emotions and aphantasia"
[youtube-ted-tamara-alireza-aphantasia]: <https://youtu.be/arc1fdoMi2Y> "YouTube: Aphantasia: Seeing the world without a mind's eye | Tamara Alireza | TEDxGoodenoughCollege"
[youtube-adam-zeman-ama]: <https://youtu.be/z9MCrrBt-_8> "YouTube: Aphantasia AMA with Dr. Adam Zeman"
[youtube-feynman-ways-of-thinking]: <https://youtu.be/zVaPOa7CZs4?t=133> "YouTube: Ways of Thinking - Richard Feynman"
[wikipedia-aphantasia]: <https://en.wikipedia.org/wiki/Aphantasia> "Wikipedia: Aphantasia"
