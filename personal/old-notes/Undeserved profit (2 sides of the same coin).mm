<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node ID="ID_583753807" CREATED="1329678288205" MODIFIED="1329680772607">
<richcontent TYPE="NODE">
<html>
  <head>
    
  </head>
  <body>
    <p style="text-align: center">
      Undeserved profit<br/>(2 sides of the same coin)
    </p>
  </body>
</html></richcontent>
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="Copying hardware/digital content" POSITION="right" ID="ID_1763487335" CREATED="1329678297124" MODIFIED="1329679901848" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="No money to producer" ID="ID_1140561089" CREATED="1329678483442" MODIFIED="1329678489325"/>
<node TEXT="Unreal expectancy of payment" ID="ID_1180053687" CREATED="1329678507722" MODIFIED="1329678518323"/>
<node TEXT="No worth because of satiation" ID="ID_1448015593" CREATED="1329678518784" MODIFIED="1329678577058"/>
<node TEXT="Complaints get all attention: criminalized" ID="ID_1972146711" CREATED="1329679632532" MODIFIED="1329679648862" COLOR="#ff0000">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node TEXT="Patents" POSITION="right" ID="ID_1410587072" CREATED="1329678636861" MODIFIED="1329679902727" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Easy money" ID="ID_1555970319" CREATED="1329678640113" MODIFIED="1329678642621"/>
<node TEXT="Intentionally left vague (trying to get as much as possible)" ID="ID_1396503312" CREATED="1329678642860" MODIFIED="1329679203097"/>
<node TEXT="Blocks competitors" ID="ID_1423343391" CREATED="1329678751079" MODIFIED="1329679269121"/>
<node TEXT="A way to earn money before you have actually created anything" ID="ID_1879154667" CREATED="1329679296087" MODIFIED="1329679305697"/>
<node TEXT="No possibility for patents on mathematical concepts" ID="ID_400630634" CREATED="1329678727432" MODIFIED="1329678739783"/>
</node>
<node TEXT="Vrijheid" POSITION="right" ID="ID_1518475" CREATED="1329680722951" MODIFIED="1329680725760" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Internetvrijheid" ID="ID_1486644154" CREATED="1329680728604" MODIFIED="1329680731512">
<node TEXT="Kennis beinvloed door rijke mensen" ID="ID_1431832652" CREATED="1329680738115" MODIFIED="1329680747347"/>
</node>
</node>
<node TEXT="Sending work to cheaper countries" POSITION="left" ID="ID_514721883" CREATED="1329678356781" MODIFIED="1329679900873" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="No work in own country" ID="ID_1938217564" CREATED="1329678467376" MODIFIED="1329678473626"/>
<node TEXT="Lower wages" ID="ID_111910376" CREATED="1329678476931" MODIFIED="1329678479262"/>
<node TEXT="Complaints get no attention: too bad" ID="ID_337542605" CREATED="1329679584370" MODIFIED="1329679617122" COLOR="#ff0000">
<font NAME="Liberation Sans" SIZE="12"/>
<icon BUILTIN="yes"/>
</node>
</node>
<node TEXT="Patents" POSITION="left" ID="ID_1803876636" CREATED="1329679183675" MODIFIED="1329679899945" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="No possibility to create something new" ID="ID_938401735" CREATED="1329679207250" MODIFIED="1329679215315"/>
<node TEXT="No clear line, so up to judge" ID="ID_1937600153" CREATED="1329679215531" MODIFIED="1329679225449"/>
<node TEXT="Blocks innovation" ID="ID_1873378655" CREATED="1329679271618" MODIFIED="1329679273810"/>
<node TEXT="Undeserved respect/money for some vague idea" ID="ID_1275590373" CREATED="1329679313415" MODIFIED="1329679338680"/>
</node>
<node TEXT="Vrijheid" POSITION="left" ID="ID_1638209480" CREATED="1329680749651" MODIFIED="1329680801562" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Internetvrijheid" ID="ID_331599281" CREATED="1329680781645" MODIFIED="1329680784445">
<node TEXT="Toegang tot dat wat vrij beschikbaar zou moeten zijn" ID="ID_1636046171" CREATED="1329680786037" MODIFIED="1329680799647"/>
</node>
</node>
<node TEXT="Properties" POSITION="left" ID="ID_307710620" CREATED="1329679917649" MODIFIED="1329679920326" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Supply and Demand" ID="ID_1155902587" CREATED="1329679777208" MODIFIED="1329680120028" COLOR="#669900" HGAP="26" VSHIFT="7">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="Absurd amount of artists/albums" ID="ID_912874424" CREATED="1329680011895" MODIFIED="1329680020201"/>
</node>
<node TEXT="Loss of control (giving away control)" ID="ID_1976509034" CREATED="1329679858514" MODIFIED="1329680123866" COLOR="#669900" HGAP="26" VSHIFT="-11">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="Stock Exchange instead of direct investment" ID="ID_940697857" CREATED="1329679997214" MODIFIED="1329680048954"/>
</node>
</node>
<node TEXT="Reasons" POSITION="right" ID="ID_1674980663" CREATED="1329680056473" MODIFIED="1329680060245" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Too trusting, unsure" ID="ID_696727241" CREATED="1329680068883" MODIFIED="1329680112760"/>
<node TEXT="Too greedy, misled by smarter person" ID="ID_779125758" CREATED="1329680071816" MODIFIED="1329680085313"/>
</node>
</node>
</map>
