<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Future Web-series" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1394406307727"><hook NAME="MapStyle" zoom="1.5">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="12"/>
<font SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="World-Wide Web" POSITION="right" ID="ID_1185725551" CREATED="1394404281955" MODIFIED="1394404288266">
<edge COLOR="#ff0000"/>
<node TEXT="HTTP" ID="ID_1978083404" CREATED="1394404528046" MODIFIED="1394404529458"/>
<node TEXT="First web pages" ID="ID_670359599" CREATED="1394404373511" MODIFIED="1394404376015"/>
<node TEXT="Web browsers" ID="ID_940606300" CREATED="1394404376464" MODIFIED="1394404378957">
<node TEXT="WorldWideWeb" ID="ID_1828746662" CREATED="1395869700418" MODIFIED="1395869705614"/>
<node TEXT="Mosaic" ID="ID_632142986" CREATED="1395869706158" MODIFIED="1395869708095"/>
</node>
<node TEXT="Distributed" ID="ID_1131235654" CREATED="1394404379214" MODIFIED="1394404383030"/>
<node TEXT="Simple" ID="ID_251308282" CREATED="1394405834927" MODIFIED="1394405835956"/>
<node TEXT="Accessible" ID="ID_1016889439" CREATED="1394405837172" MODIFIED="1394405838537"/>
<node TEXT="Connect to server directly" ID="ID_18818050" CREATED="1394404384108" MODIFIED="1394404390369"/>
<node TEXT="Indexes" ID="ID_1765994542" CREATED="1394404390642" MODIFIED="1394404393284">
<node TEXT="Yahoo!" ID="ID_1789742634" CREATED="1394404457912" MODIFIED="1394404460660"/>
<node TEXT="Excite" ID="ID_300355078" CREATED="1394404461116" MODIFIED="1394404462442"/>
</node>
<node TEXT="IP addresses" ID="ID_1293305578" CREATED="1395869730811" MODIFIED="1395869734922"/>
<node TEXT="Necessity: DNS support for friendly names" ID="ID_1058750441" CREATED="1394404416999" MODIFIED="1394891543722"/>
<node TEXT="Everyone knows everyone" ID="ID_860661744" CREATED="1394404428057" MODIFIED="1394404431635"/>
<node TEXT="Email" ID="ID_1535970542" CREATED="1394404433071" MODIFIED="1394404435416"/>
<node TEXT="Web&apos;s birth" ID="ID_95699606" CREATED="1394405301910" MODIFIED="1394405303847"/>
<node TEXT="Data also accessible via the web" ID="ID_979991142" CREATED="1394567631758" MODIFIED="1394567674705">
<node TEXT="Typical characteristics" ID="ID_1474524858" CREATED="1394567675445" MODIFIED="1394567678846">
<node TEXT="physical data in digital form" ID="ID_622968777" CREATED="1394567679470" MODIFIED="1395869752103"/>
<node TEXT="digital form as a way of sharing over large distances" ID="ID_1565103534" CREATED="1395869760561" MODIFIED="1395869769884"/>
<node TEXT="replication of data intended for physical representation" ID="ID_763023590" CREATED="1394567688594" MODIFIED="1394567708220"/>
</node>
<node TEXT="Physical data (papers etc.) to the web" ID="ID_25206738" CREATED="1394891560170" MODIFIED="1394891691820"/>
</node>
<node TEXT="Search engines do not really exist yet. The first, very basic very rudimentary search engines are born." ID="ID_1374625328" CREATED="1394568629066" MODIFIED="1394568655006"/>
<node TEXT="Search engines basically just index the text, not much more." ID="ID_31322606" CREATED="1394568655208" MODIFIED="1394568680051"/>
<node TEXT="HTML in very basic form, very rough and little rules. HTML (page) contains structure, content, formatting. No code/language/script exists yet." ID="ID_608543487" CREATED="1394568680311" MODIFIED="1394568771765"/>
<node TEXT="Web browsers are either text based or very basic graphics" ID="ID_888827608" CREATED="1394568734794" MODIFIED="1394568749214"/>
<node TEXT="Bootstrapping stage" ID="ID_1009267627" CREATED="1395957615661" MODIFIED="1395957683556">
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop-sign"/>
<icon BUILTIN="stop-sign"/>
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="The Web" POSITION="right" ID="ID_1318239634" CREATED="1394404290128" MODIFIED="1394404292362">
<edge COLOR="#0000ff"/>
<node TEXT="Search engines" ID="ID_1977096697" CREATED="1394404404129" MODIFIED="1394404406241">
<node TEXT="Altavista" ID="ID_294060017" CREATED="1394404450133" MODIFIED="1394404451603"/>
<node TEXT="Google" ID="ID_1980167849" CREATED="1394404447749" MODIFIED="1394404449760"/>
</node>
<node TEXT="Geocities" ID="ID_1394589843" CREATED="1394404472269" MODIFIED="1394404473479"/>
<node TEXT="eCommerce" ID="ID_343788902" CREATED="1394404298625" MODIFIED="1394404301629"/>
<node TEXT="large audience" ID="ID_1165253264" CREATED="1394404302573" MODIFIED="1394404315654"/>
<node TEXT="first cloud services offered" ID="ID_396714889" CREATED="1394404317571" MODIFIED="1394404325861">
<node TEXT="hotmail" ID="ID_1722056880" CREATED="1395870996514" MODIFIED="1395870997703"/>
</node>
<node TEXT="IRC, as a supporting technology for otherwise static Web" ID="ID_1878780789" CREATED="1396810550534" MODIFIED="1396810662831"/>
<node TEXT="SSL" ID="ID_325961367" CREATED="1394404293962" MODIFIED="1394404294991"/>
<node TEXT="HTTPS" ID="ID_672825060" CREATED="1394404537461" MODIFIED="1394404538982">
<node TEXT="improvement over HTTP for sensitive data" ID="ID_1364069928" CREATED="1396810668790" MODIFIED="1396810679889"/>
</node>
<node TEXT="Amazon" ID="ID_64274457" CREATED="1394404542286" MODIFIED="1394404543662"/>
<node TEXT="Web&apos;s puberty" ID="ID_1774535063" CREATED="1394405295885" MODIFIED="1394405298195"/>
<node TEXT="accessibility" ID="ID_362781127" CREATED="1395956461722" MODIFIED="1395956463824">
<node TEXT="DNS useful, predictable --&gt; names are basic" ID="ID_202477408" CREATED="1394406007946" MODIFIED="1394406028042"/>
<node TEXT="basic URL&apos;s, easy to share a specific page" ID="ID_1136611001" CREATED="1395956467799" MODIFIED="1395956479189"/>
<node TEXT="&quot;hazard-free&quot;, basic web pages, easy to understand for everybody" ID="ID_1899192775" CREATED="1395956484498" MODIFIED="1395956503798"/>
<node TEXT="People can learn by copy-pasting pieces of other web pages." ID="ID_1491855647" CREATED="1395956504223" MODIFIED="1395956523710"/>
</node>
<node TEXT="Data put on the web for transportation, midway station between one physical location and the next. The Web isn&apos;t the &quot;home&quot; of the data." ID="ID_1750458830" CREATED="1394567725591" MODIFIED="1394568796112">
<node TEXT="Email" ID="ID_663462664" CREATED="1394567834265" MODIFIED="1394567835738"/>
<node TEXT="Typical characteristics" ID="ID_1107389222" CREATED="1394567838482" MODIFIED="1394567841930">
<node TEXT="Data unformatted, since format doesn&apos;t matter for The Web" ID="ID_1236767421" CREATED="1394567842814" MODIFIED="1394567860627"/>
</node>
</node>
<node TEXT="Search engines try to do their best to make something of the blob of text that are called websites." ID="ID_1798359564" CREATED="1394568592553" MODIFIED="1394568609503"/>
<node TEXT="Websites are just about as freeform as can be. First occurrence of scripting, HTML still contains everything else." ID="ID_820843208" CREATED="1394568609862" MODIFIED="1394568849455"/>
<node TEXT="Advanced websites use server-side scripting in order to generate customized pages for the user." ID="ID_117709253" CREATED="1394569366800" MODIFIED="1394569391999"/>
<node TEXT="Server-side page generation very much a popular luxury among &quot;advanced&quot; web sites." ID="ID_1592916615" CREATED="1394570332144" MODIFIED="1394570354487"/>
<node TEXT="Web browsers are graphics based, multiple flavours, each with its own rendering engine and due to the freedom in HTML rendering results vary." ID="ID_309382985" CREATED="1394568859644" MODIFIED="1394568913136"/>
<node TEXT="Web Ring (Webring)" ID="ID_1749219193" CREATED="1396808309152" MODIFIED="1396808315538"/>
<node TEXT="Forums" ID="ID_284874264" CREATED="1396808332986" MODIFIED="1396808334851"/>
<node TEXT="First discussions on privacy as users want their content to be deleted once they leave a forum." ID="ID_1105959620" CREATED="1396815702274" MODIFIED="1396815721948"/>
<node TEXT="Infancy" ID="ID_471850812" CREATED="1399144166780" MODIFIED="1399144169739">
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="Web 2.0" POSITION="right" ID="ID_1374728133" CREATED="1394404342303" MODIFIED="1394404344060">
<edge COLOR="#00ff00"/>
<node TEXT="Neocities" ID="ID_408294997" CREATED="1394404477265" MODIFIED="1394404479231"/>
<node TEXT="OpenID" ID="ID_1918274103" CREATED="1394404507134" MODIFIED="1394404508377"/>
<node TEXT="OAuth" ID="ID_1396265283" CREATED="1394404497063" MODIFIED="1394404500860"/>
<node TEXT="Web Services" ID="ID_1127611560" CREATED="1394404516980" MODIFIED="1394404519039">
<node TEXT="mostly, accessible via the web, not so much presence on the web" ID="ID_491009118" CREATED="1394405149331" MODIFIED="1394405166614"/>
<node TEXT="complicated" ID="ID_428167567" CREATED="1394405188204" MODIFIED="1394405190433"/>
<node TEXT="corporate" ID="ID_635525565" CREATED="1394405190622" MODIFIED="1394405192191"/>
</node>
<node TEXT="Tor? (or at The Web)" ID="ID_1936856890" CREATED="1394404559986" MODIFIED="1394404575325"/>
<node TEXT="Authorization of users" ID="ID_719301727" CREATED="1394404605109" MODIFIED="1394404621718"/>
<node TEXT="Authenticity of users" ID="ID_331795705" CREATED="1394404616825" MODIFIED="1394404620024"/>
<node TEXT="Very much corporate presence trying to earn money by pushing users towards their product" ID="ID_1016464907" CREATED="1394405196857" MODIFIED="1394405212686"/>
<node TEXT="Maturation of the web: everyone connected to the web" ID="ID_384070683" CREATED="1394405279377" MODIFIED="1394405290930"/>
<node TEXT="More or less everyone connected" ID="ID_1866736556" CREATED="1394405271209" MODIFIED="1394405277356">
<node TEXT="everyone &quot;gets&quot; the basic services" ID="ID_769079526" CREATED="1394405316608" MODIFIED="1394405322454"/>
</node>
<node TEXT="Adoption of email by all &amp; simultaneous decline of usefulness of email" ID="ID_942668947" CREATED="1394405235913" MODIFIED="1394405257103">
<node TEXT="All reachable by email" ID="ID_473333263" CREATED="1394405260058" MODIFIED="1394405268229"/>
</node>
<node TEXT="Settling of basic (standard) services that use the web as medium, much still connected to real life" ID="ID_1945592678" CREATED="1394405460563" MODIFIED="1394405491964"/>
<node TEXT="Emergence of &quot;advanced&quot; &quot;connected&quot; services." ID="ID_1948871820" CREATED="1394405337259" MODIFIED="1394405353884"/>
<node TEXT="Web (UI) for the Web, Clients UI for local apps" ID="ID_934009161" CREATED="1394405747188" MODIFIED="1394405773934"/>
<node TEXT="DNS is necessary, not sufficient anymore" ID="ID_1270818690" CREATED="1394406031426" MODIFIED="1394406065831"/>
<node TEXT="DNS starting to show cracks" ID="ID_563124652" CREATED="1394406068061" MODIFIED="1394406078412">
<node TEXT="fake sites" ID="ID_901007389" CREATED="1394404653385" MODIFIED="1394404659801"/>
<node TEXT="phishing sites" ID="ID_1162681497" CREATED="1394404660009" MODIFIED="1394404662783"/>
<node TEXT="many domain names registered" ID="ID_653045817" CREATED="1394404662933" MODIFIED="1394404671147"/>
<node TEXT="starting to reach towards alternative TLDs for more domain names" ID="ID_1174629549" CREATED="1394406152264" MODIFIED="1394406181235"/>
<node TEXT="Creative names, obscure TLDs" ID="ID_743792669" CREATED="1394406105660" MODIFIED="1394406111925">
<node TEXT="During transition to The Semantic Web" ID="ID_1871270201" CREATED="1394406133980" MODIFIED="1394406142309"/>
<node TEXT="partial names - added to by TLD name&apos;s letters" ID="ID_180753729" CREATED="1394406143908" MODIFIED="1394406233418"/>
</node>
<node TEXT="DNS poisoning" ID="ID_1705976129" CREATED="1395516930312" MODIFIED="1395516940219"/>
<node TEXT="urgent requirements for secure DNS (DNSSEC)" ID="ID_1666942914" CREATED="1395516941025" MODIFIED="1395516959265"/>
<node TEXT="discussion on ownership/governance of DNS system" ID="ID_868402167" CREATED="1395527182223" MODIFIED="1395527202464"/>
</node>
<node TEXT="Emergence of eCrime, becoming more lucrative than real crime." ID="ID_836774658" CREATED="1394406537019" MODIFIED="1394406551265"/>
<node TEXT="Excessive treatment of everything WWW" ID="ID_1673521351" CREATED="1394406556156" MODIFIED="1394406583063">
<node TEXT="penalties/crimes" ID="ID_1025114382" CREATED="1394406584653" MODIFIED="1394406602732"/>
<node TEXT="hacking" ID="ID_54890607" CREATED="1394406603254" MODIFIED="1394406605669"/>
<node TEXT="overrated tech companies" ID="ID_53636731" CREATED="1394406606061" MODIFIED="1394406611727"/>
<node TEXT="over-admired services" ID="ID_1397730467" CREATED="1394406618061" MODIFIED="1394406630506"/>
</node>
<node TEXT="New generation grows up with internet, finds out it can participate" ID="ID_1054798918" CREATED="1394407004052" MODIFIED="1394407019513">
<node TEXT="can&apos;t wait to be more active, non/sub-web-technology out of reach" ID="ID_8469016" CREATED="1394407092818" MODIFIED="1394407134882"/>
</node>
<node TEXT="Very old services (with alternatives) die off" ID="ID_1943491158" CREATED="1394407389306" MODIFIED="1394407425950">
<node TEXT="Geocities" ID="ID_1628638665" CREATED="1394407394090" MODIFIED="1394407395501">
<node TEXT="had the original thought, didn&apos;t evolve with time" ID="ID_652867299" CREATED="1394407523316" MODIFIED="1394407533519">
<font ITALIC="true"/>
</node>
</node>
<node TEXT="ICQ" ID="ID_859698296" CREATED="1394407396598" MODIFIED="1394407404490"/>
<node TEXT="MSN" ID="ID_268531722" CREATED="1394407407037" MODIFIED="1394407407926"/>
</node>
<node TEXT="Data is put on The Web for the intention of putting it on the Web itself. The web is the end goal/location." ID="ID_307661256" CREATED="1394568209589" MODIFIED="1394568238444">
<node TEXT="Typical characteristics" ID="ID_1962418838" CREATED="1394568238879" MODIFIED="1394568243620">
<node TEXT="User interaction" ID="ID_886885110" CREATED="1394568244148" MODIFIED="1394568249620"/>
<node TEXT="Opinions" ID="ID_468893670" CREATED="1394568249803" MODIFIED="1394568253292"/>
<node TEXT="Participation in forums, groups" ID="ID_1873447724" CREATED="1394568253623" MODIFIED="1394568268037"/>
<node TEXT="Largely unformatted" ID="ID_432701623" CREATED="1394568277125" MODIFIED="1394568281249"/>
<node TEXT="Discovery via direct communication between users" ID="ID_118042779" CREATED="1394568281454" MODIFIED="1394568298289"/>
<node TEXT="Data/Format not native to the web, but can be put there anyways" ID="ID_1119871343" CREATED="1394568366840" MODIFIED="1394568393932"/>
</node>
</node>
<node TEXT="Search engines (mostly Google) get rules, &quot;read&quot; pages, &quot;demand content&quot;, &quot;demand structure&quot;, punish cheaters, think with the user." ID="ID_1797522537" CREATED="1394568969733" MODIFIED="1394569059020">
<node TEXT="Emergence of the Personal Search Bubble." ID="ID_1709680075" CREATED="1394569021488" MODIFIED="1394569039060"/>
</node>
<node TEXT="Websites slowly gain a more structured source, more advanced tags available, reliance on plug-ins for those &quot;really different interactive visual&quot; experiences." ID="ID_873390164" CREATED="1394569069383" MODIFIED="1394569130820">
<node TEXT="Scripting already separated" ID="ID_1794203057" CREATED="1394569179068" MODIFIED="1394569188252"/>
<node TEXT="Separation of formatting" ID="ID_1533996765" CREATED="1394569159828" MODIFIED="1394569175480"/>
<node TEXT="Combined structure + content" ID="ID_1669323490" CREATED="1394569175674" MODIFIED="1394569197065"/>
</node>
<node TEXT="Websites rely heavily on dynamically generated pages." ID="ID_210702554" CREATED="1394569206594" MODIFIED="1394569428156">
<node TEXT="User participation is center" ID="ID_417103126" CREATED="1394569428703" MODIFIED="1394569435695"/>
<node TEXT="Client-side scripts are there, but server-side scripting mostly prevalent." ID="ID_1357779756" CREATED="1394569437483" MODIFIED="1394570308172"/>
<node TEXT="Heavy focus on personally approaching the user." ID="ID_192212163" CREATED="1394570375968" MODIFIED="1394570390683"/>
</node>
<node TEXT="Webbrowsers aim to standardization, fight for correct representation of page, aggressively extend capabilities in order to offer more possibilities to users." ID="ID_736550155" CREATED="1394570471417" MODIFIED="1394571733185">
<node TEXT="Force structure upon web pages in order to provide reliable representation" ID="ID_1461334191" CREATED="1394570530720" MODIFIED="1394570543313"/>
<node TEXT="Extend possibilities." ID="ID_97286390" CREATED="1394570544381" MODIFIED="1394570555497"/>
<node TEXT="Scripts, structure, content, styling separated." ID="ID_1753012154" CREATED="1394570555696" MODIFIED="1394570657540"/>
</node>
<node TEXT="Puberty" ID="ID_599021622" CREATED="1399144198005" MODIFIED="1399144200765">
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="The semantic Web" POSITION="right" ID="ID_975160199" CREATED="1394404348738" MODIFIED="1394404354147">
<edge COLOR="#00ffff"/>
<node TEXT="Clear path defined by Tim Berners Lee" ID="ID_88334425" CREATED="1394405008666" MODIFIED="1394405026525">
<node TEXT="RDF" ID="ID_1516739771" CREATED="1394407150581" MODIFIED="1394407151411"/>
<node TEXT="OWL" ID="ID_1044599302" CREATED="1394407152055" MODIFIED="1394407155501"/>
<node TEXT="etc." ID="ID_1283979902" CREATED="1394407167869" MODIFIED="1394407168399"/>
<node TEXT="Doesn&apos;t gain much traction to start with" ID="ID_750155160" CREATED="1394405027558" MODIFIED="1394405038894"/>
</node>
<node TEXT="Seems to be guided into this direction" ID="ID_157174580" CREATED="1394405039177" MODIFIED="1394405127713">
<node TEXT="by the desire the automate" ID="ID_1192207723" CREATED="1394405057011" MODIFIED="1394405057913"/>
<node TEXT="by desire to be accessible/included" ID="ID_119695392" CREATED="1394405058339" MODIFIED="1394405072501">
<node TEXT="Position in Google Search index" ID="ID_1496640483" CREATED="1394405072917" MODIFIED="1394405092355"/>
<node TEXT="Connectivity with other (hyped) services" ID="ID_1054885766" CREATED="1394405077285" MODIFIED="1394405104389"/>
</node>
<node TEXT="by Open Data initiatives" ID="ID_444715475" CREATED="1394405110080" MODIFIED="1394405129770"/>
</node>
<node TEXT="Need to structure" ID="ID_1578813448" CREATED="1394407187109" MODIFIED="1394407192275">
<node TEXT="Wikipedia" ID="ID_86236902" CREATED="1394407194080" MODIFIED="1394407195617"/>
<node TEXT="for practical reasons, such as translations, storage, classification, comparability, historical record keeping, uniformity of UI, etc." ID="ID_872059435" CREATED="1394407201627" MODIFIED="1394407235294"/>
</node>
<node TEXT="REST-ful services" ID="ID_1132080907" CREATED="1394404513344" MODIFIED="1394404516254">
<node TEXT="Less complicated" ID="ID_61166563" CREATED="1394405182737" MODIFIED="1394405184620"/>
<node TEXT="Very much presence on the web itself" ID="ID_723993811" CREATED="1394405173060" MODIFIED="1394405180539"/>
<node TEXT="Simple implementations richly available (already improvement over SOAP)" ID="ID_366699338" CREATED="1402609785468" MODIFIED="1402609822076"/>
<node TEXT="Advanced implementations emerging (hypermedia)" ID="ID_1302703710" CREATED="1402609822320" MODIFIED="1402609831760"/>
</node>
<node TEXT="Very much users setting things up because they are fed up with companies pushing products" ID="ID_1543794271" CREATED="1394405215128" MODIFIED="1394405233959"/>
<node TEXT="Omnipresence of the web" ID="ID_385581029" CREATED="1394405367559" MODIFIED="1394405378565">
<node TEXT="everyone connected" ID="ID_1578965008" CREATED="1394405380237" MODIFIED="1394405406947"/>
<node TEXT="The web available everywhere (smartphones)" ID="ID_1323431489" CREATED="1394405407177" MODIFIED="1394405422462"/>
<node TEXT="Steady emergence of other devices to the web." ID="ID_1062846624" CREATED="1394405502437" MODIFIED="1394405512081"/>
<node TEXT="All data on the web" ID="ID_1886585473" CREATED="1394405516540" MODIFIED="1394405522090"/>
<node TEXT="Open data initiatives" ID="ID_1400646607" CREATED="1394405522310" MODIFIED="1394405526735"/>
</node>
<node TEXT="Focus of web activity on advanced connected services that are only relevant to/on the web itself" ID="ID_1882271277" CREATED="1394405435719" MODIFIED="1394405457078"/>
<node TEXT="Web UI for everything, client application the exception" ID="ID_911281496" CREATED="1394405776652" MODIFIED="1394405788187"/>
<node TEXT="YOU ARE HERE! (clearly emerging into Semantic Web)" ID="ID_654327057" CREATED="1394407254371" MODIFIED="1402609773921">
<icon BUILTIN="messagebox_warning"/>
<icon BUILTIN="very_positive"/>
<icon BUILTIN="very_negative"/>
<icon BUILTIN="button_ok"/>
<font BOLD="true" ITALIC="true"/>
<node TEXT="We are currently fully in Web 3.0. There reason this is not so prevalant is that there is a lot of legacy on the Internet." ID="ID_794890871" CREATED="1395871852133" MODIFIED="1395871896179">
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="Original intention for DNS failing" ID="ID_1426017683" CREATED="1394404647887" MODIFIED="1394405954201">
<node TEXT="Short name services set up by various companies" ID="ID_1941929446" CREATED="1394406248506" MODIFIED="1394406265005"/>
<node TEXT="names are:" ID="ID_620834313" CREATED="1394404671375" MODIFIED="1394404678812">
<node TEXT="slogans" ID="ID_1279019105" CREATED="1394404679136" MODIFIED="1394404681838"/>
<node TEXT="complete sentences" ID="ID_479762036" CREATED="1394404682038" MODIFIED="1394404684922"/>
<node TEXT="strange notations to which company name is adapted" ID="ID_69954851" CREATED="1394404685163" MODIFIED="1394404695173"/>
<node TEXT="target audience" ID="ID_1977803448" CREATED="1394404697422" MODIFIED="1394404703834"/>
<node TEXT="key selling points" ID="ID_1149383104" CREATED="1394404704595" MODIFIED="1394404709314"/>
</node>
<node TEXT="companies want to be present, don&apos;t have a clue how to" ID="ID_1608214750" CREATED="1394404718918" MODIFIED="1394404728577">
<node TEXT="completely new sites for temporary things not worth mentioning" ID="ID_1708700333" CREATED="1394404729420" MODIFIED="1394404743288"/>
<node TEXT="register anything they can" ID="ID_6295596" CREATED="1394404745429" MODIFIED="1394404752469"/>
</node>
<node TEXT="anything allowed to be a TLD" ID="ID_809113893" CREATED="1394404783914" MODIFIED="1394404797428"/>
<node TEXT="Certificates, CA&apos;s failing, domain names aren&apos;t the right technological connection/tool" ID="ID_1481054688" CREATED="1394405963764" MODIFIED="1394405984418"/>
<node TEXT="Alternative DNS and DNS implementations are being investigated." ID="ID_695186644" CREATED="1394890934919" MODIFIED="1394890947907"/>
</node>
<node TEXT="Bitcoin" ID="ID_842071887" CREATED="1394406459906" MODIFIED="1394406461420">
<node TEXT="on-line only presence" ID="ID_1245851071" CREATED="1394406464601" MODIFIED="1394406474061"/>
<node TEXT="leans on the network" ID="ID_1918827290" CREATED="1394406474548" MODIFIED="1394406481683"/>
<node TEXT="audience determines (not just participation), empowered" ID="ID_714254770" CREATED="1394406481868" MODIFIED="1394406504312"/>
<node TEXT="Semantics presence (machine-readable)" ID="ID_277475319" CREATED="1394406511226" MODIFIED="1394406525798"/>
</node>
<node TEXT="Steady maturation of understanding, far from complete" ID="ID_226872463" CREATED="1394406650754" MODIFIED="1394406660203">
<node TEXT="Slow steady understanding of the web for global audience" ID="ID_702038154" CREATED="1394406636388" MODIFIED="1394406649842"/>
</node>
<node TEXT="Governmental outreach, cyberwar, problems with all things cyber" ID="ID_1615214624" CREATED="1394406664047" MODIFIED="1394406692747"/>
<node TEXT="New generation &quot;lives&quot; on the internet and does everything by the internet, but with lack of knowledge flocks to the-hyped-internet-services-of-the-moment(tm)" ID="ID_1890412719" CREATED="1394407022957" MODIFIED="1394407061388">
<node TEXT="need for something new" ID="ID_894781830" CREATED="1394407066785" MODIFIED="1394407070995"/>
<node TEXT="need for presence" ID="ID_1495798083" CREATED="1394407071322" MODIFIED="1394407083543"/>
<node TEXT="need for attention" ID="ID_180683022" CREATED="1394407083763" MODIFIED="1394407086183"/>
</node>
<node TEXT="Other services emerge/evolve" ID="ID_1121741411" CREATED="1394407440483" MODIFIED="1394407480676">
<node TEXT="MSN --&gt; Skype" ID="ID_1064257395" CREATED="1394407448707" MODIFIED="1394407485945"/>
<node TEXT="XMPP" ID="ID_630630377" CREATED="1394407461682" MODIFIED="1394407465179">
<node TEXT="Separate servers" ID="ID_1693773280" CREATED="1394407465577" MODIFIED="1394407470212"/>
<node TEXT="Facebook chat" ID="ID_484524604" CREATED="1394407470412" MODIFIED="1394407472204"/>
<node TEXT="Google Talk" ID="ID_1795435936" CREATED="1394407473733" MODIFIED="1394407476149"/>
</node>
</node>
<node TEXT="Services resurrected" ID="ID_1350333676" CREATED="1394407499254" MODIFIED="1394407502471">
<node TEXT="Neocities" ID="ID_469108956" CREATED="1394407502863" MODIFIED="1394407504682">
<node TEXT="has the original thought" ID="ID_1119498424" CREATED="1394407537272" MODIFIED="1394407548388">
<node TEXT="always available, ready" ID="ID_1472622157" CREATED="1394407587146" MODIFIED="1394407600608"/>
</node>
<node TEXT="new target audience" ID="ID_1206235436" CREATED="1394407548773" MODIFIED="1394407551085">
<node TEXT="new users" ID="ID_1449069399" CREATED="1394407602171" MODIFIED="1394407604952"/>
</node>
<node TEXT="new target" ID="ID_1224965476" CREATED="1394407551367" MODIFIED="1394407552498">
<node TEXT="show, express opinions, basic information, no nonsense" ID="ID_169082954" CREATED="1394407756854" MODIFIED="1394407770458"/>
</node>
<node TEXT="presence, contribution, opinions" ID="ID_177238025" CREATED="1394407555780" MODIFIED="1394407574297"/>
<node TEXT="simple, specific" ID="ID_716398593" CREATED="1394407574541" MODIFIED="1394407577437"/>
</node>
</node>
<node TEXT="Data belongs on the web, applications only access data." ID="ID_1889436880" CREATED="1394567525933" MODIFIED="1394567533232">
<node TEXT="Typical characteristics" ID="ID_1704051461" CREATED="1394567538760" MODIFIED="1394567544422">
<node TEXT="(too) much data" ID="ID_790107823" CREATED="1394567545009" MODIFIED="1394568315248"/>
<node TEXT="data only useful at specific moments, times, reasons, locations, etc." ID="ID_1702756671" CREATED="1394567550194" MODIFIED="1394567570383"/>
<node TEXT="data can be easily shared" ID="ID_31669521" CREATED="1394567575926" MODIFIED="1394567582040"/>
<node TEXT="application in form of appropriate UI (typically a thin layer) to the data" ID="ID_391271294" CREATED="1394567591313" MODIFIED="1394567615204"/>
</node>
<node TEXT="Separates data from machine/location" ID="ID_735242409" CREATED="1394891738512" MODIFIED="1394891749406"/>
</node>
<node TEXT="Search engines know how to read highly structured pages, such as Wikipedia. Search engine results contain links to different sections within a website, understand the structure of the web site." ID="ID_1817085556" CREATED="1394570690242" MODIFIED="1394570731260">
<node TEXT="Coercion of structure in web sites by promoting them in search results." ID="ID_1390991424" CREATED="1394570733999" MODIFIED="1394570755808"/>
<node TEXT="Actual (rough) understanding of the website and content." ID="ID_465165770" CREATED="1394570756019" MODIFIED="1394570772360"/>
</node>
<node TEXT="Web browsers evolve into application platforms complete with persistence options, animation, video, audio, drawing support. Plug-ins become obsolete as same capabilities are presented within the browser itself. Scripting (execution) becomes the twin feature together with page rendering.&#xa;Engines are highly optimized for speed, the scripting languages is even adjusted to improve better cooperation with the browser (platform)." ID="ID_633354404" CREATED="1394571766166" MODIFIED="1394571884874">
<node TEXT="Advanced scripting engine" ID="ID_586549656" CREATED="1394571890557" MODIFIED="1394571895685"/>
<node TEXT="Rigorous separation of" ID="ID_778409817" CREATED="1394571895884" MODIFIED="1394571906211">
<node TEXT="structure" ID="ID_1567341970" CREATED="1394571906928" MODIFIED="1394571909380"/>
<node TEXT="content" ID="ID_458841509" CREATED="1394571909577" MODIFIED="1394571910424"/>
<node TEXT="formatting/styling" ID="ID_1462490315" CREATED="1394571910974" MODIFIED="1394571913588"/>
<node TEXT="scripting" ID="ID_85479414" CREATED="1394571913808" MODIFIED="1394571920216"/>
</node>
</node>
<node TEXT="Web sites continue to further separate concerns" ID="ID_1337108987" CREATED="1394571923406" MODIFIED="1394572056002">
<node TEXT="Server-side for dynamic structure, possible combined with initial content." ID="ID_1479063844" CREATED="1394571943216" MODIFIED="1394571967748"/>
<node TEXT="Client-side for dynamic content, content updates, interaction with the user." ID="ID_1080758251" CREATED="1394571951139" MODIFIED="1394571982639"/>
<node TEXT="Pages are often partially updated by scripting rather than reloaded upon every action. (As long as there are no serious changes to the structure of the page.)" ID="ID_1276432355" CREATED="1394571983162" MODIFIED="1394572049271"/>
<node TEXT="Separation of structure from content is encouraged by dynamic client-side updates." ID="ID_469664969" CREATED="1394572068401" MODIFIED="1394572087001"/>
<node TEXT="(Web) Apps rely on scripting to update otherwise static pages with new content." ID="ID_789912622" CREATED="1394572090442" MODIFIED="1394572126252"/>
</node>
<node TEXT="Alternative API&apos;s for retrieving data in structured form by other web sites/services/search engines" ID="ID_968891106" CREATED="1394575165891" MODIFIED="1394575185298"/>
<node TEXT="Social networks" ID="ID_1992016519" CREATED="1394890959091" MODIFIED="1394890968654">
<node TEXT="Try to be all-you-ever-need for the user" ID="ID_159567751" CREATED="1394890971837" MODIFIED="1394890982575"/>
<node TEXT="Try to lock in users into their network" ID="ID_1087850743" CREATED="1394890982814" MODIFIED="1394890997028"/>
<node TEXT="Fail to be an alternative network" ID="ID_635248935" CREATED="1394890997299" MODIFIED="1394891004050"/>
<node TEXT="Limited by the domain (name)" ID="ID_637509820" CREATED="1394891004281" MODIFIED="1394891030642"/>
<node TEXT="Try to gather all data, little sharing." ID="ID_388331343" CREATED="1394891756276" MODIFIED="1394891769196"/>
</node>
<node TEXT="Special search engines" ID="ID_1049688410" CREATED="1395951836244" MODIFIED="1395951846812">
<node TEXT="ShodanHQ" ID="ID_175897881" CREATED="1395951847503" MODIFIED="1395951850774"/>
<node TEXT="*.google.com" ID="ID_883310979" CREATED="1395951850982" MODIFIED="1395951860983">
<node TEXT="video" ID="ID_728672574" CREATED="1395951862279" MODIFIED="1395951863647"/>
<node TEXT="images" ID="ID_664059468" CREATED="1395951863841" MODIFIED="1395951864450"/>
<node TEXT="scholar" ID="ID_1752569837" CREATED="1395951866456" MODIFIED="1395951867257"/>
</node>
<node TEXT="Launchpad" ID="ID_104827969" CREATED="1395951874887" MODIFIED="1395951934412">
<node TEXT="RDF metadata" ID="ID_250704063" CREATED="1395951923830" MODIFIED="1395951925857"/>
</node>
<node TEXT="Ohloh" ID="ID_1342167691" CREATED="1395951987467" MODIFIED="1395951988782"/>
</node>
<node TEXT="Online participation measured, determines type of user" ID="ID_62898724" CREATED="1395951899324" MODIFIED="1395951912138"/>
<node TEXT="No longer for the curious user. By now, The Web is mostly integrated into society." ID="ID_1090471947" CREATED="1399146443298" MODIFIED="1399146471783"/>
<node TEXT="(Struggling) maturity" ID="ID_101690413" CREATED="1399144514920" MODIFIED="1399144519517">
<font BOLD="true" ITALIC="true"/>
</node>
</node>
<node TEXT="The Future" LOCALIZED_STYLE_REF="default" POSITION="right" ID="ID_1825932942" CREATED="1394404344514" MODIFIED="1395870323647">
<edge COLOR="#ff00ff"/>
<node TEXT="Inherently much choice, since it is still the future." ID="ID_1980671503" CREATED="1394406708941" MODIFIED="1394406719539"/>
<node TEXT="Alternative name systems" ID="ID_661392805" CREATED="1394404554258" MODIFIED="1394404757573">
<node TEXT="GNU nameSomething (GNATS)" ID="ID_1295463210" CREATED="1394404758593" MODIFIED="1394995478359"/>
<node TEXT="Namecoin" ID="ID_551602803" CREATED="1394404765236" MODIFIED="1394404767117"/>
<node TEXT="several rogue name systems" ID="ID_1166272850" CREATED="1394404771922" MODIFIED="1394404781104"/>
<node TEXT=".onion TLD for TOR" ID="ID_300836426" CREATED="1394404803213" MODIFIED="1394404813466"/>
<node TEXT=".something else Darknet?" ID="ID_905859245" CREATED="1394404814108" MODIFIED="1394404824374"/>
</node>
<node TEXT="Alternative networks" ID="ID_451895974" CREATED="1394404589494" MODIFIED="1394404592793">
<node TEXT="IPv4" ID="ID_278796262" CREATED="1394406400032" MODIFIED="1394406401817"/>
<node TEXT="IPv6" ID="ID_1758673435" CREATED="1394406402023" MODIFIED="1394406403390"/>
<node TEXT="VPNs" ID="ID_1270487039" CREATED="1394406408401" MODIFIED="1394406410577"/>
<node TEXT="TOR" ID="ID_966965908" CREATED="1394404828078" MODIFIED="1394404829114"/>
<node TEXT="Darknet" ID="ID_818353212" CREATED="1394406333256" MODIFIED="1394406334573"/>
<node TEXT="..." ID="ID_121000088" CREATED="1394406335196" MODIFIED="1394406336114"/>
<node TEXT="Services with need to separate/distinguish themselves from others choose alt. network" ID="ID_1390086014" CREATED="1394406432462" MODIFIED="1394406450645"/>
<node TEXT="ZeroTier" ID="ID_1809433241" CREATED="1394572571076" MODIFIED="1394572573051"/>
<node TEXT="Social networks really separated" ID="ID_394638532" CREATED="1394890909452" MODIFIED="1394890917289"/>
<node TEXT="Social networks" ID="ID_243315250" CREATED="1394891043406" MODIFIED="1394891045609"/>
<node TEXT="Internet of Things" ID="ID_832796269" CREATED="1394891052055" MODIFIED="1394891055820"/>
<node TEXT="&quot;Knowledge base&quot; for robots" ID="ID_1246227619" CREATED="1394891056021" MODIFIED="1394891073279"/>
<node TEXT="Research networks" ID="ID_1350243473" CREATED="1395868979319" MODIFIED="1395868982421"/>
<node TEXT="Mesh networks" ID="ID_1677790332" CREATED="1395868982863" MODIFIED="1395868997969"/>
</node>
<node TEXT="Indicators for necessity of alternative networks" ID="ID_687233836" CREATED="1401540662917" MODIFIED="1401540674104">
<node TEXT="Instagram, Pinterest, Facebook, Google+, Twitter, Tumblr, Flickr, ..." ID="ID_820047537" CREATED="1401540677184" MODIFIED="1401540705043"/>
<node TEXT="Reddit, Digg, ..." ID="ID_1883041833" CREATED="1401540709996" MODIFIED="1401540718903"/>
</node>
<node TEXT="Extra layer: DNS separate networks, create binding/compatibility layer for old to new region/network" ID="ID_942053416" CREATED="1394404942938" MODIFIED="1394404970474">
<node TEXT="Become entry point to alternative networks" ID="ID_933374521" CREATED="1394891492797" MODIFIED="1394891498954"/>
<node TEXT="Addressing for alternative networks" ID="ID_566806500" CREATED="1394891499177" MODIFIED="1394891508849"/>
<node TEXT="Focus from Memorable to Unique" ID="ID_1403409299" CREATED="1394891510701" MODIFIED="1394891519245"/>
<node TEXT="Zooko&apos;s Triangle" ID="ID_535254509" CREATED="1394891522337" MODIFIED="1394891526288"/>
</node>
<node TEXT="IPv4, IPV6" ID="ID_777716309" CREATED="1394404984904" MODIFIED="1394406389057">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="80" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_823348614" STARTINCLINATION="89;0;" ENDINCLINATION="89;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
</node>
<node TEXT="All things connected" ID="ID_823348614" CREATED="1394405388885" MODIFIED="1394405397402"/>
<node TEXT="Anonimity" ID="ID_1549269857" CREATED="1394404835476" MODIFIED="1394404837766"/>
<node TEXT="Authenticity of services/sites" ID="ID_778023633" CREATED="1394404594494" MODIFIED="1394404629305"/>
<node TEXT="Authorization of services/sites" ID="ID_1635741454" CREATED="1394404599939" MODIFIED="1394404635623"/>
<node TEXT="Predictions: original web sites slowly declining." ID="ID_1539908382" CREATED="1394405547101" MODIFIED="1394405563109">
<node TEXT="Only fossils and lagging companies will persist presence on the original web" ID="ID_1370668114" CREATED="1394405563567" MODIFIED="1394405576824"/>
<node TEXT="Separation into a number of connected networks." ID="ID_858395530" CREATED="1394405608266" MODIFIED="1394405622226"/>
<node TEXT="References, connectivity between those networks." ID="ID_1218037594" CREATED="1394405622417" MODIFIED="1394405640326"/>
</node>
<node TEXT="Extra layers on top of web sites" ID="ID_435893618" CREATED="1394405580143" MODIFIED="1394405588752">
<node TEXT="Discussion layer via e.g. Browser-plugins" ID="ID_983030895" CREATED="1394405589115" MODIFIED="1394405602148"/>
<node TEXT="Notion of neighbors by visiting the same sites" ID="ID_155724225" CREATED="1394572474769" MODIFIED="1394572489468"/>
<node TEXT="Notion of presence by visiting the same/related sites" ID="ID_1066662107" CREATED="1394572459639" MODIFIED="1394572474491"/>
<node TEXT="(Inter)Connectivity with different/other networks" ID="ID_1468698937" CREATED="1394572579382" MODIFIED="1394572704273"/>
<node TEXT="Opens up possibilities not otherwise offered by the website itself." ID="ID_1431545479" CREATED="1394572710840" MODIFIED="1394572723136"/>
</node>
<node TEXT="Reinventing the web" ID="ID_424650230" CREATED="1395003815547" MODIFIED="1395003820769">
<node TEXT="Accessibility" ID="ID_1986816192" CREATED="1395003823714" MODIFIED="1395003828411"/>
<node TEXT="Simple set-up (again)" ID="ID_844980606" CREATED="1395003828757" MODIFIED="1395003837776"/>
<node TEXT="Simple start up (again)" ID="ID_93700675" CREATED="1395003936877" MODIFIED="1395003940524"/>
<node TEXT="Getting started quickly and easily" ID="ID_651284825" CREATED="1395003950214" MODIFIED="1395003960453">
<node TEXT="with modern formatting" ID="ID_95162150" CREATED="1395003960904" MODIFIED="1395003963551"/>
<node TEXT="with useful javascript functions" ID="ID_982230835" CREATED="1395003963747" MODIFIED="1395003994486"/>
<node TEXT="with examples" ID="ID_1683120000" CREATED="1395003996556" MODIFIED="1395003998646"/>
<node TEXT="with minimal set up" ID="ID_1969740715" CREATED="1395003998984" MODIFIED="1395004001766"/>
<node TEXT="html5 compliant" ID="ID_1127330355" CREATED="1395004008518" MODIFIED="1395004013822"/>
<node TEXT="little to no legacy" ID="ID_1484553247" CREATED="1395004014133" MODIFIED="1395004020866"/>
<node TEXT="only support modern browsers" ID="ID_907027897" CREATED="1395004029457" MODIFIED="1395004053330"/>
<node TEXT="Advanced features" ID="ID_890318338" CREATED="1395004109009" MODIFIED="1395004111732">
<node TEXT="some templating for tedious tasks?" ID="ID_1968991001" CREATED="1395004035383" MODIFIED="1395004042599"/>
<node TEXT="some hints for coping with the impossible?" ID="ID_1201368429" CREATED="1395004066388" MODIFIED="1395004076379">
<node TEXT="surge of visitors (use CDNs)" ID="ID_1094426849" CREATED="1395004077509" MODIFIED="1395004103000"/>
</node>
</node>
</node>
<node TEXT="Direct access in (a particular) network" ID="ID_400133814" CREATED="1395003838191" MODIFIED="1395003858974"/>
</node>
<node TEXT="Refocusing on relevant properties" ID="ID_324006290" CREATED="1395530252866" MODIFIED="1395530261982"/>
<node TEXT="Organism evolution" ID="ID_1342320957" CREATED="1399144525512" MODIFIED="1399144535248">
<font BOLD="true" ITALIC="true"/>
</node>
<node TEXT="Concepts of hypermedia more widely spread/available." ID="ID_352811995" CREATED="1402609852474" MODIFIED="1402609865264">
<node TEXT="Makes clients more easily implemented." ID="ID_1195423662" CREATED="1402609866121" MODIFIED="1402609873015"/>
<node TEXT="Makes API self-describing/self-explaining." ID="ID_380971928" CREATED="1402609873215" MODIFIED="1402609902668">
<node TEXT="Except for data types, domain knowledge" ID="ID_1116759809" CREATED="1402609902937" MODIFIED="1402609925836"/>
<node TEXT="Except for meaning of actions, domain knowledge" ID="ID_321820944" CREATED="1402609906632" MODIFIED="1402609929237"/>
</node>
<node TEXT="Stateless communication/interaction" ID="ID_1529432638" CREATED="1402609941967" MODIFIED="1402609956702">
<node TEXT="all state is communicated at every request" ID="ID_346983661" CREATED="1402609958576" MODIFIED="1402609997416"/>
</node>
<node TEXT="Not necessarily stateless data persistence" ID="ID_977381819" CREATED="1402609999810" MODIFIED="1402610011176"/>
<node TEXT="Server determines state of data" ID="ID_471789810" CREATED="1402610013074" MODIFIED="1402610024267"/>
<node TEXT="Client determines state of user interface/xperience" ID="ID_782812272" CREATED="1402610024537" MODIFIED="1402610033915"/>
</node>
<node TEXT="Properties of the various networks" ID="ID_1969148380" CREATED="1402610140285" MODIFIED="1402610146626">
<node TEXT="Generic properties" ID="ID_745518498" CREATED="1402610147154" MODIFIED="1402610150340">
<node TEXT="Proximity, at different levels" ID="ID_917654066" CREATED="1402610150930" MODIFIED="1402610157142"/>
</node>
</node>
</node>
<node TEXT="Global Intelligence (Internet Brain)" POSITION="right" ID="ID_1686354859" CREATED="1395869004155" MODIFIED="1395869025997">
<edge COLOR="#7c7c00"/>
<node TEXT="many different networks" ID="ID_88916394" CREATED="1395869037682" MODIFIED="1395869047063">
<node TEXT="correspond to regions of the brain" ID="ID_302472425" CREATED="1395869113457" MODIFIED="1395869119469"/>
<node TEXT="different focus" ID="ID_1768198378" CREATED="1395869049862" MODIFIED="1395869051934"/>
<node TEXT="unique relevancy" ID="ID_1034941932" CREATED="1395869052279" MODIFIED="1395869057558"/>
<node TEXT="importance of preserving a status quo" ID="ID_1554042166" CREATED="1395869057931" MODIFIED="1395869083767">
<node TEXT="relevant for keeping the network intact" ID="ID_714052115" CREATED="1395869085105" MODIFIED="1395869090192"/>
</node>
</node>
<node TEXT="powered by any net participants" ID="ID_539952281" CREATED="1395870340239" MODIFIED="1395870357609">
<node TEXT="human" ID="ID_1586270177" CREATED="1395870359412" MODIFIED="1395870360916"/>
<node TEXT="computer" ID="ID_1117393120" CREATED="1395870361518" MODIFIED="1395870364062"/>
<node TEXT="robot" ID="ID_1216373855" CREATED="1395870364341" MODIFIED="1395870365092"/>
<node TEXT="..." ID="ID_865493530" CREATED="1395870367581" MODIFIED="1395870370810"/>
</node>
<node TEXT="powered by any motivation" ID="ID_1366592988" CREATED="1395870372972" MODIFIED="1395870383124">
<node TEXT="ideology" ID="ID_1010734926" CREATED="1395870384219" MODIFIED="1395870386034"/>
<node TEXT="law" ID="ID_1462507117" CREATED="1395870386247" MODIFIED="1395870387355"/>
<node TEXT="preservation" ID="ID_114629684" CREATED="1395870387837" MODIFIED="1395870391856"/>
<node TEXT="research &amp; developments" ID="ID_233076982" CREATED="1395870395037" MODIFIED="1395870402562"/>
<node TEXT="necessity" ID="ID_469963089" CREATED="1395870404251" MODIFIED="1395870406062"/>
</node>
<node TEXT="results in many different qualities, conflicting, supporting, etc." ID="ID_1121019148" CREATED="1395870408154" MODIFIED="1395870426644"/>
<node TEXT="We won&apos;t immediately recognize the global intelligence as is because we are part of it." ID="ID_1858044965" CREATED="1402611365475" MODIFIED="1402611383686"/>
<node TEXT="Rogue sections of the brain can be interpreted several ways" ID="ID_373298297" CREATED="1402611383897" MODIFIED="1402611401945">
<node TEXT="Correction mechanism, feedback loop" ID="ID_1459784557" CREATED="1402611402732" MODIFIED="1402611436303">
<icon BUILTIN="full-1"/>
</node>
<node TEXT="Undesirable rogue undermining operations" ID="ID_157773660" CREATED="1402611413882" MODIFIED="1402611437652">
<icon BUILTIN="full-2"/>
</node>
<node TEXT="Conflicting desires/requirements, creating grounding/context for a choice, to which the determining factor(s) is/are external from these sections." ID="ID_1590619132" CREATED="1402611441988" MODIFIED="1402611490708">
<icon BUILTIN="full-3"/>
</node>
</node>
</node>
<node TEXT="History of networking practices" POSITION="left" ID="ID_352594096" CREATED="1395343155897" MODIFIED="1395343168356">
<edge COLOR="#007c7c"/>
<node TEXT="single mainframe" ID="ID_1209048969" CREATED="1395343083712" MODIFIED="1395343163644"/>
<node TEXT="single computers" ID="ID_1974185211" CREATED="1395343073253" MODIFIED="1395343163648"/>
<node TEXT="networked terminals" ID="ID_715792580" CREATED="1395343121627" MODIFIED="1395343163651"/>
<node TEXT="networked computers" ID="ID_1949222467" CREATED="1395343078666" MODIFIED="1395343163654"/>
<node TEXT="hosted apps" ID="ID_289732023" CREATED="1395343095543" MODIFIED="1395343163674"/>
</node>
</node>
</map>
