<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Network Layers" ID="ID_842404706" CREATED="1333134894272" MODIFIED="1333134901966">
<hook NAME="MapStyle" layout="OUTLINE" max_node_width="600"/>
<font BOLD="true" ITALIC="true"/>
<node TEXT="Physical Layer" POSITION="right" ID="ID_428815718" CREATED="1333134908836" MODIFIED="1333134981184" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="provides the media" ID="ID_1165263229" CREATED="1333134960337" MODIFIED="1333139082686">
<font BOLD="true"/>
</node>
<node TEXT="Cable" ID="ID_1797479231" CREATED="1333135399473" MODIFIED="1333135401146"/>
</node>
<node TEXT="Data Link" POSITION="right" ID="ID_565352059" CREATED="1333134916196" MODIFIED="1333134983663" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="provides intranetwork (inter-node) communication (local)" ID="ID_1346045143" CREATED="1333135019729" MODIFIED="1333139129104">
<font BOLD="true"/>
</node>
<node TEXT="Ethernet" ID="ID_1263185840" CREATED="1333135374331" MODIFIED="1333135377932"/>
</node>
<node TEXT="Network" POSITION="right" ID="ID_1482675069" CREATED="1333134930409" MODIFIED="1333134983994" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="provides internetwork communication (connect local networks)" ID="ID_1785622058" CREATED="1333135098468" MODIFIED="1333139148574">
<font BOLD="true"/>
</node>
<node TEXT="IP" ID="ID_850009750" CREATED="1333135442948" MODIFIED="1333135443769"/>
</node>
<node TEXT="Transport" POSITION="right" ID="ID_467900079" CREATED="1333134935148" MODIFIED="1333134984211" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="provides network access to multiple applications simultaneously" ID="ID_846579350" CREATED="1333135500647" MODIFIED="1333139286870">
<font BOLD="true"/>
</node>
<node TEXT="TCP" ID="ID_721416022" CREATED="1333135450305" MODIFIED="1333135451311"/>
</node>
<node TEXT="Session" POSITION="right" ID="ID_117964493" CREATED="1333134940386" MODIFIED="1333134984456" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</node>
<node TEXT="Presentation" POSITION="right" ID="ID_1560103339" CREATED="1333134939274" MODIFIED="1333134984671" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</node>
<node TEXT="Application" POSITION="right" ID="ID_562058667" CREATED="1333134952333" MODIFIED="1333134984931" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
</node>
</node>
</map>
