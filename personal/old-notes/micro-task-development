Bargaining Developers development method:
    "Bargaining for more features and a more refined implementation with
    tests."

'Feature - Micro-Task' development method:
 1. First developer writes flow of process (e.g. happy path) top-down. (writes
    functioning code!)
     + Each assignment is a flow/trace through a use case scenario.
     + Write minimal amount of code to get functional application.
     + Use TDD methodology.
     + Takes care of functional decisions, architectural requirements, etc.
     + Owns and takes care of the interfaces.
 2. Second developer finishes up functioning code with error correction,
    checking+fixing border cases, etc. (finishes up functioning code!)
     + Micro-assignments: each assignment is a method used in the code.
     + Write additional code to satisfy border cases, error handling, argument
       checking, etc.
     + Continues TDD: writes additional tests for border cases etc.
     + Apply design patterns where necessary.
     + Refactor where necessary.
     + "Owns" and takes care of the technical implementation (correctness),
       portability, durability, consistency, etc.
     + Note discrepancies or things that we may need to change.
 3. Third developer a.k.a. the optimization list:
     + Large-scale changes.
     + Issues detected by the first developer (functionally imperfect)
       ? Additional non-functional requirement.
       ? Improvement/Refinement of an existing non-functional requirement.
       ? Layering model to improve separation of concerns for requirements.
          i.e.: Requirements that are absolutely necessary for the program to
          deliver are always put in the first place. It should only provide the
          absolute minimum of functionality required to get the idea working.
          Any other functionals and any non-functionals should be addressed next
          in order of importance.
     + Issues detected by the second developer (technically inefficient)
       ? Problems with regard to the application structure.
       ? Necessary refactoring steps in order to improve maintainability.
       ? Improvements in implementation because of changes to dependencies.
       ? Improvements to the implementation because of better algorithms.

Disadvantages of current methods:
 - Forced to focus on both the functional and technical aspects of development.
 - Focused on both efficiency, as well as implementing the exactly correct
   functional element.

Advantages of FMT method:
 - Pair-programming
 - Both programmers have concrete tasks. (No one has to get bored.)
 - Dependent on each-other: can solve each other's problems.
 - Always provides a functional and a technical view on a problem.
 - Always 2 people that are informed about the written code.
 - Better separation of focus: one focuses on functionality, the other on
   technology.
 - Better separation of focus between the larger whole, and nitpicking every
   piece of code.
 - Competition not necessary, both have their own expertise area.
 - Second developer can be a complete son-of-a-bitch and disallow as much as
   possible. Whenever you break any of the desired functions, tests will fail.
   Methods should be squeezed based on their technical area of expertise, since
   the bottom determines what it definitely can do. The top only focuses on what
   it potentially could do.
 - Sparring partner in case of difficult problem.
 - Although each is competing for his own goal, they work together towards the
   end result.
 - Potentially creates a pressure field: technique pushing inwards for bound/
   limited but correct/complete implementation, while features push outwards for
   ever more spectacular features. This creates a pressure field that should
   come to a natural balance between features and correctness. This may however
   turn out to require 2 technical developers for 1 functional developer or
   whatever balances it out.

--------------------------------------------------------------------------------

Micro-task development model:
 -  Create micro development tasks by having unimplemented methods available
    for implementation.
 -  Top-down development allows for many increasingly lower-level methods that
    have to be implemented.
 -  Good code consists of methods that have many small methods, hence there
    should be lots of methods that are ready to be implemented.
 -  Development environment that can recognize these "tasks" and automatically
    distribute them among developers.
 -  "Tasks" could be distributed after every commit.
 -  Commits should only be done on "feature branch" since we will be generating
    lots of "broken" system commits.
 -  Target one goal at a time.

Micro-task multi-level separation:
 -  Primary programmer applies strict TDD. (Top-down)
    The result is that the main developer develops exactly those tasks that are
    required to produce a correctly working program. Do not require more of this
    developer, since he has produced a (minimal) working program.
 -  Secondary programmer considers valid and invalid inputs and outputs.
    (Top-down)
    The secondary programmer considers which inputs and outputs are supposed to
    be valid or invalid. This programmer adds tests that check for these border
    cases.
 -  Tests that prove functional behavior should be left to fail, since we
    restricted the behaviour by correcting/completing the implementation. The
    primary developer should determine whether or not these tests should fail
    or pass. (If they are required to pass, he should bargain with the secondary
    developer on how to get this to pass and maintain a correct technical
    implementation.)
 -  BONUS: This 2-step process forces at least two people to think about the
    to-be-developed software without forcing them to think about everything at
    once or to without there is any need to do it.
 -  Clear separation of general structure, such as 'happy path', and border
    casing. Border cases usually take time to figure out and clearly "envision".
    It requires the programmer to dive in-depth into the functionality. The
    general structure on the other hand, should be done top-down and requires
    the programmer to not think too deep about the problem, i.e. take the most
    direct solution possible.

--------------------------------------------------------------------------------

Separation of focus (don't force simultaneously considering conflicting
interests):
 * Top-down:
   - Strong focus on writing something that functions.
      ==> Express the happy path of how it should function.
   - Architectural patterns (what patterns to apply to acquire the
     non-functional properties that are required.)
   - interfaces (shaped to usability for the front section)
      : better testable, since better structured
      : more easily useable
   - usability
   - ...
   - security (which user can access which part of an interface/website/service/
     view)
      : Prevent unauthorized access via the front door.
 * Bottom-up:
   - Strong focus on writing something that correctly applies mechanics
     available by the language. (effectivity, efficiency, etc.)
      ==> Correct implementation for every case.
   - Design patterns (Which pattern to apply to acquire a working solution for
     a common problem that also takes care of border cases and exceptions. That
     way we do not have to make the same mistakes again.)
   - Technical correctness/mathematical beauty (such as recursive applicability,
     correctness, completeness, verifiability, validatability, etc.)
      : efficient implementations
      : use of the correct, efficient algorithms
      : applying the correct data structure
      : recognizing and using the right properties
   - ...
   - security (measures to prevent unauthorized access to parts that definitely
     are off-limit)
      : Prevent unauthorized access via the back door.

--------------------------------------------------------------------------------

Questions:
 * Is there any relation between a design pattern and an architectural pattern?
   - Design patterns solve a common (technical implementation) problem. It is
     independent of non-functional requirements.
   - Architectural patterns provide a solution for a non-functional requirement.
     Although it is an implementation, there are often multiple actual
     implementations of such a pattern.
 ==> Am I right in assuming/suspecting that Architectural Patterns clearly
     belong to the Top-down (functional) approach, and Design Patterns clearly
     to the Bottom-up (technical) approach.
  * Competing developers principle:
     - Primary developer codes the functionality. It codes what it should be
       doing, however it must produce something that actually works.
     - Secondary developer codes the technical implementation. It codes what it
       must be able to do. This means limiting the methods to only valid inputs
       based on the technical implementation and what it can handle.
     --> Separation of concerns: functionality vs. correctness
         The concerns of producing a functional application and the concern of
         producing something that is technically correct are opposing values, as
         functionality forces you to be progressive and correctness forces you
         to be conservative.
     - Developers are not required to be from the same company.
  * Is there a class of problems that we are blinded from because of primary
    focus on top-down development, and secondary focus on bottom-up? Some
    in-between/in-the-middle type of problem that both will lose focus of.
  * Will secondary developer kill himself out of boredom/annoyance/whatever?
    Maybe the role of secondary developer will be less interesting because he
    only works at the bottom?
  * Tooling:
    1. The primary developer works on a piece of functional code.
    2. Whenever a piece of functionality is finished, he commits to the
       development server.
    3. After the commit has been made, a piece of software scans all touched
       methods and produces micro tasks for the Secondary developer(s).
    4. The secondary developers pick up the available pieces of code and
       technically finish up the code by finishing the functions, refactoring
       removing bugs, writing additional tests to prove correct functioning,
       etc.
    5. After the secondary developer is finished (i.e. all technical tests pass)
       he commits his code again. Whenever it breaks some of the tests written
       by the primary developer, discussion is required to fix that which has
       been broken. This iterative cycle can then continue.
  * Dangerous hidden assumption: problems don't occur because of limited
    context switches.
    Use of TDD can help to further limit the amount of mistakes being made,
    since it says to test everything that you expect that should happen.
  * Are we going to make mistakes because the perspective of the first developer
    is different from the perspective of the second developer. (And maybe the
    first or second developer made assumptions that aren't communicated.)
    --> Alternatively: there are two approaches to the same solution, which
        makes it easier to "double-check" implementation.
  * This bargaining method can also be used for other purposes. For example:
    Security could be tested by having developers develop the back-end and at
    the same time having security testers that test against the developed
    code. (Got this idea after watching 'Webcast: Hackers beschermen onze
    privacy door te wijzen op lekken.' of nationaalprivacydebat.nl)
  * Should we be alergic to TODOs?
    Don't fear TODOs. Instead, use them to create your list of attention
    points. Bargain with the business side on which items are worth investing
    time in and things that are necessary should not be forgotten. You cannot
    fix everything at the same time, and you shouldn't force your brain to
    remember everything while you're thinking out a solution for a problem.
