<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Docker infrastructure" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1461008263023"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="8"/>
<node TEXT="Management Tooling" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_544812108" CREATED="1461008275573" MODIFIED="1461008780820">
<edge COLOR="#ff0000"/>
<node TEXT="Apache Mesos" ID="ID_1449136551" CREATED="1461008283811" MODIFIED="1461009202693" LINK="https://mesos.apache.org/"/>
<node TEXT="Kubernetes" ID="ID_231740731" CREATED="1461008286522" MODIFIED="1461009223796" LINK="http://kubernetes.io/"/>
<node TEXT="CoreOS Fleet" ID="ID_952056998" CREATED="1461008290410" MODIFIED="1461009254733" LINK="https://github.com/coreos/fleet/"/>
<node TEXT="Docker Swarm" ID="ID_1656305939" CREATED="1461008296595" MODIFIED="1461009273667" LINK="https://www.docker.com/products/docker-swarm"/>
</node>
<node TEXT="Auto-configured appservers" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_65775670" CREATED="1461008956324" MODIFIED="1461009054731">
<edge COLOR="#00ffff"/>
<node TEXT="JuJu" ID="ID_1120215906" CREATED="1461008967746" MODIFIED="1461008971768" LINK="https://jujucharms.com/"/>
</node>
<node TEXT="Auto-deploying applications" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_1988189666" CREATED="1461009090878" MODIFIED="1461009103170">
<edge COLOR="#ffff00"/>
<node TEXT="Otto" ID="ID_494163686" CREATED="1461009109515" MODIFIED="1461009111863" LINK="https://www.ottoproject.io/"/>
</node>
<node TEXT="Auto-scaling infrastructure" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_1741594923" CREATED="1461010389902" MODIFIED="1461010410163">
<edge COLOR="#00007c"/>
</node>
<node TEXT="Differences between management tooling" POSITION="right" ID="ID_1432476145" CREATED="1461008756344" MODIFIED="1461008769334" LINK="https://stackoverflow.com/questions/27640633/docker-swarm-kubernetes-mesos-core-os-fleet">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Concerns" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_575980841" CREATED="1461008305299" MODIFIED="1461008387920">
<edge COLOR="#0000ff"/>
<node TEXT="Dev" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1716047167" CREATED="1461008309428" MODIFIED="1461008397558">
<node TEXT="containers" ID="ID_1695523019" CREATED="1461008313172" MODIFIED="1461008317823"/>
</node>
<node TEXT="Prd" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_536361181" CREATED="1461008311010" MODIFIED="1461008394213">
<node TEXT="Security" ID="ID_511979977" CREATED="1461008328254" MODIFIED="1461008330858"/>
<node TEXT="Code quality" ID="ID_183330470" CREATED="1461008331050" MODIFIED="1461008334642"/>
<node TEXT="Container hosting" ID="ID_480072882" CREATED="1461008334845" MODIFIED="1461008340076"/>
<node TEXT="Peer discovery" ID="ID_442393252" CREATED="1461008340330" MODIFIED="1461008343675"/>
<node TEXT="Configuration management" ID="ID_1541553122" CREATED="1461008343879" MODIFIED="1461008349863"/>
<node TEXT="Configuration changes" ID="ID_1955751371" CREATED="1461008350055" MODIFIED="1461008352435"/>
<node TEXT="Supervision" ID="ID_175064237" CREATED="1461008353150" MODIFIED="1461008357854"/>
<node TEXT="Monitoring" ID="ID_69216815" CREATED="1461008358070" MODIFIED="1461008360779"/>
<node TEXT="Rolling deployment" ID="ID_1696264918" CREATED="1461008361004" MODIFIED="1461008365037"/>
<node TEXT="Networking" ID="ID_516844280" CREATED="1461008365240" MODIFIED="1461008368416"/>
<node TEXT="Management tooling" ID="ID_542812733" CREATED="1461008368612" MODIFIED="1461008377570"/>
<node TEXT="Docker" ID="ID_610589766" CREATED="1461008377763" MODIFIED="1461008379474"/>
</node>
<node TEXT="The &quot;Learning Cliff&quot;" ID="ID_1269349458" CREATED="1461008624722" MODIFIED="1461008642305" LINK="https://twitter.com/mfdii/status/697532387240996864"/>
</node>
<node TEXT="Focus" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_686258914" CREATED="1461009700514" MODIFIED="1461009710703">
<edge COLOR="#7c0000"/>
<node TEXT="What kind of services do we want to provide?" ID="ID_1875121109" CREATED="1461009863467" MODIFIED="1461009947412">
<node TEXT="Limited to advice/consultancy?" ID="ID_929335323" CREATED="1461009883159" MODIFIED="1461009954378"/>
<node TEXT="Take supporting role in managing/deploying?" ID="ID_1842148180" CREATED="1461010065574" MODIFIED="1461010077614"/>
</node>
<node TEXT="Boundaries to define between containers used for development, and managing containers infrastructure." LOCALIZED_STYLE_REF="default" ID="ID_1841973098" CREATED="1461009716204" MODIFIED="1461009753650"/>
<node TEXT="Manageability" ID="ID_1687715378" CREATED="1461010176154" MODIFIED="1461010183754">
<node TEXT="Separating concerns" ID="ID_26413469" CREATED="1461010184645" MODIFIED="1461010187745">
<node TEXT="Persistence" ID="ID_1598205955" CREATED="1461010190713" MODIFIED="1461010193633"/>
<node TEXT="Configurability" ID="ID_1420257380" CREATED="1461010193901" MODIFIED="1461010195982"/>
<node TEXT="Discovery" ID="ID_487254090" CREATED="1461010196269" MODIFIED="1461010197879"/>
<node TEXT="Peers - Consensus" ID="ID_1497789915" CREATED="1461010207022" MODIFIED="1461010278730"/>
</node>
<node TEXT="Scaling" ID="ID_1566838607" CREATED="1461010290163" MODIFIED="1461010291985"/>
</node>
</node>
</node>
</map>
