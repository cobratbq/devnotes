<map version="freeplane 1.2.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Personal &quot;Life&apos;s work&quot;" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1421588866268"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4"/>
<node TEXT="Why defining a personal Life&apos;s work" POSITION="right" ID="ID_924829976" CREATED="1421589316702" MODIFIED="1421589322084">
<edge COLOR="#ff00ff"/>
<node TEXT="Update the rights of the individual to the modern day copyright environment." ID="ID_747305051" CREATED="1421591405719" MODIFIED="1421591417717"/>
<node TEXT="privacy" ID="ID_1375215792" CREATED="1421589324413" MODIFIED="1421589326933"/>
<node TEXT="copyright" ID="ID_1524834800" CREATED="1421589327132" MODIFIED="1421589328350"/>
<node TEXT="protection of the individual" ID="ID_453276257" CREATED="1421589329013" MODIFIED="1421589333373"/>
<node TEXT="defining a &quot;clear&quot; border of operation for personal data by companies" ID="ID_1583205388" CREATED="1421589336126" MODIFIED="1421589355551"/>
<node TEXT="lower bound for privacy (obviously imperfect)" ID="ID_1554325717" CREATED="1421589357854" MODIFIED="1421591444511"/>
<node TEXT="imperfect given that it tries to provide an analogue in terms of notions of copyright" ID="ID_294667481" CREATED="1421589364997" MODIFIED="1421589394340"/>
<node TEXT="create a distinction between &quot;contributing&quot; to others and actions that solely contribute to your personal life&apos;s work (such as searching for something with a search engine for yourself)" ID="ID_197458387" CREATED="1421589415339" MODIFIED="1421591469799"/>
<node TEXT="given this definition, google does not intrude if you ask a search query, but it does intrude if it builds a personal &quot;model&quot; (bad word as it represents incomplete picture) of a individual&apos;s Life work" ID="ID_417205933" CREATED="1421589445014" MODIFIED="1421591490143"/>
<node TEXT="Any reproduction of this first derivation should be considered copyright infringement." ID="ID_1163547229" CREATED="1421589511398" MODIFIED="1421591393863"/>
</node>
<node TEXT="What is personal Life&apos;s work?" POSITION="right" ID="ID_1266988794" CREATED="1421588867694" MODIFIED="1421588874749">
<edge COLOR="#ff0000"/>
<node TEXT="everything that you produce during your lifetime" ID="ID_683406839" CREATED="1421588884534" MODIFIED="1421588893889"/>
<node TEXT="you are your life&apos;s work&apos;s only author" ID="ID_1578996401" CREATED="1421588901932" MODIFIED="1421588912997"/>
<node TEXT="as long as you exist, then the life&apos;s work isn&apos;t finished yet" ID="ID_1347318208" CREATED="1421588914278" MODIFIED="1421588979067"/>
<node TEXT="every action contributes" ID="ID_181903183" CREATED="1421589403405" MODIFIED="1421589411859"/>
<node TEXT="counterpart to gathering of all data that is produced by an individual by companies" ID="ID_823210027" CREATED="1421591291759" MODIFIED="1421591366254"/>
<node TEXT="defines the boundaries for data and privacy for individuals" ID="ID_1178625971" CREATED="1421591341480" MODIFIED="1421591358366"/>
<node TEXT="3 parts" ID="ID_833840778" CREATED="1421623361573" MODIFIED="1421623362631">
<node TEXT="individual components" ID="ID_1939807192" CREATED="1421623363165" MODIFIED="1421623365547"/>
<node TEXT="composition" ID="ID_1317856361" CREATED="1421623365917" MODIFIED="1421623368388"/>
<node TEXT="first derivative" ID="ID_1487348614" CREATED="1421623370557" MODIFIED="1421623373373"/>
</node>
</node>
<node TEXT="Other copyright thingies" POSITION="right" ID="ID_1498973440" CREATED="1421589001957" MODIFIED="1421589009419">
<edge COLOR="#00ff00"/>
<node TEXT="music" ID="ID_269578867" CREATED="1421589009901" MODIFIED="1421589011607">
<node TEXT="individual components" ID="ID_1723168185" CREATED="1421589026366" MODIFIED="1421589031306">
<node TEXT="NOT COPYRIGHTED" ID="ID_1941936482" CREATED="1421589135925" MODIFIED="1421589139412"/>
<node TEXT="musical notes" ID="ID_943876137" CREATED="1421589033148" MODIFIED="1421589037814"/>
<node TEXT="pauses" ID="ID_1884158915" CREATED="1421589039109" MODIFIED="1421589044079"/>
<node TEXT="instruments" ID="ID_1525474748" CREATED="1421589044268" MODIFIED="1421589046521"/>
<node TEXT="..." ID="ID_685631134" CREATED="1421589046980" MODIFIED="1421589055965"/>
</node>
<node TEXT="composition?" ID="ID_466520194" CREATED="1421623328245" MODIFIED="1421623330895"/>
<node TEXT="only the author truly understands the way in which he intended the base components to work together (even though it may be hard to put in words)" ID="ID_1210102508" CREATED="1421589061156" MODIFIED="1421589159390"/>
<node TEXT="first derivative" ID="ID_942261495" CREATED="1421589056748" MODIFIED="1421589059838">
<node TEXT="IS COPYRIGHTED" ID="ID_1570284135" CREATED="1421589142958" MODIFIED="1421589146653"/>
<node TEXT="the song" ID="ID_1564845681" CREATED="1421589102573" MODIFIED="1421589112569"/>
<node TEXT="the live performance" ID="ID_1254582615" CREATED="1421589112852" MODIFIED="1421589120529"/>
</node>
</node>
<node TEXT="texts (books, papers, etc.)" ID="ID_371986146" CREATED="1421589011861" MODIFIED="1421589024714">
<node TEXT="individual components" ID="ID_389174315" CREATED="1421589166733" MODIFIED="1421589169910">
<node TEXT="NOT COPYRIGHTED" ID="ID_320653402" CREATED="1421589179789" MODIFIED="1421589181700"/>
<node TEXT="paper" ID="ID_1927714934" CREATED="1421589170502" MODIFIED="1421589171667"/>
<node TEXT="words" ID="ID_1323997856" CREATED="1421589171924" MODIFIED="1421589173766"/>
</node>
<node TEXT="first derivative (of the whole work or parts thereof)" ID="ID_944320980" CREATED="1421593388242" MODIFIED="1421593422320">
<node TEXT="the story" ID="ID_978875786" CREATED="1421593392243" MODIFIED="1421593394875"/>
<node TEXT="the inventions" ID="ID_328722437" CREATED="1421593424841" MODIFIED="1421593432611"/>
</node>
</node>
<node TEXT="computer program" ID="ID_1049550837" CREATED="1421623022966" MODIFIED="1421623027073">
<node TEXT="individual components" ID="ID_1105264301" CREATED="1421623027637" MODIFIED="1421623034535">
<node TEXT="words of programming language" ID="ID_1222108671" CREATED="1421623035117" MODIFIED="1421623054552"/>
<node TEXT="words of scripting language" ID="ID_1430792026" CREATED="1421623055269" MODIFIED="1421623185687"/>
<node TEXT="images (though they can be copyrighted in itself, ofcourse)" ID="ID_385854324" CREATED="1421623134877" MODIFIED="1421623146294"/>
<node TEXT="tools (compiler, linker, etc.)" ID="ID_123997918" CREATED="1421623114726" MODIFIED="1421623119906"/>
</node>
<node TEXT="composition?" ID="ID_1859553510" CREATED="1421623165115" MODIFIED="1421623166829">
<node TEXT="words of scripts into script" ID="ID_1084662934" CREATED="1421623172893" MODIFIED="1421623178583"/>
<node TEXT="programming language story into executable" ID="ID_138612029" CREATED="1421623225917" MODIFIED="1421623252585"/>
<node TEXT="scripts + programs + images into program" ID="ID_220530687" CREATED="1421623237789" MODIFIED="1421623250229"/>
</node>
<node TEXT="first derivative" ID="ID_421984017" CREATED="1421623157341" MODIFIED="1421623160463"/>
</node>
</node>
<node TEXT="Protecting your personal Life&apos;s work" POSITION="right" ID="ID_1236332501" CREATED="1421588875134" MODIFIED="1421588882410">
<edge COLOR="#0000ff"/>
<node TEXT="cannot be sold, promised, etc. - since it isn&apos;t finished" ID="ID_1782331643" CREATED="1421588897814" MODIFIED="1421588968077"/>
<node TEXT="automatic rights, since you&apos;re the author" ID="ID_1430122565" CREATED="1421589545190" MODIFIED="1421589555710"/>
<node TEXT="individual components" ID="ID_1476634127" CREATED="1421589187166" MODIFIED="1421589190854">
<node TEXT="search operation" ID="ID_616800964" CREATED="1421589191349" MODIFIED="1421589194624"/>
<node TEXT="thing you say" ID="ID_280780837" CREATED="1421589194798" MODIFIED="1421589205121"/>
<node TEXT="thing you do" ID="ID_1547273241" CREATED="1421589205316" MODIFIED="1421589207388"/>
<node TEXT="something you create" ID="ID_1867136051" CREATED="1421589286253" MODIFIED="1421589288705"/>
<node TEXT="something you learn" ID="ID_1404541482" CREATED="1421589291589" MODIFIED="1421589294390"/>
<node TEXT="etc." ID="ID_540491579" CREATED="1421589294765" MODIFIED="1421589295411"/>
</node>
<node TEXT="individual components contribute to the life&apos;s work" ID="ID_1120066647" CREATED="1421589215733" MODIFIED="1421589224948"/>
<node TEXT="first derivative is the life&apos;s work itself" ID="ID_410944023" CREATED="1421589263950" MODIFIED="1421589270526">
<node TEXT="only the author truly understands ..." ID="ID_832642478" CREATED="1421589302653" MODIFIED="1421589308084"/>
<node TEXT="cannot be recreated with authorization" ID="ID_1556604417" CREATED="1421590614983" MODIFIED="1421590620986"/>
<node TEXT="this in particular should be a problem for advertising companies and data mining companies" ID="ID_1918184671" CREATED="1421590622215" MODIFIED="1421590639999"/>
<node TEXT="much more than it troubles service companies, such as Google, as google is allowed to work with the individual components" ID="ID_792961442" CREATED="1421590640461" MODIFIED="1421590667871"/>
</node>
<node TEXT="incentive to not store more information than necessary" ID="ID_356642137" CREATED="1421622301309" MODIFIED="1421622310542"/>
<node TEXT="set base level for information management, lay groundwork for boundaries such that companies know not to go too far" ID="ID_273824701" CREATED="1421622311646" MODIFIED="1421622378944"/>
<node TEXT="by demotivating companies to do anything to gather data, motivates to provide good products (products without need for cloud)" ID="ID_1454649050" CREATED="1421622419629" MODIFIED="1421622462821"/>
</node>
</node>
</map>
