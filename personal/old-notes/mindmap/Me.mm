<map version="0.9.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Me" ID="ID_1077474800" CREATED="1328570939238" MODIFIED="1328571295029">
<hook NAME="MapStyle" max_node_width="600"/>
<font NAME="SansSerif" SIZE="12" BOLD="true" ITALIC="false"/>
<node TEXT="TODO" POSITION="right" ID="ID_1160751625" CREATED="1328570984934" MODIFIED="1328571289534" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Download TED Talks" ID="ID_373432228" CREATED="1328570991048" MODIFIED="1328571286454">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Download KhanAcademy" ID="ID_1557314818" CREATED="1328571001671" MODIFIED="1328571286452">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Cyril terugbetalen (&#x20ac; 7,35, broodje van Subway)" ID="ID_1736357178" CREATED="1328571303949" MODIFIED="1328656703812">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Download The Current podcast (cbc.ca/thecurrent)" ID="ID_264763069" CREATED="1328656708545" MODIFIED="1328656726384"/>
</node>
<node TEXT="Projects" POSITION="right" ID="ID_432599476" CREATED="1328571010560" MODIFIED="1328571290064" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="Device Driver Tool" ID="ID_783470520" CREATED="1328571016473" MODIFIED="1328571286447">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Interactive Visualization" ID="ID_889260775" CREATED="1328571028293" MODIFIED="1328571286445">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
<node TEXT="Use eXist-db for storage of XLog" ID="ID_1938272438" CREATED="1328656630520" MODIFIED="1328656655053"/>
<node TEXT="Experiment with eXist-db as event pool (massive store for XEvent elements)" ID="ID_817485481" CREATED="1328656655664" MODIFIED="1328656673080"/>
<node TEXT="Fine-tune the color generator" ID="ID_999079862" CREATED="1328656675688" MODIFIED="1328656692804"/>
</node>
</node>
<node TEXT="Bedrijven" POSITION="left" ID="ID_1094407261" CREATED="1328571220603" MODIFIED="1328571288766" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="12" BOLD="true" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#808080" WIDTH="thin"/>
<node TEXT="VGO Advies" ID="ID_487570521" CREATED="1328571223619" MODIFIED="1328571286437">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Haga" ID="ID_1963827908" CREATED="1328571225539" MODIFIED="1328571286436">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
<node TEXT="Source Finance" ID="ID_1776948240" CREATED="1328571228375" MODIFIED="1328571286433">
<font NAME="SansSerif" SIZE="12" BOLD="false" ITALIC="false"/>
</node>
</node>
</node>
</map>
