# Insightful

Notes on insights and insightful opinions.

## Discussion

- Alternative view on the nature and usefulness of conspiracy theories: [a playful intellectual exercise][joe-rogan-experience-1343-playful-intellectual-exercise] ([full discussion][joe-rogan-experience-1343-conspiracy-theories])

## Quotes

- > I'm trying to not think ever of "us" and "them". I'm trying to say "those of us who voted for Trump" and "those of us who believe this". So it is always "us". -- Penn Jillette ([source][joe-rogan-experience-1343-penn-jillette])
- > With or without religion, good people can behave well and bad people can do evil; but for good people to do evil - that takes religion. -- Steven Weinberg
- > I like to get things done. I like to be useful. That is one of the hardest things to do; is to be useful. [...] Doing something useful for other people. That I like doing. -- Elon Musk ([source](https://youtu.be/ycPr5-27vSI?t=8364))
- > One of the great challenges in life is knowing enough to think you're right, but not enough to know you're wrong. -- Neil deGrasse Tyson
- > The only point of the scientific method is to make sure you are not fooled into thinking that something is true that is not, or thinking that something is not true that is. -- Neil deGrasse Tyson
- > ... all of these extraordinary changes that are taking place, that mean that, instead of us being at the mercy of the elements, the elements - you know - in a single generation, are at the mercy of us. -- Alanna Mitchell (Documentary: Melting ice)
- > If we don't believe in freedom of expression for people we despise, we don't believe in it at all. -- Noam Chomsky

## Notes

- When pitching your product to your (enterprise) customers:  
  > "why buy anything?"  
  > "why buy us?"  
  > "why buy now?"

[joe-rogan-experience-1343-penn-jillette]: https://www.youtube.com/watch?v=1OnBVqeW5JA&feature=youtu.be&t=1090 "Joe Rogan Experience #1343 - Penn Jillette"
[joe-rogan-experience-1343-playful-intellectual-exercise]: https://www.youtube.com/watch?v=1OnBVqeW5JA&feature=youtu.be&t=4204 "Joe Rogan Experience #1343 - Penn Jillette: conspiracy theories as an playful intellectual exercise"
[joe-rogan-experience-1343-conspiracy-theories]: https://www.youtube.com/watch?v=1OnBVqeW5JA&feature=youtu.be&t=3734 "Joe Rogan Experience #1343 - Penn Jillette: conspiracy theories"
