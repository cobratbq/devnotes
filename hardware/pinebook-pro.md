# Pinebook Pro

Notes on the one-time initialization and many-time installation/configuration of the [Pinebook Pro][wiki-pinebook-pro].

## One-time initialization

1. Install touchpad and keyboard firmware update ([original][original-keyboard-touchpad-firmware], [jackhumbert][jackhumbert-keyboard-touchpad-firmware])
   - Follow the instructions in the repository. Note that the ANSI keyboard and touchpad will lose their function in between update steps 1 and 2. An USB keyboard is necessary to finish the process.
   - [Custom firmware by JackHumbert][jackhumbert-keyboard-touchpad-firmware] provides an additional upgrade step that [restores some malfunctioning keyboard bindings.][keyboard-mapping-issue]

## Installation/configuration

1. Install [Manjaro Linux ARM for Pinebook Pro][manjaro-linux-pinebook-pro] ([forum thread][manjaro-linux-forum-thread] with installation remarks)
1. Install and enable firewall: (don't do this remotely)
   - `pacman -S firewalld`, use `firewall-cmd` for configuration.
1. Enable smartcard support  
   ```
   sudo pacman -S pcsclite ccid
   sudo systemctl enable --now pcscd
   gpg --card-status
   ```
1. Install/update software:
   1. Update _mesa_: `sudo pacman -S mesa-git`  
      Note that default `fbturbo` video driver does not leverage full graphics card capabilities, but performs remarkably well with new `mesa-git` package. `fbturbo` does not produce graphics artifacts. This may be enough for many use cases, although not for games.
      - If you keep with `fbturbo`, configure _X11 video output (XCB)_ in VLC, for smooth full-screen playback.
   1. If opting to use `panfrost` driver instead of `fbturbo` already:
      1. `sudo pacman -Rc xf86-video-fbturbo-git`
      1. `sudo pacman -S picom`
      1. Configure `/usr/bin/picom` to run at user log-in.
   1. Install misc applications:  
      `sudo pacman -S flatpak neovim gnome-disk-utility`
      1. Flatpak, for self-contained software packages off [FlatHub](https://flathub.org)
      1. Neovim (`neovim`)
      1. Disks (`gnome-disk-utility`)
   1. Install flatpak packages: (install: `flatpak install <identifier>`, search: `flatpak search <keyword>`)
      1. VLC (`org.videolan.VLC`)
      1. Clementine (`org.clementine_player.Clementine`)
      1. Transmission (`com.transmissionbt.Transmission`)
      1. LibreOffice (`org.libreoffice.LibreOffice`)
   1. Consider cleaning up packages you don't urgently need. Maybe you prefer flatpak installs over installed packages.
1. Tweaking the linux configuration:
   1. See `config` directory for configuration scripts with explanatory comments.
   1. Enable suspend:  
      ```
      [Sleep]
      AllowSuspend=yes
      SuspendState=freeze
      ```
1. Tweaking the desktop experience:
   1. Consider disabling anti-aliasing for better, more comfortable reading in case of frequent use:
      - Open "Appearance" settings, go to tab "Fonts".
      - Uncheck 'Enable anti-aliasing'
      - Set 'Hinting' to `Full`
      - Possibly, increase DPI: set "Custom DPI setting" to `136`
      - You can apply the same configuration in "LightDM GTK+ Greeter settings" for the log-in screen.
1. Disabling `tlp` for excessive power saving actions:
   - `systemctl stop tlp`
   - Mask (`systemctl mask tlp`) or uninstall (`pacman -Rss tlp`).

## Issues

- [Flaky WiFi connection, intermediate drops/crashes](https://forum.pine64.org/showthread.php?tid=8822)
  - Disable power management: `sudo iw dev wlan0 set power_save off` (Most likely better to uninstall `tlp`.)
- [Keyboard key mapping issue][keyboard-mapping-issue] ([unofficial solution][jackhumbert-keyboard-touchpad-firmware])
- [Suspend to RAM is not working (yet)][manjaro-linux-forum-thread] (at least for Manjaro Linux for ARM)
- Graphics driver for Mali graphics card: Manjaro's `mesa-git` package has decent support, but far from optimal.  
  Note: make sure `xf86-video-fbturbo-git` is removed, otherwise it will get priority over panfrost driver.

## Troubleshooting

- WiFi issues, disconnecting, re-requesting wifi password, etc.
  - Try removing `/usr/lib/firmware/brcm/brcmfmac43456-sdio.clm_blob`.
  - Ensure XFCE4 runs _Gnome Keyring: secrets service_.
  - Try storing WiFi profile with password stored for individual user, i.e. use user's keyring.

## References

- [Pinebook Pro wiki-page][wiki-pinebook-pro]
- [Pinebook Pro debian desktop releases][debian-desktop-releases]
- [Pinebook Pro debian updates repository][debian-updates-repo]
- [Manjaro Linux for Pinebook Pro][manjaro-linux-pinebook-pro]
- [Unofficial good firmware](https://gitlab.manjaro.org/tsys/pinebook-firmware.git)
- [Community-managed WiFi adapter firmware](https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware)



[wiki-pinebook-pro]: https://wiki.pine64.org/index.php/Pinebook_Pro "PINE64 Pinebook Pro wiki-page"
[debian-desktop-releases]: https://github.com/mrfixit2001/debian_desktop/releases "github.com/mrfixit2001/debian_desktop"
[debian-updates-repo]: https://github.com/mrfixit2001/updates_repo "github.com/mrfixit2001/updates_repo"
[original-keyboard-touchpad-firmware]: https://github.com/ayufan-rock64/pinebook-pro-keyboard-updater/issues "original keyboard, touchpad firmware by ayufan-rock64"
[jackhumbert-keyboard-touchpad-firmware]: https://github.com/jackhumbert/pinebook-pro-keyboard-updater "custom keyboard, touchpad firmware by JackHumbert"
[manjaro-linux-pinebook-pro]: https://manjaro.org/download/arm/pinebook-pro/arm8-pinebook-pro-xfce/ "Manjaro Linux for Pinebook Pro - XFCE"
[manjaro-linux-forum-thread]: https://forum.pine64.org/showthread.php?tid=8248 "Manjaro ARM 19.12 Official Release - PineBook Pro"
[PINE64 Linux on Pinebook Pro forum]: https://forum.pine64.org/forumdisplay.php?fid=114 "PINE64 - Pinebook Pro - Linux on Pinebook Pro"
[keyboard-mapping-issue]: https://github.com/ayufan-rock64/pinebook-pro-keyboard-updater/issues/14 "[ANSI] Fn + F[9-12] has wrong assignment after firmware update #14"

