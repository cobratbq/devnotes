# Notebooks

Notes on hardware and software considerations for notebooks.

## Buying considerations

- Matte display panel (possibly IPS panel, but TN may be good enough)
  - avoid harmful/tiring constant reflection
- Sufficiently high resolution
  - avoid having to enable anti-aliasing/font-smoothing for reasonably looking fonts
- Supports [LVFS](https://fwupd.org) (`fwupdmgr`) firmware updates
- Thin body
- Sufficient USB ports
- Flash-/NAND-based storage.
- Gaming:
  - OR: dedicated GPU (Nvidia/AMD/Intel Iris Xe(?)
  - OR: check [eGPU](https://egpu.io) compatibility: check for optimal hardware configuration for eGPU bandwidth:
    - Thunderbolt version
    - necessary dedicated PCIe lanes
    - configuration for optimal bandwidth utilization (e.g. use external display as opposed to laptop screen)
- OPTIONAL
  - linux formally supported
  - SD card reader
