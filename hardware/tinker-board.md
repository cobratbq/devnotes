# Asus Tinker Board

## Building u-boot

> ⚠ Armbian has 10+ patches prepared to make Tinker Board function correctly. Building own u-boot and/or using other OS kernel may be suboptimal in almost all cases. ⚠

```sh
git clone https://salsa.debian.org/debian/u-boot.git
cd u-boot
make clean
make tinker-rk3288_defconfig
ARCH=armhf CROSS_COMPILE=arm-none-eabi- make -j4 all
```

Recent configs have EFI and ESP support enabled by default.

Best guess, but haven't been able to make it work yet:

```sh
dd if=idbloader.img of=/dev/sdc seek=64 conv=notrunc,fsync
dd if=u-boot-dtb.img of=/dev/sdc seek=16384 conv=notrunc,fsync
```

... no conclusive method for making Tinker Board bootable yet.

## References

- <https://build.opensuse.org/package/show/hardware:boot/u-boot>
- <https://en.opensuse.org/openSUSE:OpenSUSE_on_your_ARM_board>
- <https://download.opensuse.org/ports/armv7hl/tumbleweed/appliances/>
- <https://en.opensuse.org/HCL:Tinker_Board>
- <https://salsa.debian.org/debian/u-boot>
- <https://deb.debian.org/debian/dists/testing/main/installer-armhf/current/images/u-boot/>

