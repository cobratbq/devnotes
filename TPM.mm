<map version="freeplane 1.8.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Trusted Platform Module (TPM)" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1213036125" CREATED="1604069123621" MODIFIED="1604069981831"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#cc0000">
<font ITALIC="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" COLOR="#ff0000">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Security" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_1902339399" CREATED="1604070312695" MODIFIED="1604070316543">
<edge COLOR="#0000ff"/>
<node TEXT="Root of Trust" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1130712605" CREATED="1604071874291" MODIFIED="1604071882789">
<node TEXT="How many entities do we need to trust?" ID="ID_968610088" CREATED="1604071884683" MODIFIED="1604071893590"/>
<node TEXT="What entities can we trust?" ID="ID_1628184395" CREATED="1604071893740" MODIFIED="1604071899787"/>
<node TEXT="Trusting device manufacturer is trivial given that they also manufactured the device itself. (Or said otherwise, if you do not trust that manufacturer, you should not use the device at all.)" ID="ID_735900390" CREATED="1604071900027" MODIFIED="1604071949495"/>
</node>
<node TEXT="Concerns" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1739676704" CREATED="1604071821764" MODIFIED="1604071827281">
<node TEXT="Machine identity" ID="ID_1489286350" CREATED="1604071828968" MODIFIED="1604071833412"/>
<node TEXT="User identity" ID="ID_380791440" CREATED="1604071833566" MODIFIED="1604071836166"/>
<node TEXT="Reliance on truthful components. OS not trustworthy (compromise)." ID="ID_1783737491" CREATED="1604071848339" MODIFIED="1604071869253"/>
<node TEXT="Provide a single identity for a device, for use by many applications." ID="ID_1730621983" CREATED="1604072946001" MODIFIED="1604072965359"/>
</node>
<node TEXT="Degrees of Trust" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_241040620" CREATED="1604070320648" MODIFIED="1604071819488">
<node TEXT="TPM" ID="ID_553975836" CREATED="1604070331824" MODIFIED="1604070334123"/>
<node TEXT="Secure Boot" ID="ID_1347560814" CREATED="1604070334239" MODIFIED="1604070335995"/>
<node TEXT="Attestable" ID="ID_1000033535" CREATED="1604070337239" MODIFIED="1604073139491">
<node TEXT="device identity + user identity + device state" ID="ID_1900764910" CREATED="1604073142210" MODIFIED="1604073145444"/>
</node>
</node>
<node TEXT="Event Log" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1651862211" CREATED="1604073261935" MODIFIED="1604073264942">
<node TEXT="Firmware measurement" ID="ID_1548084934" CREATED="1604073266968" MODIFIED="1604073271231"/>
<node TEXT="Secure boot configuration" ID="ID_1232250938" CREATED="1604073271352" MODIFIED="1604073275345"/>
<node TEXT="Secure boot databases" ID="ID_1258155253" CREATED="1604073275463" MODIFIED="1604073279483"/>
<node TEXT="UEFI boot targets" ID="ID_579343987" CREATED="1604073279792" MODIFIED="1604073283277"/>
<node TEXT="Partition table" ID="ID_1879507387" CREATED="1604073283414" MODIFIED="1604073287275"/>
<node TEXT="Used UEFI boot keys" ID="ID_1746427803" CREATED="1604073287416" MODIFIED="1604073291296"/>
<node TEXT="Bootloader (with path)" ID="ID_984663617" CREATED="1604073291423" MODIFIED="1604073298629"/>
<node TEXT="OS-specific data" ID="ID_1135124709" CREATED="1604073298967" MODIFIED="1604073303030"/>
<node TEXT="ExitBootServices state" ID="ID_966503383" CREATED="1604073303201" MODIFIED="1604073309020"/>
</node>
</node>
<node TEXT="Components" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_576921784" CREATED="1604073323407" MODIFIED="1604073325989">
<edge COLOR="#ff00ff"/>
<node TEXT="PCRs" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_111267442" CREATED="1604073328984" MODIFIED="1604073365937"/>
<node TEXT="UEFI keys" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1705506988" CREATED="1604073331719" MODIFIED="1604073366458">
<node TEXT="PK" ID="ID_268392431" CREATED="1604073345807" MODIFIED="1604073347056">
<node TEXT="&quot;Platform Key&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_176645774" CREATED="1604073478857" MODIFIED="1604073485201"/>
<node TEXT="only single key (typically)" ID="ID_112962340" CREATED="1604073356638" MODIFIED="1604073510781"/>
<node TEXT="is NOT allowed to sign PE bootables" ID="ID_1694733205" CREATED="1604073511015" MODIFIED="1604073525235"/>
<node TEXT="signs `KEK` keys" ID="ID_1088380779" CREATED="1604073532984" MODIFIED="1604073606119"/>
</node>
<node TEXT="KEK" ID="ID_1271681851" CREATED="1604073347286" MODIFIED="1604073348221">
<node TEXT="&quot;Key Exchange Key&quot;" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_210746709" CREATED="1604073489537" MODIFIED="1604073495312"/>
<node TEXT="multiple keys" ID="ID_1210187892" CREATED="1604073578968" MODIFIED="1604073581575"/>
<node TEXT="signs `db` keys for signing bootables" ID="ID_1254167837" CREATED="1604073551152" MODIFIED="1604073573581"/>
</node>
<node TEXT="db" ID="ID_68298219" CREATED="1604073348358" MODIFIED="1604073351253">
<node TEXT="bootables signing keys" ID="ID_1034254217" CREATED="1604073585568" MODIFIED="1604073593529"/>
<node TEXT="multiple keys" ID="ID_1707611968" CREATED="1604073593842" MODIFIED="1604073595637"/>
</node>
<node TEXT="dbx" ID="ID_841615084" CREATED="1604073351695" MODIFIED="1604073352336">
<node TEXT="exclusion, banned keys overriding `db`" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1525334449" CREATED="1604073380751" MODIFIED="1604073763494"/>
</node>
</node>
<node TEXT="bootable PE-executables" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_110604068" CREATED="1604073773128" MODIFIED="1604073984964">
<node TEXT="signed for Secure Boot" ID="ID_1729959515" CREATED="1604073792216" MODIFIED="1604073797931"/>
<node TEXT="signing keys must be present in `db` and not excluded by `dbx`" ID="ID_283736976" CREATED="1604074000807" MODIFIED="1604074013412"/>
</node>
</node>
<node TEXT="Services" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="right" ID="ID_836813049" CREATED="1604072977039" MODIFIED="1604072982967">
<edge COLOR="#00ff00"/>
<node TEXT="Attestation (Remote)" ID="ID_1108244310" CREATED="1604072985416" MODIFIED="1604073060871">
<node TEXT="Direct Anonymous Attestation (DAA) using zero-knowledge proof (ZKP)" ID="ID_145634298" CREATED="1628192315066" MODIFIED="1628192452282"/>
<node TEXT="attestation via AIK" ID="ID_1511729868" CREATED="1628192433251" MODIFIED="1628192440301"/>
</node>
<node TEXT="Sealing/Unsealing (confidentiality)" ID="ID_131643270" CREATED="1604072988192" MODIFIED="1604073036196">
<node TEXT="only accessible through device" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_187470388" CREATED="1604073042114" MODIFIED="1604073053660"/>
</node>
<node TEXT="Proof of trust (implies non-compromised)" ID="ID_798485408" CREATED="1604073071023" MODIFIED="1604073221203"/>
<node TEXT="Secure Boot" ID="ID_1401438373" CREATED="1604073803879" MODIFIED="1604073834900">
<node TEXT="measured, verified boot" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_807797601" CREATED="1604073837167" MODIFIED="1604073844492"/>
<node TEXT="confirm, measure which boot path taken during boot process (e.g. only Microsoft UEFI boot keys)" ID="ID_1105007837" CREATED="1604073934183" MODIFIED="1604073966057"/>
</node>
<node TEXT="Measured Event Log" ID="ID_211392271" CREATED="1604073812173" MODIFIED="1604073883912">
<node TEXT="measured through checksumming, attested using particular PCR registers" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_987714673" CREATED="1604073849936" MODIFIED="1604073973120"/>
<node TEXT="&quot;replayable&quot; against TPM PCR value to confirm expected event log entries (reasonable security)" ID="ID_297736138" CREATED="1604073892583" MODIFIED="1604073930206"/>
</node>
</node>
<node TEXT="References" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="left" ID="ID_483543043" CREATED="1604069179861" MODIFIED="1604074065287">
<edge COLOR="#ff0000"/>
<node TEXT="Entities" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_254082110" CREATED="1604074086839" MODIFIED="1604074088629">
<node TEXT="TPM manufacturer" ID="ID_704064098" CREATED="1604070472879" MODIFIED="1604070538607">
<node TEXT="Endorsement Key" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1150104181" CREATED="1604070489359" MODIFIED="1604071987072">
<node TEXT="Endorsement Key (EK)" ID="ID_457972122" CREATED="1628192399972" MODIFIED="1628192405658"/>
<node TEXT="not changed at TPM lifetime" ID="ID_378640719" CREATED="1604071989714" MODIFIED="1604072167022"/>
<node TEXT="generates Attestation Keys, can prove it generated them" ID="ID_544130873" CREATED="1604072153921" MODIFIED="1604072162840"/>
</node>
<node TEXT="Endorsement Certificate" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1838627451" CREATED="1604071999826" MODIFIED="1604072008976">
<node TEXT="certifying EK by manufacturer, linking trust to manufacturer" ID="ID_1087257201" CREATED="1604070504623" MODIFIED="1604072171152"/>
</node>
<node TEXT="Attestation Keys" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_233170903" CREATED="1604072177427" MODIFIED="1604072182688">
<node TEXT="Identity Attestation Identity Key (IAK)" ID="ID_387253088" CREATED="1628192418026" MODIFIED="1628206338836">
<node TEXT="Inserted by OEM at production time" ID="ID_224959509" CREATED="1628206360183" MODIFIED="1628206367547"/>
</node>
<node TEXT="Local Attestation Key (LAK)" ID="ID_941482096" CREATED="1628206339998" MODIFIED="1628206347127">
<node TEXT="Inserted by owner for (long-term) use,&#xa;but can be cleared/refreshed" ID="ID_555338478" CREATED="1628206370148" MODIFIED="1628206397031"/>
</node>
<node TEXT="generated by TPM, related to Endorsement Keys" ID="ID_1672530805" CREATED="1604072185309" MODIFIED="1604072200341"/>
<node TEXT="LAK may overlay IAK or may be used next to IAK." ID="ID_1064393386" CREATED="1628206402021" MODIFIED="1628206439038"/>
</node>
</node>
<node TEXT="Device manufacturer" ID="ID_1837653245" CREATED="1604070478207" MODIFIED="1604070540074">
<node TEXT="Platform Certificate" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1213551851" CREATED="1604070521934" MODIFIED="1604070545763"/>
</node>
<node TEXT="Trusted Computing Group (TCG)" ID="ID_1565402318" CREATED="1604069392420" MODIFIED="1604070697590" LINK="https://trustedcomputinggroup.org/"/>
</node>
<node TEXT="Specifications" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1117246288" CREATED="1604073629240" MODIFIED="1604074106007">
<node TEXT="TPM 1.2" ID="ID_38596481" CREATED="1604073634664" MODIFIED="1604073638412"/>
<node TEXT="TPM 2.0" ID="ID_1463855828" CREATED="1604073638577" MODIFIED="1604073640948"/>
</node>
<node TEXT="Tooling" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_1351218330" CREATED="1604074068199" MODIFIED="1604074083425">
<node TEXT="tpm2-tools" ID="ID_852364622" CREATED="1604074137204" MODIFIED="1604074143342" LINK="https://github.com/tpm2-software/tpm2-tools"/>
<node TEXT="go-attestation" ID="ID_148378180" CREATED="1604074073503" MODIFIED="1604074079902" LINK="https://github.com/google/go-attestation"/>
</node>
</node>
</node>
</map>
