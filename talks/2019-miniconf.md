# Beyond Java 11 - The road to Valhalla ... and Amber, Loom, Panama, Metropolis

- Required: 15-20 minutes

# Amber

Simplifying Java syntax / "_Right-sizing language ceremony_"

__Goal__: Help you to express more clearly what you mean when you write Java code.

Mark Reinhold: "Still static typing, but improved _keyboard_-typing" (paraphrased)

- JEP301: Enhanced enums
  - Generic type parameters
- JEP302: Lambda left-overs
  - Single underscore for unused parameters
- JEP325: switch-expression (JDK12 PREVIEW):
  - respects missing `default` if all cases satisfied.
  - combined cases
- JEP326: Raw string literals (JDK12) (`c:\Users\simon`)
  - includes multi-line support
- JEP305: Pattern matching:
  - switch by type instead of by value.
- JEP323: Local variable syntax for Lambda parameters (JDK10)
- JEP286: Local-variable type inference (JDK11)
- ...: Sealed classes, i.e. fixed, predefined set of subtypes, such that compiler knows exactly what types exist and can be sure there is nothing else.

Very preliminary: [JEP draft 8209434](https://openjdk.java.net/jeps/8209434) on concise method bodies

Historic improvements:

> List&lt;String> empty = Collections.__&lt;String>__emptyList();  
List<String> empty = Collections.emptyList();  
List&lt;String> empty = Collections.__&lt;String>__emptyList();  
List&lt;String> empty = Collections.__<>__emptyList();  
Predicate<String> isEmpty = __(String s)__ -> s.length() == 0;  
Predicate<String> isEmpty = __s__ -> s.length() == 0;  
__BufferedReader__ reader = new BufferedReader(new FileReader(file));  
__var__ reader = new BufferedReader(new FileReader(file));

Switch expressions:
```java
int numLetters = switch (day) {
  case MONDAY, FRIDAY, SUNDAY -> 6;
  case TUESDAY -> 7;
  case THURSDAY, SATURDAY -> 8;
  case WEDNESDAY -> 9;
  default -> throw new IllegalArgumentException("Huh?: " + day);
};
```
- No default needed if compiler can verify if all cases are satisfied.

Pattern-matching:
```java
if (o instanceof Integer intValue) {
  // use intValue
}
```
- No longer need to do explicit cast, and evaluate the type twice.

Pattern matching and record type deconstruction:
```java
if (shape instanceof Circle(var center, var radius)) {
  // use deconstructed components immediately
}
```

Combining pattern matching, record type deconstruction and switch expressions:
```java
String format(Object o) {
  return switch (o) {
    case Integer i -> String.format("int %d", i);
    case Double d -> String.format("double %f", d);
    case Point(x, y) -> String.format("point %f %f", x, y);
    default -> "unknown";
  };
}
```

# Loom

Continuations and fibers

Threads are too heavy-weight.

Make concurrency simple again. (To benefit of parallel computation, i.e. multi-processing and multi-threading.)

`Continuation` a light-weight program object representing a pausable execution of logic. A low-level construct that developers will not typically use. `yield` method to give control back to continue later. (Support is completely in Java VM)

Fibers:
- Concurrent systems:
  - you already benefit on a single thread, due to ability to yield and switch tasks.
  - allows writing in blocking/synchronous style - sequential thinking
- Managed/scheduled by Java i.s.o. operating system.
- JVM-level threads ([user-level threads](https://www.youtube.com/watch?v=KXuZi9aeGTw) N:M mapping of fibers to threads)
- This is not "Green threads" from Java 1.0/1.1.
- type `Fiber` (probably distinct types)
- Fibers have simpler API, work well with `CompletableFuture`.
- Uses `ForkJoinPool` scheduler.
- Much lighter-weight: less memory (low footprint), close-to-zero overhead for task switching.
- Tail-call elimination. (functional-language-hippies) - but not started on this topic yet.
- More predictability, stability of performance, lower latency due to Java keeping in control and scheduling to OS Threads.
- Used as a mechanism to "reinvent" concurrent execution. (Think "Threads 2.0")
- System calls park fiber, instead of blocking all execution. (Sockets, pipes, I/O, ...)
- Due to blocking semantics, can write code that runs infinitely and blocks until accessed.
- Explicit locking mechanisms may prevent or force parking of fiber.
- In-progress fiber executions may be pinned to particular thread.
- Exploring ThreadLocal alternatives, such as ProcessorLocals and locals for individual frames/fibers.
- Footprint:
  - _Thread_: 1MB reserved + 16KB kernel data structures.
  - _Fiber_: hundreds of bytes up to KBs, ~240 bytes per fiber. (current prototype)

Enables ability to design _concurrent systems_ where you can reasonably make the assumption that the "many threads of execution" provide insignificant overhead. Now we need the developer to stop thinking sequentially!

Allow writing simple, blocking, synchronous code and let JVM handle the concurrency/scheduling.

# Panama

Foreign functions and foreign data. (FFI)

__Vision__: "If non-Java programmers find some library useful and easy to access, it should be similarly accessible to Java programmers."

Not "dirty" anymore when using native libraries. (Necessity for some performance cases - e.g. AI, interoperability, etc.)

Models data types, extraction tool to transform C/C++ header files to Java interfaces, binding method-mechanism at run-time (no need to carry around .so file).

`pid_t getpid();` --> `public abstract int getpid()`

- Operate on native data.
- Pass native structures in
- Get structures back
  - `timespec clock_gettime()`
  - Generates struct_timespec class.
- Supports defining scope for C memory model.
- Acquire pointer to created struct.
- Generated implementation (binding) can be trusted by runtime, hence performance benefits.
- Alternative implementation, not based on _native_ interfaces.
- Plans to Panama-ize the JDK.

In progress, biggest complications are the complicated constructions in C/C++ header files.

# Metropolis

- "JVM in Java" - part of a movement towards more java-code in the JVM.
- JDK 10: Garbage Collector interface ([JEP304](https://openjdk.java.net/jeps/304]))
  - [Shenandoah (JEP189)](https://openjdk.java.net/jeps/189),
  - [Epsilon (JEP318)](https://openjdk.java.net/jeps/318),
  - [ZGC (JEP333)](https://openjdk.java.net/jeps/333)
- GraalVM: JIT written in Java
  - As JIT
  - For static compilation (AOT)
  - Cross-platform (SubstrateVM)

Goals:
- Development speed.
- Flexibility
- Simplification

Future benefits:
- better native-to-Java bindings
- replacement for Client JIT (C1)
- coding native methods in statically-compiled Java
- easier to port to different platforms

Based on Graal Compiler Project.

# Valhalla

- Cannot mix primites and objects. (Needs boxing etc.)
- Generic specialization
- Value-types "Codes like a class, works like a primitive"
  - methods + fields
  - implement interfaces
  - encapsulation
  - generic
  - _Immutable_ (i.e. fixed value, immutable enforced by JVM)
  - no instance fields allowed (no longer transparent holder for data)
  - final, i.e. not subclassable
  - No "identity"
- Consequence: more control over memory use.
- Very invasive feature!
- Performance - speed: less memory dereferencing implies significant speed improvements. (Over the years, CPUs have gotten significantly faster, but randomized memory accesses haven't. Memory _is_ bottleneck.)
- Gets rid of unnecessary encapsulation: no need to write methods if fields are publicly accessible. (equals, hashCode, toString generated for free, can be overridden)
- Deconstruction of record types for immediately accessible fields.

Very deep cuts in language 

Huge benefit for data science applications.
In demonstration on matrix multiplication:
- +/- 13x speed-up
- barely 1 inst. per cycle -->  close to 3
- barely needs any allocation (1000+ times less)
- less actual CPU instructions to execute

```Java
record Point(double x, double y);
```

```java
record Point(double x, double y) {

  String toString() {
    ...
  }
}
```

Supports deconstruction of record types:
```java
String print(Object o) {
  return switch(o) {
    case Point(x, y) -> String.format("point(%f, %f)", x, y);
    default -> "unknown";
  };
}
```

# References

- [OpenJDK: Valhalla](https://openjdk.java.net/projects/valhalla/)
- [OpenJDK: Metropolis](https://openjdk.java.net/projects/metropolis/)
- [OpenJDK: Amber](https://openjdk.java.net/projects/amber/)
- [OpenJDK: Loom](https://openjdk.java.net/projects/loom/)
- [OpenJDK: Panama](https://openjdk.java.net/projects/panama/)
- [Java, Today and Tomorrow by Mark Reinhold](https://www.youtube.com/watch?v=Csc2JRs6470)
- [Project Loom: Fibers and Continuations for Java by Alan Bateman](https://www.youtube.com/watch?v=vbGbXUjlRyQ)
- [Panama: a foreign policy for java by Maurizio Cimadamore](https://www.youtube.com/watch?v=cfxBrYud9KM)
- [Java Futures, Devoxx 2018 edition by Brian Goetz](https://www.youtube.com/watch?v=4r2Wg-TY7gU) - Amber and Valhalla
- [JDK 9, 10, 11 and Beyond: Delivering New Feature in the JDK](https://www.youtube.com/watch?v=mFyzyVnYcoY)
- [User-level threads ... with threads - Paul Turner - Google](https://www.youtube.com/watch?v=KXuZi9aeGTw)

For future investigation:

- https://www.youtube.com/watch?v=mFyzyVnYcoY
- https://www.youtube.com/watch?v=CMMzG8I23lY
- https://www.youtube.com/watch?v=bhSBpNXwa60
- https://www.youtube.com/watch?v=OFwjfUyJqUA
- https://www.youtube.com/watch?v=l26JKbijLqQ
