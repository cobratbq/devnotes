# TODO

- Makefile-like mechanism with broader, more shallow purpose:
  - w/o implicit rules, or configurable single default ruleset.
  - expiry check based on choice: timestamp vs sources, checksum (sha256, etc.), ...
  - actions:
    - build tree
    - detect necessary actions
    - execute necessary actions
- Need to modify otr4j code-base?
  - Cleanroom re-implementation of Joldilocks / replacement with Bouncy Castle.
- Investigate how (remote) attestation mechanisms work.
- Investigate how Fedora package archive works, risks:
  - signing prevents modification
  - expiration date for metadata?
  - ...
- Applying "[Theory of Regions](https://pdfs.semanticscholar.org/d78e/1242c4dedf907324216cdfcf6c1ec37821ae.pdf)" to code analysis to discover the minimal set if bidirectionally-dependent variables (data), such that this subset can be consolidated in a class/data structure of its own. (Basis for determining what should and should _not_ be in a class to ensure separation of concerns, high cohesion, low coupling.)
- Article "On Logging":
  - `TODO: the meaning of INFO, WARNING, ERROR/SEVERE is in context of application execution. Do not log ERROR for bad user input. It may be critical for the user, but the application will happily continue running.`
- The idea/notion of building syntactically-correct/-sound structure such that we can reduce the number of tests to be written.  
  This also means that tests can not be easily constructed for significant parts of the application. By testing the guards, we are allowed to assume that illegal cases in these parts cannot happen.
- Reevaluate use of [Checker Framework](https://checkerframework.org/) for source code verification.
- [Animal Sniffer maven plug-in](https://www.mojohaus.org/animal-sniffer/animal-sniffer-maven-plugin/)  
_Allows recording and verifying the public API signature._  
```
<plugin>
  <groupId>org.codehaus.mojo</groupId>
  <artifactId>animal-sniffer-maven-plugin</artifactId>
  <version>1.16</version>
  <executions>
    <execution>
      <phase>test</phase>
      <goals>
        <goal>check</goal>
      </goals>
      <configuration>
        <signature>
          <groupId>org.codehaus.mojo.signature</groupId>
          <artifactId>java16</artifactId>
          <version>1.1</version>
        </signature>
      </configuration>
    </execution>
  </executions>
</plugin>
```

# SQL videos

<https://www.youtube.com/watch?v=yuuhkHORzfM>
<https://www.youtube.com/watch?v=wTPGW1PNy_Y>

