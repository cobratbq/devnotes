# Builds and continuous integration

Tricks and strategies to improve quality through smart builds and compile-time checking. This relieves developers from having to chase down these minor coding issues and facilitates continuous integration.

## Functional goals

- The process should benefit the developer, and it should interfere with quality degradation.  
  _The process should be set up in such a way that developers can circumvent detections if needed for the time functionality is under development. The process should ensure that the final delivery is of quality. Up until the time of the delivery, the developer should be in charge._
- Don't expect developers to take into account everything from memory.  
  _Let's off-load as much as possible from developer memory, such that they can free up mind-share for what actually matters: the solution for the current engineering problem._
- Set up a process that signals developers.  
  _A signaling system (push-nature) ensures that information reaches the developers. Instead of a pull-system where you expect the developer to search for potential problems every once-in-a-while._
- Set up a process that stimulates developers to think about all various aspects of engineering.  
  _Forcing Javadoc is an infamous practice for inviting developers to auto-generate documentation. Simply allowing developers to not write any Javadoc, however, does not fix the problem. Documenting functionality triggers developers to think about it from the functional perspective: completeness is important. Similarly, it is important that developers think about which types and methods are part of the API. Prefer forcing Javadoc on public API (consider also 'protected'). Trigger developers to think about minimize the public API, such that there are fewer risks of breaking and over-commitment, of restricting to minimum moving parts, therefore less to types and methods to document. (Underlying problem: if API is big, too much is required to be documented.)_

## Options to control code quality

  1. Guard against star-imports  
     _Star-imports risk including name collisions in later iterations of dependencies. If they introduce more classes, these newly introduced classes will be automatically imported. This may result in conflicts simply by upgrading a dependency._
  1. Fail build on compile-time warnings  
     _Compile-time warnings always indicate areas where improvements are possible. For example, classes expecting a parametric type that is missing its parametric type. There should be no argument on fact that you code shouldn't be half-baked. There is no excuse to adopt an old Java 1.4-style of development (i.e. "Type"), just because you don't feel like adding the parametric type wildcard ("Type<?>"). Almost all of the time the reason is simply that developers aren't aware this mistake exists in the code._
  1. Fail build on below-minimal-threshold javadoc  
     _Ensure minimum level of javadoc. Consider enforcing only on classes that are part of your public API. Stricter is better if goal remains reasonable, but benefit is largest for public API as it is used by users *other* than the package/module developers._
  1. Fail build on badly declared dependencies (not declared / declared but not used)  
     _Dependencies that are used while not declared, i.e. accessed transitively, and dependencies that are declared but not actually used will be detected._
  1. Fail build on static analysis results/warnings  
     _Use static analysis to discover potential bugs. Fail the build such that analysis results are not ignored._
  1. Fail build on new code that does not follow the code style and conventions  
     _Instead of having to inspect and detect manually, why not let the software do it for you?_
  1. Favor explicit suppression of warnings  
     _To make a code-base manageable, one needs to be able to evaluate dubious situations. It is better to silence individual false positives, in favor of spotting more potential bugs._  
     _NOTE: This does assume that developers can reasonably be trusted to make the right call for suppression, or alternative are sufficiently mature to consult with colleagues if they are not sure._
  1. Fail build when failing to adhere to design structure.  
     _Enforce single-direction imports. By failing a build upon violating import rules, we avoid causing unintentional entanglement of packages through messy imports._

## Strategies for gradually increasing quality-threshold

Strategies to increase code quality and control over the software. Especially given a growing community of developers (and especially within companies) it is important that significant rules are enforced. Not so much to protect from "bad actors" but rather to relieve developers from the ever-increasing amount of rules. An environment should be created that allows the developers to focus on their own work, with an infrastructure that helps developers by pointing out obvious issues.

### Gradually increase strictness/build-time support

One of the biggest annoyances is very restrictive behavior when it is not welcome yet. For instance, you have just started a project and you're still experimenting with the package structure.

- Static checking should become more powerful with added information. (Like Python adding type hints, Java adding annotations for extending the scope of static analysis.)
- Obvious rules regarding application structure (e.g. layered design) could be enforced by plug-ins. Developers can add structure rules whenever the new rule emerges.

### Introduce build-time checking that fails builds

1. Build-time code formatting, such as:
   - [maven-formatter-plugin](https://code.revelc.net/formatter-maven-plugin/) which uses the Eclipse formatter.
1. Install build-time checking, such as:  
  _NOTE: that this assumes that developers are able to build without these checks being active. For example, using the built-in compiler in the IDE. If not, this could be a heavy burden on development speed._
  - maven-compiler-plugin
    - Fail on warnings: `-Werror`
    - Raise warnings threshold: `-Xlint:all` (or demonstrated by maven-compiler-plugin: `-Xlint:all,-options,-path`)
    - Raise warnings for javadoc errors: `-Xdoclint:...`
  - maven-dependency-analyzer
    - Use `resolve` and `resolve-plugins` to resolve effective version and fix in `pom.xml` to make build explicit.
    - Use `analyze` to inspect build configuration:
      - Ensure that all required dependencies are declared.
      - Optionally, ensure that unused dependencies are removed.
  - pmd (maven-pmd-plugin)
  - SpotBugs/FindBugs
  - checkstyle (as alternative to automated formatting with maven plug-in)
    - Guard against bad styles of importing, such as star-imports.
    - Guard against [imports breaking the architectural layering rules](http://checkstyle.sourceforge.net/config_imports.html#ImportControl).
  - Animal Sniffer (maven plug-in)
    - Guard against public API changing (unintentionally) in a backwards-incompatible way.  
    _NOTE: Mostly relevant for libraries and APIs._
  - _ErrorProne_ for further static analysis capabilities.
    - Including _NullAway_ (combined with JSR-305 or alternative nullability annotations) in order to build with non-null by default.
1. Configure build-time checking with exceptions for rules that are (heavily) violated such that the current build succeeds.
  - Immediate benefit: sets quality threshold to current state, guards quality from lowering.
  - Eases adoption: insignificant effort requires to put in place. Exceptions to rules can be removed as quality improves. Improvements can be planned independently without blocking initial adoption.
1. Identify which rules you would like to have active. (Some rules might enforce a coding style that is not desirable.)
1. Plan follow-up actions to remove exceptions for identified rules one-by-one.
  - Fix all violations of the rule.
  - Remove exception from the build configuration.
1. Repeat for other identified rules.
