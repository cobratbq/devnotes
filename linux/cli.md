# Linux command-line interface

Basic command-line tools: `tmux htop rsync curl zstd git`

## Tools

## `restic`

Backups, local and networked, to encrypted storage repository.

## OpenSSL

- Generate:
  - self-signed certificate: `openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:4096 -keyout private.key -out certificate.crt`
  - private key: `openssl genrsa -out some-CA.key 2048`  
  _See `man  genrsa` or `man 1SSL genrsa`._
  - x509 CA-qualified certificate: `openssl req -x509 -new -nodes -key ca.key -subj '/CN=Some CA/C=US/L=New York' -sha256 -days 1825 -out ca.crt`  
  _See `man 1SSL req` for more info._
  - CSR: `openssl req -new -subj '/C=GB/CN=foo' -addext 'subjectAltName = DNS:foo.co.uk' -addext 'certificatePolicies = 1.2.3.4' -days 1000 -newkey rsa:2048 -keyout key.pem -out req.pem`  
  _See `man 1SSL req`._
  - CSR and private key: `openssl req -new -newkey rsa:2048 -nodes -out request.csr -keyout private.key`
- Verify:
  - CSR: `openssl req -in request.csr -text -noout -verify`
- Display:
  - certificate: `openssl x509 -in ca.pem -text -noout`
  - existing CRL: `openssl crl -in crl.pem -text`
- Modifications/updates:
  - Change password on private key: `openssl rsa -des3 -in server.key -out server.key.new`
  - Remove password from private key: `openssl rsa -in server.key -out server.key.new`
  - Convert to PKCS12: `openssl pkcs12 -export -out certificate.pfx -inkey privateKey.key -in certificate.crt`
    - use `-certfile more.crt` to include more certs, e.g. CA certificate.
- `TODO: ... generate/sign certificate, generate client certs, ...`

References:

- https://sockettools.com/kb/creating-certificate-using-openssl/

## SSH

- SSH signing/verification.
- Show info on public keys: `ssh-keygen -l -f /etc/ssh/ssh_host_ecdsa_key.pub`
- Generate public key from private key: `ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub`
- Drop lines with weak moduli (LogJam): `awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.tmp && mv /etc/ssh/moduli.tmp /etc/ssh/moduli`

References:

- <https://www.agwa.name/blog/post/ssh_signatures>
- <https://goteleport.com/blog/security-hardening-ssh-bastion-best-practices/>

## `sshuttle`

- `sshuttle -vr user@sshserver:22 0/0`  
  Set up connection to `sshserver:22` (port optional if default) and reroute everything (`0/0` or `0.0.0.0/0`) through this (proxy) server.

## Shell commands

- Use `dash` as secure, reliable, straight-forward `sh` shell implementation.
- `type -p $command` to query the full path of _command_. Better alternative to `which`.  
  _Be careful because in some shells this command is very limited, i.e. missing program arguments._
- Generate random strings with restricted characterset and specific length: `tr -dc "[:graph:]" < /dev/urandom | read -n 32`

