# PostmarketOS

Notes regarding the use of [PostmarketOS](https://postmarketos.org/) and image generation.

## Preparing installation image

- [PostmarketOS wiki: Installation Guide](https://wiki.postmarketos.org/wiki/Installation_guide)

First install `pmbootstrap`: `pip3 install --user -U pmbootstrap`

Now prepare a file system for use on the smartphone/SD-card.

1. (Initial) preparation:
    - Initial: `pmbootstrap init`
    - Update: `pmbootstrap pull`, `pmbootstrap update`.
1. Updating content:
    - `pmbootstrap pull`, `pmbootstrap update`
1. Query current state: `pmbootstrap status`.
1. Build and install to SD-card: `pmbootstrap install --fde --sdcard /dev/mmcblk --add git,neovim,powersupply,nano,dnscrypt-proxy,zram-init`
    - `--fde` indicates use of full-disk encryption (cryptsetup)
    - `--sdcard <device>` indicates which block-device to use for destination.
    - `--add <packages-comma-separated>` include additional packages as following in comma-separated list.
    - NOTE: dnscrypt-proxy openrc-init-script has issue that it uses `dnscrypt` user causing dnscrypt-proxy to fail to open port 53 (privileged).
1. After installation:
    - Update `/etc/conf.d/zram-init`: change `numdevices=1` (original: `2`)

## Common tasks

- `rc-service <servicename> <operation>` to `start`, `stop`, query `status`, etc.
- `rc-update <operation> <servicename>` to `enable`, `disable` at various run-levels.

## Set-up notes

Rough out-line of set-up notes for the PinePhone. This is by no means critical and rather for experimentation.

1. Check open ports: (`sudo netstat -ltupn`)
    1. Disable `sshd`
1. Run `sudo flatpak update` (even without remote present) once to ensure necessary directories get created.
1. Configure appearance, theme, network time, timezones, etc.
1. Configure network with static DNS servers, e.g. QUAD9.
1. Configure _Network Manager_ to ignore DNS settings and configure DNS servers manually: `/etc/resolv.conf`.

## Troubleshooting

### Build custom kernel

1. Update kernel config: `pmbootstrap kconfig edit linux-postmarketos-allwinner` (enable required option `CONFIG_CRYPTO_ZSTD`, press '/' to search)
1. Update package version to ensure updated kernel parameters are preferred: `pmbootstrap pkgrel_bump linux-postmarketos-allwinner` (make sure package version is increased)
1. `pmbootstrap build --force --strict --arch aarch64 linux-postmarketos-allwinner` (build new kernel package)  
    _Note: during build-phases `sudo` might be required, so a prompt may show and will time-out after a period. Issuing the build command again, may continue where it left off._
1. `pmbootstrap install --sdcard <device>` (proceed to install as usual, flash and test)
