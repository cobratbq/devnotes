# Linux system

Notes on the linux system that are independent of graphical desktop or command-line interface.

## Outline

- Install:
  - LUKS for confidentiality
  - LVM+XFS for solid file system on dynamically managed platform. (size, location, redundancy)

## Summary

- kernel parameters: `nohpet mitigations=auto lockdown=integrity debugfs=off init_on_alloc=1 init_on_free=1 randomize_kstack_offset=on slab_nomerge pti=on nosmt iommu.passthrough=0 iommu.strict=1 vsyscall=none` (ref: [Kernel Self-Protection Project](<https://kernsec.org/wiki/index.php/Kernel_Self_Protection_Project/Recommended_Settings#kernel_command_line_options>))
- Packages:
  - Install: `tmux htop fish gnupg pcsclite p7zip zstd zswap`
  - `dnscrypt-proxy` for advanced DNS services
  - `ssh` client + server
    - ensure only _public key authn_,
    - restrict logins to explicit user list `AllowUsers user` or `AllowGroups group`)
    - restrict moduli to subset `>= 3072`
  - `syncthing` (file synchronization)
  - Remove: `tlp`  
    _NOTE_ may not be necessary, but test this out. On debian, installing only `tlp`, i.e. without recommendations, there is no issue.
- (confirm?) Use `systemd-cryptsetup >= 248` for enrolling FIDO2 tokens.
- Set up reliable, (relatively) secure DNS:  
  __NOTE__: `systemd-resolved` is unreliable. It does not validate its servers.
  - Install `dnscrypt-proxy` (require DNSSEC, specify DNSCrypt or -- if `dnscryptproxy >= 2.1.1` -- DoH servers)
    - Check which (caching) DNS resolver is active on the system, e.g. `systemd-resolved`.
    - Ensure DNS resolver uses `127.0.0.1` (local `dnscrypt-proxy`), e.g. `resolvectl status`.
    - Test if DNSSEC functions correctly. E.g. `dig +dnssec dnssec-failed.org` and check `SERVFAIL` response.
  - OR: install `unbound` (for secure DNS-over-TLS connections and DNSSEC validation)
- Install `iwd` (ensure Management Frame Protection enabled, MAC address randomization) or keep using `wpa_supplicant`.
  - NOTE: in case of issues with dropping connections, set up WPA2-Enterprise (or later) as that has multiple stages such that it is harder to disrupt completely.
- Kernel entropy daemons:
  - Check amount of entropy: `cat /proc/sys/kernel/random/entropy_avail`
  - Test need for entropy daemon: `time timeout 30 pv /dev/random > /dev/null`
  - `kernel` < `5.6`: support entropy using `haveged` or another entropy daemon.
  - `kernel` >= `5.10`: changes in kernel obviate need for entropy daemons. (`haveged` can be useful for user-space uses.)
- TPM2-tools:
  - `sudo apt install libtpm2-pkcs11-tools libtpm2-pkcs11-1`
  - `sudo usermod -a -G tss "$(id -nu)"`  
    NOTE: check if `/dev/tpmrm*` exists and is `tss` group accessible.
  - Each `init`-call generates a new primary (keyslot).
    ```
    tpm2_ptool init
    tpm2_ptool addtoken --pid=1 --label=ssh --userpin=MySecretPassword --sopin=MyRecoveryPassword
    tpm2_ptool addkey --label=ssh --userpin=MySecretPassword --algorithm=ecc256
    ```
  - `ssh-keygen -D /usr/local/lib/pkcs11/libtpm2_pkcs11.so`
  - `.ssh/config`:  
    ```
    Host myserver.example
        PKCS11Provider /usr/local/lib/pkcs11/libtpm2_pkcs11.so
    ```
  - Update or add locales:
    - `sudo dpkg-reconfigure locales` or `sudo locale-gen en_US.UTF-8 en_NL.UTF-8 ...` or edit `/etc/locale.gen` then `sudo locale-gen`
    - consider setting environment `LC_ALL=en_US.UTF-8`
  - References:
    - <https://www.evolware.org/?p=597>
    - <https://donjon.ledger.com/ssh-with-tpm/>

## Configuration

- Check (server) security:
  - `/etc/ssh/ssh_host_*_key`, probably needs reconfiguring package.
  - Ensure entropy daemon is running, e.g. `haveged`.
  - Check which `sh` shell is installed. `dash` is suitable for non-fancy to-the-letter posix shell implementation known to be less vulnerable.
- Disable/remove swap partition/file. Enable zram-based swap.  
  _Extra performance. If relied on swap space, consider adding zram-based swap with higher priority._
- Unattended (security) updates.
- udev rules:
  - Block queue scheduler (`bfq`)
  - CPU scheduler (`schedutil`)  
    NOTE do not start with `intel_pstate=passive` for it causes badly managed platform thermal throttling issues that case some suspend actions to result in excessive CPU use and excessive heating in suspend/standby.
  - TCP congestion protocol (`bbr`)
- Install usbguard and configure. (Check to make sure some rules exist before usbguard is activated.)

## Grub 2

- Having loaded `grubx64.efi`, have minimal config to redirect to and load `grub.cfg` that is located elsewhere:  
  ```
  search --no-floppy --fs-uuid --set=dev abababab-bcde-aaaa-aabb-015651b00001
  set prefix=($dev)/grub2

  export $prefix
  configfile $prefix/grub.cfg
  ```

## http- vs https-updates

- (http) faster/more efficient
- (http) no dependence on correct time
- (http) security: authenticity through signed update packages and index files
- (http) not much leaking of package data as transfer size itself is already a good indicator of which packages are downloaded
- (https) confidential transport hides (attempts to) actual packages downloaded
- (https) security: additionally, "~guaranteed" true server because of certificates (however reliable server not critical because of signed packaging data)

## LUKS full-disk encryption / Yubikey

- Use `cryptsetup` for configuration of encrypted (LUKS) partition.
- Yubikey: add challenge-response config to slot 2:  
  `ykpersonalize -v -2 -ochal-resp -ochal-hmac -ohmac-lt64 -oserial-api-visible -ochal-btn-trig`
  - SHA1-HMAC
  - serial number accessible
  - challenge requires button press
- cryptsetup
  - Make sure LUKS contains a (working) normal passphrase to use as fallback at the very least during set-up.
  - [`yubikey-full-disk-encryption` on Arch/Manjaro](https://awesomeopensource.com/project/agherzan/yubikey-full-disk-encryption?ref=...#install):
    1. Install `ykfde`.
    1. Use `sudo ykfde-enroll -d /dev/<LUKS-partition> -s <LUKS-keyslot>` to add yubikey challenge-response configuration to LUKS.
    1. Configure `/etc/ykfde.conf`:
       - `YKFDE_CHALLENGE=""` for automatic mode (where you set the challenge in the configuration file),
       - `YKFDE_CHALLENGE_PASSWORD_NEEDED="1"` for manual challenge entry,
       - `YKFDE_CHALLENGE_SLOT="2"` to set expected Yubikey slot.
    1. Edit `/etc/mkinitcpio.conf`: include hook `ykfde` before `encrypt`.
    1. Update initramfs: `sudo mkinitcpio -P`.
    1. Reboot and try out if the challenge-response set-up works. If not, use normal password to decrypt LUKS partition.
  - [`yubikey-luks` on Debian/Ubuntu](https://github.com/cornelinux/yubikey-luks):
    1. Install `yubikey-luks`.
    1. Use `yubikey-luks-enroll` to add yubikey challenge-response configuration to LUKS keyslot.
    1. Add `keyscript=/usr/share/yubikey-luks/ykluks-keyscript` to LUKS partition entries in `/etc/crypttab`.
    1. Run `sudo update-initramfs -u -k all`.
    1. `TODO: ...`

## USBGuard

USBGuard is a daemon that monitors and controls which USB devices are allowed to interact with the system according to a preconfigured policy.

### Getting started

1. `sudo dnf install usbguard`
1. `sudo usbguard generate-policy > rules.conf`  
   Produce your first rules set, generated from the devices currently attached.
1. `sudo cp rules.conf /etc/usbguard/rules.conf` (in case of selinux, maybe need to restore labels: `restorecon -Rv /etc/usbguard`)
1. `sudo systemctl start usbguard`  
   NOTE: immediately upon starting usbguard, all devices not in the policy will be blocked, so ensure that an existing policy (rules) file is in place.
1. If everything works according to expectations and all critical devices, such as keyboard, are operational.  
   `sudo systemctl enable usbguard`

### Actions

- List current policy: `sudo usbguard list-rules`
- List currently attached, detected USB devices: `sudo usbguard list-devices`
- Allow a device according to a default rule construction: `sudo usbguard allow-device <number>` where number refers to the device number in the _list-devices_ output.

## systemd

- Use masking (`systemctl mask <servicename>`) to prevent a service from being started, if the service cannot be uninstalled.

## journald

- `journalctl --disk-usage`
- `journalctl --rotate` to rotate log files.
- `journalctl --vacuum-time=14d` for cleaning up logs older than 14 days.
- `journalctl --vacuum-size=100M` for cleaning up logs until collective log size is reduced to 100M.
- `journalctl --vacuum-files=5` for cleaning up a number of log files.

## systemd-resolved

_Do not use. Insecure._

## systemd-analyze

- restrict systemd capabilities
- additional information:
  - manpages for `systemd.exec`, `systemd.resource-control`, `systemd.unit`, etc.
  - `/proc/devices` for device classes

## DNSCrypt-proxy

- Configuration: require DNSSEC, DoH, DNS servers: Quad9, libredns
- ...

## DNS: Quad9

1. Classic DNS: `9.9.9.9 149.112.112.112 2620:fe::fe 2620:fe::9`
1. DNS-over-TLS (DoT): `dns.quad9.net`
1. DNS-over-HTTPS (DoH): `https://dns.quad9.net/dns-query`

## DNS: libredns.gr

1. Classic DNS: `116.202.176.26`
1. DNS-over-TLS (DoT): `dot.libredns.gr`
1. DNS-over-HTTPS (DoH): `https://doh.libredns.gr/dns-query`

## DNS resolver: dnsmasq through NetworkManager

1. Configure _dnsmasq_:  
   Resolve using DNSSEC, predefined trust roots, check absence of authority signatures, use resolvers as configured instead of checking `/etc/resolv.conf`.
   1. Resolve using DNSSEC, predefined trust roots, check absence of authority signatures for domain.  
      ```
      conf-file=/usr/share/dnsmasq/trust-anchors.conf
      dnssec
      dnssec-check-unsigned
      ```  
   1. Configure DNS servers for relaying, do not check for DNS-servers in `/etc/resolv.conf`, always try first DNS-server first.  
      ```
      strict-order
      no-resolv
      server=1.1.1.1
      server=8.8.8.8
      ```
   1. (Optionally) configure hosts/domains blocklists:  
      ```
      conf-file=/path/to/domains.txt
      addn-hosts=/path/to/hostnames.txt
      ```
   There is no need to enable or start dnsmasq. It is managed through NetworkManager.
1. Enable _NetworkManager_ to use dnsmasq.
   1. Edit `/etc/NetworkManager/NetworkManager.conf`:  
      ```
      [main]
      dns=dnsmasq
      ```
    1. Restart NetworkManager: `sudo systemctl restart NetworkManager`.
1. Ensure that `/etc/resolv.conf` lists nameserver `127.0.0.1`.
1. Test recursive resolving using DNSSEC: `dig +trace ds www.ietf.org.`  
   (Needs package `bind-tools` on Arch/Manjaro.)

## DNS resolver: dnsmasq (Previous)

1. Enable on start-up: `sudo systemctl enable --now dnsmasq`
1. Enable in NetworkManager: set `dns=dnsmasq` in `/etc/NetworkManager/NetworkManager.conf`
1. Configure dnsmasq `/etc/dnsmasq.conf`:
   1. Enable `dnssec` and `conf-file=<path-to-trust-anchors-configuration>`
   1. Enable `dnssec-check-unsigned`
   1. Redirect typical resolve-requests to dnsmasq:
      1. Add configuration line: `server=1.1.1.1`
      1. Add configuration line: `server=8.8.8.8`
      1. Edit `/etc/resolv.conf`:  
         ```
         # Manual configuration: direct to dnsmasq
         nameserver 127.0.0.1
         nameserver [::1]
         ```
   1. Add [notracking-blocklists](https://github.com/notracking/hosts-blocklists):
      1. Download [domains.txt blocklist](https://raw.githubusercontent.com/notracking/hosts-blocklists/master/domains.txt).
      1. Download [hostnames.txt blocklist](https://raw.githubusercontent.com/notracking/hosts-blocklists/master/hostnames.txt).
      1. Add configuration lines:  
         ```
         conf-file=/path/to/domains.txt
         addn-hosts=/path/to/hostnames.txt
         ```

### Firewalld (`firewall-cmd`)

_nftables_-based firewall

Raw dump of active rules: `nft list ruleset`.

- Check state of firewall:
  - `firewall-cmd --state` indicates `running` if active.
  - `firewall-cmd --list-all` shows active profiles, rules.
- Usage notes:
  - `--permanent` to modify permanent instead of run-time configuration.
  - `firewall-cmd --reload` to reload with persistent configuration.
- Allow
  - port, port range, port-for-protocol: `firewall-cmd --permanent --add-port=<port-or-range-opt-protocol>` (also `--remove-port`)
  - protocol: `firewall-cmd --permanent --add-protocol=<protocol>` (also `--remove-protocol`)
  - service: `firewall-cmd --permanent --add-service=<servicename>` (also `--remove-service`)
- Query information:
  - `firewall-cmd --get-default-zone` shows the default zone for connections.
  - `firewall-cmd --info-zone=<zone-name>` to query zone details.
  - `firewall-cmd --info-service=<service-name>` to query details for service definition.

### Flatpak

- Clean up unused objects: `flatpak uninstall --unused`  
  Removes objects not used by any flatpak package. Cleans up unnecessary files to regain disk space.

### Init-system: systemd

The systemd init system (and operating system, and games loader, and ...)

- Status: `systemctl status` (or `systemctl status <servicename>`)
- Enable service: `systemctl enable <servicename>`  
  (add `--now` to also start service)
- Disable service: `systemctl disable <servicename>`  
  (add `--now` to also stop service)
- Start service: `systemctl start <servicename>`
- Stop service: `systemctl stop <servicename>`

### Init-system: OpenRC

The OpenRC init-system.

- Status: `rc-status --list` (or `rc-service <servicename> status`)
- Enable service: `rc-update add <servicename>` (or `rc-update add <servicename> default`)
- Disable service: `rc-update del <servicename>`
- Start service: `rc-service <servicename> start`
- Stop service: `rc-service <servicename> stop`

### Power management

- `upower -d` to dump information on power supply/battery.
- `acpi -V` to dump ACPI-sourced information, among others battery and thermal.

### Wireguard

1. Generate a key pair:
  1. Set umask to ensure private key is not readable by other users: `umask 077`
  1. `wg genkey | tee cobra.key | wg pubkey > cobra.pub`
1. Create config file for wireguard:  
  - Either generate from configured wireguard interface: `sudo wg showconf wg0 > wg0.conf`, or
  - create yourself:  
    ```
    [Interface]
    Address = 192.168.1.2
    DNS = 192.168.1.1
    ListenPort = 51820
    PrivateKey = <your-generated-private-key-here>
    
    [Peer]
    PublicKey = <your-peers-public-key-here>
    AllowedIPs = 0.0.0.0/0,::/0
    Endpoint = 192.168.0.3:51820
    ```  
    _Note: `AllowedIPs = 0.0.0.0/0` reroutes everything through the wireguard interface. Restrict to single IP if you just want to reach the one computer._  
    _Note: `Endpoint` for section `[Peer]` is optional. If not provided, it can still function as a server and be reached on the initiative of the peer._
1. Start wireguard using `wg-quick` and created config file: `sudo wg-quick up ./wg0.conf`
1. Enable IP forwarding for IPv4: (similar mechanism exists for IPv6)
  - One-time: `sysctl net.ipv4.ip_forward=1`
  - At boot: modify `/etc/sysctl.conf`: add line `net.ipv4.ip_forward = 1` (to forward traffic for all interfaces)
  - iptables rules to be executed when wireguard interface goes up: (add to the wireguard config)  
    ```
    [Interface]
    PostUp = iptables -I FORWARD -i %i -j ACCEPT; iptables -I FORWARD -o %i -j ACCEPT
    PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT
    ```
  - iptables rules to enable masquerading: (add to the wireguard config)  
    ```
    PostUp = ...; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
    PostDown = ...; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
    ```
  - _Note: make sure that the appropriate routing (static routes) are configured if you don't use masquerading, as otherwise return packets won't know where to go to get back to the source._
1. Configure created wireguard config to start now and on boot: `sudo systemctl enable --now wg-quick@wg0` (This will look for `/etc/wireguard/wg0.conf`.)
1. Open an port in your firewall and/or configure port-forwarding to the server of port `51820`, protocol `UDP`.
1. Configure _Unbound_ DNS server for requests from wireguard interface:
  - Configure wireguard to listen and reply to interface `192.168.1.1`: (add to the wireguard config)  
    ```
    access-control: 127.0.0.0/8 allow
    access-control: 192.168.1.0/24 allow
    interface: 127.0.0.1
    interface: 192.168.1.1
    ```
  - Open up the firewall for DNS requests from the wireguard interface:  
    ```
    PostUp = ...; iptables -I INPUT -i %i -p udp -m udp --dport 53 -j ACCEPT; iptables -I OUTPUT -o %i -p udp -m udp --sport 53 -j ACCEPT
    PostDown = ...; iptables -D INPUT -i %i -p udp -m udp --dport 53 -j ACCEPT; iptables -D OUTPUT -o %i -p udp -m udp --sport 53 -j ACCEPT
    ```

## Notes

- Full file-system backup using `rsync` (as root):  
  `rsync -aAXv / --exclude={"/dev/","/proc/","/sys/","/tmp/","/run/","/mnt/","/media/","/lost+found"} /media/storage/destination`  
  _Avoid infinite loop by excluding destination location!_

# Desktop

## Suitability

- up-to-date drivers/firmware
- up-to-date packages
- support (but not necessarily out-of-the-box) for non-free firmware
- clean minimal install base (no desktop apps bloat)
- basic command-line tools available, then install your preferred tools. E.g. base: nano, install: (neo)vim/emacs.

Candidates:

- Debian testing
- Fedora Workstation/Silverblue
- Ubuntu (LTS/desktop?)  
  ... love-hate relationship with this one. There's always something. Maybe because of high expectations in general?
- OpenSUSE Tumbleweed (? no practical experience)

Notes:

- identify apps that "should just work" and apps that "must be up to date".
- don't rely too much on important apps being in the linux distro. (Install yourself.) For example:
  - Install JRE (only) from distro get have a run-time for Java applications. Install JDKs yourself for development.

## General

- Use _DejaVu_ fonts for a smooth non-anti-aliased experience. (Lean towards using larger DPI value and font size ~9.)
- Dark background color: `#212121` (or `#161616` for less contrast/more light distortion), which is slightly off black for more comfortable viewing.
- Disabling _key repeat_ support for games to ensure key eventing does not interfere with responsiveness/latency.

## Gnome Configuration

### Settings

- Settings/Devices/Keyboard:
  - `Super+Up`: Toggle maximization state
  - `Super+Down`: Hide window
  - Unbind `CTRL`+`ALT`+`Left` and `CTRL`+`ALT`+`Right`. (Typically for moving/switching workspace. Often used in IDEs for navigation.)
- (OBSOLETE) Custom keymapping modifications:  
  Check binding: `gsettings get org.gnome.desktop.wm.keybindings switch-to-workspace-left`
  - Unbind `CTRL`+`ALT`+`Left`: `gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "[]"`
  - Unbind `CTRL`+`ALT`+`Right`: `gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "[]"`

### Gnome Tweaks

- General:
  - Disable animations.
- Appearance:
  - Select desired theme.
- Extensions: enable or disable as appropriate.
- Fonts:
  - Hinting: _Slight_  
    _Slight_ seems to have benefits w.r.t. hinting present in font vs. font processor.)
  - Anti-aliasing: _None_
  - Scaling Factor: _1.50_ (Depends on screen dpi. Based on default of 96 dpi.)
  - Configure font sizes appropriately.  
    Expected font sizes around 9-11, if unreadable DPI value should be increased preferably.
- Keyboard & Mouse:
  - Set _Compose_ key for complex symbol composition typing.
  - Acceleration profile: _Flat_.
  - Configure _Touchpad_ behavior.
- Window Titlebars:
  - Maximize: _off_
  - Minimize: _on_
  - Placement: _left_
- Windows:
  - Resize with secondary-click: _on_
  - Window action key: _Alt_

### Manual configuration

- Disable auto-mounting USB drives:  
  ```
  gsettings set org.gnome.desktop.media-handling automount false
  gsettings set org.gnome.desktop.media-handling automount-open false
  ```

## XFCE configuration

### User settings

- Xfce Terminal:
  - Disable "display menubar in new windows"
  - Disable all shortcuts
- Window Manager:
  - Style -> Theme: `Default-hdpi`
  - Add shortcut keys:
    - `Super+Left`: Tile window on left
    - `Super+Right`: Tile window on right
    - `Super+Up`: Tile window on top
    - `Super+Down`: Tile window on bottom
    - `Super+PgUp`: Maximize window
    - `Super+PgDown`: Hide window
  - Remove shortcut keys: `CTRL+ALT+{UP,DOWN,LEFT,RIGHT}` (they conflict with IntelliJ shortcuts)
- Window Manager Tweaks:
  - Disable "Enable display compositing" (Tab: Compositor) (_critical for responsiveness/video latency_)
  - Enable "Activate focus stealing prevention"
  - Disable "Use mouse wheel on title bar to roll up the window."
- Keyboard:
  `xfce4-popup-whiskermenu` bound to `Super + R`  
  _Cannot use `Super L` as this prevents keyboard combos using `Super` key._
- Appearance settings:
  - Disable anti-aliasing
  - "Full" hinting (no specific pref for subpixel order)
  - Custom DPI setting: ~192 (or similar) for HiDPI, ~112 for non-HiDPI.  
    For notebook screens, use approx.: `⌊<original-dpi> * .88⌋`  
    Aim at DPI-value sufficiently high such that `9 pt` fonts are comfortably readable.
- Session and Startup:
  - Shutdown: Enable "Lock screen before sleep"
- Screensaver:
  - Mode: "Blank screen only"
  - Disable 'fade out' effect
- Workspaces:
  - Configure 2 workspaces.
  - Configure hotkeys: `CTRL+SUPER+LEFT` for previous workspace, `CTRL+SUPER+RIGHT` for next workspace.

### Calculate and configure custom DPI settings

1. Figure out screen sizes according to displays EDID record: `xrandr`  
   Result: `eDP-1 connected primary 3200x1800+0+0 (normal left inverted right x axis y axis) 294mm x 165mm`
2. Calculate expected DPI according to screen size and resolution:  
   - `<dpi-horizontal> = <x-resolution> / (<screen-width-in-cm> / 2.54)`
   - `<dpi-vertical> = <y-resolution> / (<screen-height-in-cm> / 2.54)`
3. Configure the correct DPI settings:
   - In X11/Xorg: `/etc/X11/Xresources`, option `Xft.dpi: 192` (Where `192` is DPI value.)
   - In log-in screen: `/etc/lightdm/lightdm-gtk-greeter.conf`, option `xft-dpi=192`. (Where `192` is DPI value.)
   - In XFCE: `Appearance settings`, tab `Fonts`, `Custom DPI setting: 192`.

## Application configuration

### Firefox

- Preferences → General → Network Settings:
  - Check _Enable DNS over HTTPS_
- `about:config`:
  - `privacy.firstparty.isolate`: `true`
  - In case you want to disable JIT compilers as attack vector:
    - `javascript.options.baselinejit`
    - `javascript.options.ion`
    - `javascript.options.asmjs`
    - `javascript.options.wasm`, or: `javascript.options.wasm_baselinejit` && `javascript.options.wasm_optimizingjit`
- Extensions:
  - uBlock Origin
  - uMatrix
  - HTTPS Everywhere
  - Privacy Badger
  - Livemarks
  - TabStash

- Blocklists/Filterlists:
  - [Energized Pro](<https://energized.pro>)
  - [OISD](<https://oisd.nl>)
  - [NoTracking](<https://github.com/notracking/hosts-blocklists>)

### Firmware updates (fwupdmgr/LVFS)

- Check for firmware updates (for supported devices):
  1. Refresh updates metadata: `fwupdmgr refresh`
  1. List available updates: `fwupdmgr get-updates`
  1. Install/schedule available updates: `fwupdmgr update`

### Git

- Apply configuration according to summary at [git-config](../config/git-config).

### GnuPG

- Copy [gpg.conf](../config/gnupg/gpg.conf) to `~/.gnupg/gpg.conf` and set `chmod 600 ~/.gnupg/gpg.conf`.

### Mutt

#### Actions

- `s` then choose folder: move/"save" email message to specific mailbox/folder

#### Config

- `~/.muttrc` for primary configuration of mutt.
- `~/.mutt/mailcap` for mime-type mapping to viewer.

#### Tools

- `/usr/bin/nvim` for email editor
- `/usr/bin/w3m` for html content viewer.

### Neovim

#### Set-up

1. Install Plug: `curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`
1. Create `init.vim` under `~/.config/nvim` with:  
   ```
   call plug#begin('~/.local/share/nvim/plugged')

   Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

   call plug#end()
   ```
1. Call `:PlugInstall` to initiate _Plug_ plug-in installation.

#### Tips & Tricks

- Working with multiple views in a single document:
  - `:sp` for horizontal split, `:vsp` for vertical split.
  - `CTRL+W j` (or `k`, `h`, `l`) to move between splits.
  - `CTRL+W +` and `CTRL+W -` to change split ratio/move split boundary.
  - `CTRL+W =` equalize all splits.
  - `CTRL+W R` to swap top/bottom or left/right splits.
  - `CTRL+W T` extract current split view into separate tab.
  - `CTRL+W o` close everything but the current view.
  - `:help splits` to check documentation.
- Clear search highlights: `:noh`
- Disable highlighting entirely: `set nohlsearch` (or toggle with `set hlsearch!`)

### Syncthing

- Syncthing allows setting a "modification time window" in advanced settings to mitigate deviations due to mod-time inaccuracies in the operating system or file system.
- `syncthing -reset-deltas` for resetting deltas in case of persistent "99% complete" sync status.
- `syncthing -reset-database` for even more (???)

### WINE

- Enable "Emulate virtual desktop" with desired resolution.  
  _This ensures that wine applications are propertly isolated and don't interfere with other (native) applications._
- Execute application in "desktop" window: (still depends on setting _Emulate virtual desktop_)
  - Use: `wine explorer /desktop Avernum.exe`
  - ... with specified resolution: `wine explorer /desktop,1600x900 Avernum.exe`
- Wine tricks and `winetricks`:
  - Compatibility issues with audio-support (XAudio, XactEngine, etc.) can be resolved by installing _FAudio_: `winetricks faudio`

## Customization: building linux kernel

The steps necessary to build a vanilla kernel and install it onto a running linux system.

1. Install necessary dependencies: `sudo dnf install openssl-devel ncurses-devel elfutils-libelf-devel flex bison patch sbsigntools`
1. Applying additional patches: `patch -p1 < patch.diff`
1. Configuring the kernel:
   1. Use the distribution configuration as a starting point: `cp /boot/config-<kernel-version> .config`
   1. Configuration editor: `make nconfig`
      - Disable compiling debug symbols into kernel (section `kernel hacking`)
      - Disable a bunch of file systems that are not used.
      - Disable a bunch of partition types that are not used.
      - Enable "Enable loadable module support" -> "Require modules to be validly signed"
1. Building and installing a kernel:
   ```
   make -j6 bzImage modules
   sudo make modules_install install
   ```
1. Configuring _grub2_:
   ```
   sudo grubby --info=ALL
   sudo grubby --set-default-index=0
   ```
   - Note: also check /boot/efi/EFI/.../grubenv file to configure default kernel parameters.
   - Note that the newly installed kernel is most likely on top. So that would mean index `0`, but `--info=ALL` shows you the full list of available boot options, so that allows you certainty of the choice.
   - To prevent automatically updating grub configuration on Fedora kernel package updates: open `/etc/sysconfig/kernel` and change `UPDATEDEFAULT=yes` to `UPDATEDEFAULT=no`.
1. Removing an installed kernel:
   1. `rm /boot/vmlinuz-$kver /boot/initramfs-$kver /boot/System.map-$kver`
   1. `rm -rf /lib/modules/$kver`

## Customization: secure boot

Verify validation:

- Check if file exists: `ls /sys/firmware/efi/efivars/MokSBStateRT-*`  
  _Note: file exists only if validation is enabled in the shim (MokManager)._
- During booting, right after the boot entry is chosen in GRUB2, it shows a single-line message saying that you are booting in insecure mode.
  _Note: this only shows if validation is indeed disabled._

### Enabling "secure boot" validation in the shim

1. You notice that the booting process is insecure if this is reported in the console just before the booting process starts loading your distribution. Or, if just any arbitrary module may be loaded. (Which may not be desirable in all cases.)
1. `sudo mokutil --enable-validation`
  - You will be asked to enter a temporary one-time use password.
1. Restart and you will enter MokManager.
  - You will be asked to enter the one-time use password, probably in form of individual characters at a given position.
1. Follow the steps to confirm that you want to enable validation.

### Creating and registering a MOK key

1. Generate certificate: `openssl req -new -x509 -newkey rsa:2048 -keyout MOK.key -outform DER -out MOK.der -nodes -days 3650 -subj "/CN=brilliance MOK/"`
1. Import MOK certificate into MOK key store: `sudo modutil --import MOK.der`
  - You will be asked for a password, which is a temporary one-time use password.
  - Reboot your machine.
  - Instead of booting linux, MokManager will show up. Enter MokManager. (For example, press a key if it asks you to.)
  - Choose the option to _Enroll MOK_ or something similar. (The preparations for this are done by `mokutil` in the previous step.)
  - Review and finish enrolling the MOK. Now continue to boot into linux.
1. Sign the custom modules using: `sudo /usr/src/kernels/4.19.10-300.fc29.x86_64/scripts/sign-file sha256 ./MOK.key ./MOK.der /lib/modules/4.19.10-300.fc29.x86_64/extra/wireguard/wireguard.ko`
1. Now try loading the signed module. It should now succeed.

## Customization: hardening

- Firewall installed/configured, and appropriate default zone.
- Check for services enabled by default, open ports.
  - Ensure `sshd` only supports safe, reliable authentication methods, such as 'public key authentication'. (E.g. ensure password authentication is disabled.)  
    ```
    PubkeyAuthentication yes
    AuthorizedKeysFile .ssh/authorized_keys
    HostbasedAuthentication no
    PasswordAuthentication no
    PermitEmptyPasswords no
    ChallengeResponseAuthentication no
    KerberosAuthentication no
    GSSAPIAuthentication no
    UsePAM no
    ```  
    Verify with: `ssh -o PreferredAuthentications=none -o NoHostAuthenticationForLocalhost=yes localhost`
  - Either enable or disable `sshd` at boot:
    - Enable: `sudo systemctl enable --now sshd`
    - Disable: `sudo systemctl disable --now sshd`
- Ensure only signed modules are loaded: `module.sig_enforce=1`
- Ensure lockdown mode: (check at `/sys/kernel/security/lockdown`)
  - Enable _integrity_: `lockdown=integrity`
  - Ensure _confidentiality_: `lockdown=confidentiality`
- Ensure _Secure Boot_ validation is enabled, also in shim. (See earlier section.)

## Customization: LVM

### Create cache for logical volume

Manage off-disk disk caches, such that a slow disk can be supported by a fast SSD carrying its cache.

1. Create cache volume: `lvcreate -L 20G -n root-cache systemVG /dev/sdb1`
   - With `systemVG` the name of the volume group. The cache must be part of the same volume group as the logical volume that should have cache backing.
   - With `/dev/sdb1` the disk whose lvm extents should be used. Do this to ensure that the cache volume is created on the fast rather than the slow volume.
1. Create cache metadata volume: `lvcreate -L 20M -n root-cache-meta systemVG /dev/sdb1`. The metadata volume should have about `1/1000` the size of the cache volume.
1. Combine cache volume and metadata volume into a _cache-pool_: `lvconvert --type cache-pool --cachemode writethrough --poolmetadata systemVG/root-cache-meta systemVG/root-cache`
1. Attach cache-pool to logical volume `root`: `lvconvert --type cache --cachepool systemVG/root-cache systemVG/root`

### Remove cache for logical volume

1. Remove cache: `lvremove systemVG/root-cache`

Make sure that you remove the _cache_ logical volume and the system will manage everything else that's needed.

### Display all components of cached logical volume

1. `lvdisplay -a` (Use `-a`, otherwise cache volumes will be hidden.)

### Find the additional _physical volumes_

If the cache _physical volume_ exists on an encrypted partition:

1. Add the LUKS-partition to `/etc/crypttab`.
1. Regenerate kernel initramfs-image: `/usr/bin/dracut -v -f`
1. Add the LUKS-partition by UUID to the default grub configuration: `/etc/default/grub` as kernel parameter `rd.luks.uuid=<crypttab-luks-name>`
	1. Regenerate grub2 configuration: `/usr/sbin/grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg`, or destination of soft-link `/etc/grub2-efi.cfg` for the location of your grub config-file.

## Troubleshooting

- SELinux: unexplained permission issues, try relabeling: `restorecon -FRvv ~/.ssh`
- SELinux: mass relabeling: create empty file `/.autorelabel` (`sudo touch /.autorelabel`)

## Notes

Notes on desktop-related concerns.

- Create desktop shortcut:  
  ```
  [Desktop Entry]
  Encoding=UTF-8
  Version=1.0
  Type=Application
  Name=Freeplane
  GenericName=Mind-mapping tooling
  Comment=Freeplane mind-mapping tool
  Icon=/home/danny/Apps/freeplane/freeplane.svg
  Exec=env JAVA_HOME=/usr/lib/jvm/java-11-openjdk /home/danny/Apps/freeplane/freeplane.sh %f
  Path=/home/danny/Apps/freeplane
  Categories=Development;IDE;
  Terminal=false
  ```  
  See [FreeDesktop.org: desktop entry specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html#recognized-keys)
  - Use `env JAVA_HOME=/opt/java-11-openjdk freeplane.sh` to explicitly set environment variables before starting the application.
- Use `dbus-monitor` to peek on dbus interaction, allowing identification of particular actions.

## References

- https://thoughtbot.com/blog/vim-splits-move-faster-and-more-naturally
- [Arch wiki page on Mutt](https://wiki.archlinux.org/index.php/Mutt "Mutt")
- [ghacks.net: Mutt is a command line email app](https://www.ghacks.net/2019/11/23/mutt-is-a-command-line-email-app-for-linux-and-heres-how-to-set-it-up/ "Mutt is a command line email app for Linux and here's how to set it up")
- wg(8) man page
- https://www.stavros.io/posts/how-to-configure-wireguard/
- [Matthew Garrett - Linux kernel lockdown, integrity, and confidentiality](https://mjg59.dreamwidth.org/55105.html)
- https://github.com/trimstray/the-practical-linux-hardening-guide
- https://stackoverflow.com/questions/39226185/kernel-module-mokutil-failed-to-enroll-new-keys
- https://github.com/lcp/mokutil/issues/9
- https://devopsx.com/change-resolution-from-command-line-in-ubuntu-18-04s-wayland/

# Devices

## Bluetooth

- _Linux kernel 5.2_: Low polling frequency for bluetooth mice:
  1. `hcitool con`, find handle id for bluetooth mouse.
  1. `hcitool lecup --handle <handle-id> --min 6 --max 7 --latency 0` to reconfigure the connection parameters for better responsiveness.
- DRM interferes with successful pairing ([Xbox One controller](https://www.addictivetips.com/ubuntu-linux-tips/xbox-one-controllers-over-bluetooth-linux/)): `echo 'Y' | sudo tee /sys/module/bluetooth/parameters/disable_ertm` to temporarily disabl	e and allow pairing to succeed.

## Secure Boot

Random notes on this seemingly endless investigation on booting directly into `grubx64.efi`.

- Getting status information:
  - `bootctl status`
  - `mokutil --sb-state`
  - `dmesg`, check for "secure boot" mentions, and "lockdown" status which is forced by secure boot enablement.
- Inspecting PE-binaries for signatures:
  - `sbverify --list <pe-binary>`
  - `osslsigncode verify -in <pe-binary>`
- Verifying PE-binaries:
  - `sbverify --cert <PEM-encoded-cert> <pe-binary>`
  - `osslsigncode verify -in <pe-binary>`
- Tools:
  - `bootctl`: systemd boot information, includes secure boot status.
  - `dmesg`
  - `mokutil`
  - `sbsigntools`: `sbsign`, `sbverify`, `sbattach`, and more.
  - `efitools`: `cert-to-efi-sig-list`, `sign-efi-sig-list`
  - `osslsigncode`
  - `kmodsign`: signing of kernel modules

```text
> sudo sbverify --list /boot/vmlinuz-5.8.0-50-generic
signature 1
image signature issuers:
 - /C=GB/ST=Isle of Man/L=Douglas/O=Canonical Ltd./CN=Canonical Ltd. Master Certificate Authority
image signature certificates:
 - subject: /C=GB/ST=Isle of Man/O=Canonical Ltd./OU=Secure Boot/CN=Canonical Ltd. Secure Boot Signing (2017)
   issuer:  /C=GB/ST=Isle of Man/L=Douglas/O=Canonical Ltd./CN=Canonical Ltd. Master Certificate Authority
```

```text
> sudo osslsigncode verify -in /boot/vmlinuz-5.8.0-50-generic 
Current PE checksum   : 0095D591
Calculated PE checksum: 0095D591

Message digest algorithm  : SHA256
Current message digest    : 2A3D4FD5B40442B3936E184D77BD2B9DE7A8E8537C3B12E679C62A506DDBA379
Calculated message digest : 2A3D4FD5B40442B3936E184D77BD2B9DE7A8E8537C3B12E679C62A506DDBA379

Signature verification: ok

Number of signers: 1
	Signer #0:
		Subject: /C=GB/ST=Isle of Man/O=Canonical Ltd./OU=Secure Boot/CN=Canonical Ltd. Secure Boot Signing (2017)
		Issuer : /C=GB/ST=Isle of Man/L=Douglas/O=Canonical Ltd./CN=Canonical Ltd. Master Certificate Authority
		Serial : 02

Number of certificates: 1
	Cert #0:
		Subject: /C=GB/ST=Isle of Man/O=Canonical Ltd./OU=Secure Boot/CN=Canonical Ltd. Secure Boot Signing (2017)
		Issuer : /C=GB/ST=Isle of Man/L=Douglas/O=Canonical Ltd./CN=Canonical Ltd. Master Certificate Authority
		Serial : 02

Succeeded
```

Random notes:

- Signed PE-executables may contain embedded intermediate signing certs (public key).
- `osslsigncode` verification sometimes lists checksum mismatch. This may be due to booted system, or may be due to bad calculation. Not sure.

References:

- [Unix &amp; Linux: How to verify signed UEFI binaries?](https://unix.stackexchange.com/questions/636263/how-to-verify-signed-uefi-binaries)
- [Debian wiki: Secure Boot](https://wiki.debian.org/SecureBoot)
- [Ubuntu wiki: Secure Boot](https://wiki.ubuntu.com/UEFI/SecureBoot)
- [Ubuntu QA regression testing git-repo](https://git.launchpad.net/qa-regression-testing/tree/notes_testing/secure-boot/)

## dmesg error messages

- In case of errors, add kernel boot parameter `pci=nommconf` to disable the mechanism (or `pci=noaer` to disable reporting of such errors only).  
  ```
  [15616.407422] pcieport 0000:00:1d.0: AER: Multiple Corrected error received: 0000:3c:00.0
  [15616.407437] pcieport 0000:00:1d.0: AER: PCIe Bus Error: severity=Corrected, type=Data Link Layer, (Transmitter ID)
  [15616.407442] pcieport 0000:00:1d.0: AER:   device [8086:9d18] error status/mask=00001000/00002000
  [15616.407448] pcieport 0000:00:1d.0: AER:    [12] Timeout               
  [15616.407456] nvme 0000:3c:00.0: AER: PCIe Bus Error: severity=Corrected, type=Physical Layer, (Receiver ID)
  [15616.407461] nvme 0000:3c:00.0: AER:   device [1c5c:1284] error status/mask=00000081/0000e000
  [15616.407465] nvme 0000:3c:00.0: AER:    [ 0] RxErr                 
  [15616.407469] nvme 0000:3c:00.0: AER:    [ 7] BadDLLP               
  [15616.407473] nvme 0000:3c:00.0: AER:   Error of this Agent is reported first
  ```

## Troubleshooting

- `tlp` is aggressive when saving power. It may cause kernel modules, such as wifi and networking, to fail/crash.
