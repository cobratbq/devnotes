# Fedora linux

## Fedora Silverblue installation

1. Perform full upgrade
1. Additional kernel parameters:
  1. Force valid signature on modules: `rpm-ostree kargs --append=module.sig_enforce=1`
  1. _version >= 5.4_: Lock down kernel by adding restrictions to protect _integrity_ (`lockdown=integrity`) or extend further to protect the kernel's _secrets_ (`lockdown=confidentiality`). Check current state at: `/sys/kernel/security/lockdown`.
1. Configure udev rule for setting _bfq_ IO-scheduler:
   1. See rules in [/config/udev/rules.d/60-scheduler.rules](../config/udev/rules.d/60-scheduler.rules).
   1. Reload configuration: `sudo udevadm control --reload`
1. Configure sysctl, see `../config/sysctl.conf`
1. Consider adding zram swap-space: `systemctl status zram-swap`
1. Enable [Flathub repository](https://flathub.org) for flatpak packages.
1. Enable [RPM Fusion](https://rpmfusion.org/) repositories (free).
1. Refer to [Desktop configuration][desktop-configuration] for configuration of DNS resolver.
1. Install additional audio/video support (codecs, players):
  - Install additional GStreamer support: `sudo rpm-ostree install gstreamer1-svt-av1.x86_64 gstreamer1-svt-vp9.x86_64 gstreamer1-vaapi.x86_64 gstreamer1-plugin-openh264.x86_64`
  - _Optional_ install `ffmpeg-libs` from RPM Fusion Free repository for additional audio/video codecs.
  - Install _VLC_ media player.
  - Install other desired applications.
1. Install additional software:
   1. Install extended git commands: `rpm-ostree install git`
   1. Install Gnome3 Tweaks utility: `rpm-ostree install gnome-tweaks`
   1. Install firewall configuration: `rpm-ostree install firewall-config`
      - Start _Firewall Configuration_
      - Configure default zone: `public` (_Options_, _Change default zone_)
   1. Install Claws-Mail email client: `rpm-ostree install claws-mail claws-mail-plugins-bogofilter claws-mail-plugins-pgp claws-mail-plugins-rssyl claws-mail-plugins-tnef claws-mail-plugins-vcalendar`
   1. Install _ffmpeg-libs_ for audio and video support: `rpm-ostree install ffmpeg-libs`
   1. Install support for smartcards/security-keys: `rpm-ostree install pcsc-lite`
   1. Install Java runtime and SDK: `rpm-ostree install java-11-openjdk{-src,-devel,-javadoc}`
   1. Install _fish_ shell (as the default shell)
      - `rpm-ostree install fish`
      - `sudo nvim /etc/passwd` and change the shell to `/usr/bin/fish` for the appropriate users.

1. Rebasing ostree image:  
```
ostree remote refs fedora
rpm-ostree rebase fedora:fedora/32/x86_64/silverblue
```
1. Pinning kernels:
   - ostree admin pin 0
   - ostree admin pin --unpin 0

### Additional: add support for snaps

 1. Enable _Snap_ packages through [Snapcraft.io](https://snapcraft.io) repository:  
    _`snapd` will not be able to install classic packages._
    1. Install _snapd_: `sudo rpm-ostree install snapd`. Reboot to make `snap` available.
    1. Install [_Snap store_](https://snapcraft.io/snap-store): `sudo snap install snap-store`.
    1. Install software (snaps) through the _Snap store_ or using `sudo snap install package`.  

## Fedora Workstation installation

An outline of a Fedora Workstation set-up.

1. Perform full upgrade
1. Configure udev rule for setting `bfq` IO scheduler:
   1. See rules in [/config/udev/rules.d/60-scheduler.rules](../config/udev/rules.d/60-scheduler.rules).
   1. Reload configuration: `sudo udevadm control --reload`
1. Configure sysctl, see `../config/sysctl.conf`
1. Install software: `sudo dnf install gnome-tweaks neovim clementine pass filezilla gucharmap htop podman claws-mail claws-mail-plugins-rssyl claws-mail-plugins-vcalendar claws-mail-plugins-bogofilter claws-mail-plugins-pgp claws-mail-plugins-tnef pcsc-lite firewall-config`
1. Remove software: `sudo dnf remove totem gnome-boxes cheese gnome-photos rhythmbox gnome-video-effects qemu-guest-agent spice-vdagent virtualbox-guest-additions open-vm-tools-desktop hyperv-daemons`
1. Install and configure Unbound recursive dns server:
   1. Install: `sudo dnf install unbound`
   1. Configure Network Manager to skip DNS configuration on network connects: in `/etc/NetworkManager/NetworkManager.conf`, in section `[main]`, add line `dns=none`.
   1. Restart Network Manager to make configuration change active: `sudo systemctl restart NetworkManager`.
   1. Modify `/etc/resolv.conf` to list:
   ```
   # Manual configuration
   nameserver 127.0.0.1
   nameserver [::1]
   ```
   1. Enable Unbound service immediately and on start-up: `sudo systemctl enable --now unbound`.
   1. Verify correct operation: `host -v www.google.com` and verify that resolver is `127.0.0.1`.
1. Enable RPM Fusion repositories: https://rpmfusion.org
   1. `sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude="PackageKit-gstreamer-plugin"`
   1. `sudo dnf groupupdate sound-and-video`
1. Install additional software: `sudo dnf install vlc ffmpeg-libs`
1. Install Java 8 development: `sudo dnf install java-1.8.0-openjdk-{devel,src,javadoc}`
1. Install Java 11 development: `sudo dnf install java-11-openjdk{,-devel,-src,-javadoc}`

## Fedora XFCE installation

1. Perform default Fedora XFCE installation.
1. Configure udev rule for setting `bfq` IO scheduler:
   1. See rules in [/config/udev/rules.d/60-scheduler.rules](../config/udev/rules.d/60-scheduler.rules).
   1. Reload configuration: `sudo udevadm control --reload`
1. Configure sysctl, see `../config/sysctl.conf`
1. Remove software:  
   `sudo dnf remove orage abiword gnumeric pragha parole geany qemu-guest-agent spice-vdagent virtualbox-guest-additions open-vm-tools-desktop hyperv-daemons fedora-release-notes galculator catfish xfce4-clipman-plugin xfce4-dict asunder xfdashboard gparted`
1. Install software:  
   `sudo dnf install clementine libreoffice htop pass filezilla fwupd gucharmap`
1. Install and configure Unbound recursive dns server:
   1. Install: `sudo dnf install unbound`
   1. Configure Network Manager to skip DNS configuration on network connects: in `/etc/NetworkManager/NetworkManager.conf`, in section `[main]`, add line `dns=none`.
   1. Restart Network Manager to make configuration change active: `sudo systemctl restart NetworkManager`.
   1. Modify `/etc/resolv.conf` to list:
   ```
   # Manual configuration
   nameserver 127.0.0.1
   nameserver [::1]
   ```
   1. Enable Unbound service immediately and on start-up: `sudo systemctl enable --now unbound`.
   1. Verify correct operation: `host -v www.google.com` and verify that resolver is `127.0.0.1`.
1. Enable [RPM Fusion](https://rpmfusion.org/) _free_ and _nonfree_ repositories.
1. Install additional software:  
   `sudo dnf install vlc gstreamer-ffmpeg libdvdnav libdvdread`
1. Install `libdvdcss` directly from RPM Fusion "Free Tainted" repository.
1. OPTIONAL: install _compton_ as replacement compositor. Compton has better performance and quality and supports disabling compositing when full-screen applications, such as games, are active.

## Notes / Tasks

- Configure default kernel arguments for _grub2_  
  _Note: check for use of BLS (Bootloader Spec) at `/etc/default/grub`, line `GRUB_ENABLE_BLSCFG=true`_
  - With BLS, update: `/boot/grub2/grubenv`, variable `$kernelopts`.
  - Without BLS, update `/etc/default/grub2`, `GRUB_CMDLINE_LINUX=..`.
