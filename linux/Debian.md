# Debian

## Notes

- Enable/use multi-arch support:
  - Add multi-arch capabilities: `sudo dpkg --add-architecture i386` to add _i386_ support for _amd64_. (E.g. _Steam_.)
  - Check existing multi-arch (foreign) architectures: `dpkg --print-foreign-architectures`
- Use `apt upgrade` for safe upgrades and `apt dist-upgrade`/`apt full-upgrade` for potentially breaking upgrade activities.  
  Typicaly, one does not require `dist-upgrade` but instead should wait for things to resolve themselves. An exception is to upgrade to a different release, which may _require_ invasive changes.
- `sudo apt -t debian-backports upgrade` to show with option to upgrade all installed packages to _bullseye-backports_ versions.
- `sudo apt -t debian-backports install <package>` to install/upgrade a single package from _bullseye-backports_ repository.
- Check which package owns file: `dpkg-query -S /bin/bash`

### Build vanilla kernel

- download and unpack kernel package
- `make nconfig`
- `make -j4 && make -j4 modules` (add `make -j4 dtbs` for ARM64)
- For ARM64: `sudo INSTALL_DTBS_PATH=/usr/lib/linux-image-5.15.36 make dtbs_install` (check u-boot-menu for install-path to correspond to expectations in logic)
- `sudo make modules_install install`
