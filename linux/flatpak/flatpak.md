# Flatpak

## Use

- Use `flatpak uninstall --delete-data <app-ID>` to uninstall a package.
- Use `flatpak repair --user` to repair existing installations and prune data.
- Use `flatpak uninstall --unused` to clean up unused runtimes.

## Packaging
  
- [Example `Makefile`](Makefile-example) for prepared set of targets for various flatpak-builder operations.
  - `validate` to validate _appdata_
  - `build` to build the flatpak package.
  - `run`, `install`, `uninstall` to run/install/uninstall built flatpak from local `build` directory.
  - `repo` to generate local repository.

## Tricks

- Find name of the `.Debug` debug symbols package:  
  _[Additional information on debugging flatpak apps](https://docs.flatpak.org/en/latest/debugging.html)_
  1. `flatpak info -m <package-name>` or `flatpak remote-info -m <remote> <package-name>`.
  1. Find the extension with `.Debug` suffix.
- Inspect flatpak to see where further clean-up is possible:
  1. `flatpak run --command=/bin/bash org.openttd.OpenTTD`
  1. check `/app`
- Clean-up, consider:
  - `include`
  - `lib/pkgconfig`
  - `lib/girepository-1.0`
  - `share/pkgconfig`
  - `share/aclocal`
  - `share/gtk-doc`
  - `share/doc`
  - `share/info`
  - `share/gir-1.0`
  - `share/man`
  - `*.la`
  - `*.a`
- Observe session bus queries:
  1. `flatpak run --log-session-bus org.claws_mail.Claws-Mail`

## References

- See [Flathub App-Submission document](https://github.com/flathub/flathub/wiki/App-Submission)
- See [Flathub App-Requirements document](https://github.com/flathub/flathub/wiki/App-Requirements)
- [Flatpak manifest properties](http://docs.flatpak.org/en/latest/manifests.html#basic-properties)
- [`flatpak-builder` command reference](http://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html)
  - parameter `--talk-name=<dbus-object-id>` and `--system-talk-name=<dbus-object-id>` to allow d-bus access, e.g. to NetworkManager.  
    Reference: [Part III. D-Bus API Reference][dbus-api-reference]

[dbus-api-reference]: https://developer.gnome.org/NetworkManager/unstable/spec.html "Part III. D-Bus API Reference"

