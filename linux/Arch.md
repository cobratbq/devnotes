# Arch / Manjaro linux

Some notes on setting up Arch/Manjaro linux.

## Configure https mirrors for pacman

`sudo pacman-mirrors -ac all -P https`

## WiFi / crda

Install `crda` for better wifi regulatory support: `sudo pacman -S crda`

## Enable smartcard support

```
sudo pacman -S pcsclite ccid
sudo systemctl enable --now pcscd
gpg --card-status
```

## Install firewall

1. Install `firewalld`  
   ```
   sudo pacman -S firewalld
   sudo systemctl enable --now firewalld
   ```
1. Optionally, ensure protective default zone:  
   ```
   sudo firewall-cmd --set-default-zone=public
   ```
1. Further configuration using GUI: `firewall-config`

## Others

- Install Gnome Disks utility: `sudo pacman -S gnome-disk-utility`.
- Install OpenJDK 11 Java Runtime: `sudo pacman -S jre11-openjdk`.
- Install Flatpak support: `sudo pacman -S flatpak`.
- Install and enable zswap:  
  ```sh
  sudo pacman -S zswap-arm
  sudo systemctl enable --now zswap-arm
  ```

## Packaging

- Upgrading: (generally: `-Syu`)
  - Metadata/index refresh - checking for updates:
    - Incremental: `-Sy`
    - Full index redownload: `-Syy`
  - Applying updates - upgrading:
    - Pure upgrades only: `-Su`
    - Upgrades with possible removals: `-Suu`
- Checking package consistency: `-Qkkq` (`k` for depth of inspection, `q` to keep quiet for expected results)
- Full reinstall of packages: `sudo pacman -Qqn | sudo pacman -S -`

## Misc

Consider cleaning up Arch/Manjaro package installs and use flatpak for installing specific tools.

- For Pinebook Pro, updating u-boot image in Manjaro linux:  
  ```
  dd if=/boot/idbloader.img of=/dev/mmcblkX seek=64 conv=notrunc,fsync
  dd if=/boot/u-boot.itb of=/dev/mmcblkX seek=16384 conv=notrunc,fsync
  ```

