# Notes

## Issues impeding development

- When redesigning a (big) application or library. When one needs to swap out one abstraction for another, invariably it will not fit perfectly at the start. (E.g. during development)
  - Run unit tests as soon as dependencies are satisfied / package compiles.

## Defining/classification of quality

- Distinguishing between absolute quality and perceived quality, i.e.:
  - "Absolute quality" as the objectively observed (absence of) mistakes, bugs, known-incorrect patterns.  
    The outcome of such an analysis would indicate whether or not you have avoided known-problematic constructions. This would indicate that none of these known "evils" will cause problems.
  - "Perceived quality" as the subjective "consensus-based" good vs. bad patterns and how these are upheld. (For example, following a predefined architecture.  
    The outcome of such an analysis would indicate whether or not you have nicely followed the architecture, but does not evaluate whether or not the architecture itself is sound. It does however provide an indication of the amount of consistency in the application landscape.
- Taking control of quality:
  - Set quality thresholds: compiler warnings? 
  - Quantified levels of quality:
    - "__Proof-of-concept__" - _Only our own developers are expected to operate the software and only on prepared test data ran prior to use._
      - No tests
      - No error handling for exceptional cases: code assumes that only valid data is ever introduced into the application. (Introducing bad input may result in anything, from bad results to total system failure.)
      - Prototype is barely usable because operation relies heavily on internal knowledge of the application.
      - __Focus__: single-minded focus on proving result (proof of concept)
    - "__Prototype__" - _Other users are expected to use the software, however we only support a predetermined use case._
      - Test only the "happy path", as this is the only supported path.
      - Basic error handling for exceptional cases: introduce code that prevents anything but "happy-path" data from being processed. The handling itself is very basic: showing an error is sufficient, as this is not one of the supported use cases. (Introducing the bad input leads to an error.)
      - Prototype is useful, but users may be disappointed quickly as only the perfect input will deliver results.
      - __Focus__: reduced-to-minimum functional concerns
    - "__Minimal application__" - _Application that supports the predetermined minimal needed use cases._
      - Tests for all supported paths, tests to ensure unsupported paths are properly handled.
      - Error handling of supported exceptional cases. Error handling for unsupported exceptional cases that block the user from unintentionally progressing with wrong input.
      - Introduction of appropriate user-friendly messages to explain when a certain situation is not possible/applicable.
      - Minimal application is useful. Sufficient tests are in place to provide regression warnings to identify when things break. User can rely on the application to function properly for supported use cases and to warn in case of unsupported use cases.
      - __Focus__: primarily functional concerns (may provide an operational requirement if urgent for primary use case)
    - _... anything in between intentionally left unspecified. One should find that anything in between are compositions of "Minimal" and "Mature" application code. Areas that are deemed important will grow towards "Mature" faster, while regularly modified areas will remain at lower quality, e.g. around "Minimal". Note that it is import to set a threshold for the lower bound quality level._
    - "__Mature application__" / Framework / Robust / Resistant to failure / ...
      - Tests for all supported paths, tests to ensure unsupported paths are properly handled.
      - Error handling for exceptional cases. Error handling for unsupported exceptional cases. Mitigation in case of certain unexpected failures (operational requirements).
      - Introduction of assisting information to guide the user where needed such that even inexperienced users can find out how to operate the application.
      - Application is generally useful. Sufficient tests are in place to provide regression warnings to identify when things break. Users can rely on the application to function. Companies can make reasonable assumptions that adopting the application as part of the workflow will not cause issues.
      - __Focus__: functional + operational concerns
  - Developmental concerns:
    - ...
  - Define which types of errors are handled. (For example, prototype-level products would handle far less types of errors than production software. Similarly, frameworks that are expected to provide robustness, fault-tolerance and error detection perform error handling with more paranoia than a normal application.)
  - Perform verification of package imports to determine appropriate and inappropriate imports according to ("layered") application architecture.
- Secure boot:
  - How to check if secure boot is active: `bootctl status` or `mokutil --sb-state`
  - Secure boot can be disabled in 2 ways:
    1. Disable secure boot itself in the UEFI.
    1. Stop performing validation: `mokutil --disable-validation` (The second one acts on the shim. The shim will report it is booting in insecure mode.)
- Email RFCs: ([Fastmail technical standards: Email](https://www.fastmail.com/help/technical/standards.html))
  - Structure: RFC5322, RFC2822, RFC822, RFC2045, RFC2046, RFC2047, RFC2231
  - Protocols: RFC5321, RFC821, RFC2821, RFC4551, RFC1939, RFC8620
  - Security: RFC2595, RFC3207, RFC5246, RFC6376, RFC8617
  - Discovery: [Thunderbird autoconfiguration](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Autoconfiguration), RFC6186
  - Filtering: RFC5228
- A simple human-readable format (e.g. JSON/TOML-like) that allows us to store/record constants for use in programs.
  - common format for persisting common knowledge.
  - enabling shared use of common knowledge.
  - ability to transform these constants to any programming language suitable format, e.g. constants in Java.
  - ability to script subselection of values that are relevant.
  - ability to choose a suitable format for use in applications.
  - repository for storing these common values that are not specific to single program.
  - commonly owned knowledge, updated by someone for benefit of everyone.
  - way too many efforts duplicate all the constants that are - for example - documented in a specification to each and every new implementation of that thing. This should be provided as a datasheet, such that it can be updated and automatically derive constants usable in implementations to avoid making mistakes.
  - way to refer to value already present somewhere else. (avoid duplication, ensure correctness)
- [RFC-5785: Defining Well-Known Uniform Resource Identifiers (URIs) (.well-known)](https://tools.ietf.org/html/rfc5785)

## Kubernetes / OCP / ...

- Code-Ready Containers (CRC)
  - `libvirt`-based virtual machine control. (Use `virt-manager` for inspection.)
  - `ssh -i ~/.crc/machines/crc/id_rsa core@(crc ip)`

## Ideas for software

### White noise generator with cancellation channel

A white noise generator that generates random white noise sound for public projection. Then add a negated (configurably delayed) white noise channel that is sent over network (UDP) for receiving by individual participants that can be personally projected through headphones in order to cancel out the environmental noise.

- Configurable (personal) network delays.
- Broadcasting to multiple (receiving) network clients.
- Use of UDP networking protocol due to latency / dropping delayed packets.
- Low-latency requirements.
- Is there some way to do auto-tuning?
- Is this even feasible given that white noise audio source and cancellation audio source are differently positioned, different distance to ears?

Would allow playing around with audio and physics effects, audio/noise cancellation, networking, etc.

### Near-universal messenger

Messenger application that offers only basic messaging functionality but at many platforms/transports. This allows providing a minimum level of communication that is near-universally available to whoever/wherever you are.

- IM-platforms/chat-networks:
  - XMPP/Jabber
  - IRC
  - WhatsApp
  - ...
- Social networks:
  - Twitter
    - Post with replies
    - DMs
  - ActivityPub (Mastodon, etc.)
    - Post with replies
    - DMs
  - Facebook
    - comments
    - Messenger
- Email/Newsgroups:
  - IMAP connection, optionally with IDLE/push messages.
- Forums/Messageboards (public topics, private messages)
  - Reddit
  - ...
- Snippets repositories (? .. would need to allow responses):
  - Pastebin
  - Github Gist
- Code-repositories(?)
  - Github
  - Gitlab
  - ...

## Personal discovery

- Revenge-plots
  - "Sins of the Father" (OSP)
  - punishment for helping people
  - 

